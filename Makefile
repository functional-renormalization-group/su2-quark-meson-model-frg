SHELL := /bin/bash
#CXX = g++ -std=c++11 -fopenmp
#CXX = g++ -O3 -std=c++11 -fopenmp
CXX = g++ -O3 -Wall -Wextra -Wfloat-equal -Wundef -Wlogical-op -Wmissing-declarations -Wredundant-decls -Wshadow -std=c++11 -fopenmp
#CXX = g++ -Wall -Wextra -Wfloat-equal -Wundef -Wlogical-op -Wmissing-declarations -Wredundant-decls -Wshadow -std=c++11 -fopenmp

INCLUDE_DIRS = -Isrc -Isrc/interpolation_methods -Isrc/ini_file_parser -Isrc/su2_quark_meson_model -Isrc/gsl_wrapper -Isrc/grid -Isrc/math_utils

DEPS = src/grid/derivative_finite_differences.h \
	   src/grid/OneDimensionalGrid.h \
	   src/gsl_wrapper/root_solver_gsl.h \
	   src/gsl_wrapper/InterpolationGSL1Dim.h \
	   src/math_utils/OneVariableFunction.h \
	   src/interpolation_methods/LinearInterpolation.h \
	   src/interpolation_methods/QuadraticInterpolationGivenExtremum.h \
	   src/ini_file_parser/IniFileParser.h \
	   src/su2_quark_meson_model/SU2QuarkMesonModelGrandPotentialFileParser.h \
	   src/su2_quark_meson_model/SU2QuarkMesonModelFlowEquations.h \
	   src/su2_quark_meson_model/SU2QuarkMesonModelEffectiveAction.h \
	   src/su2_quark_meson_model/SU2QuarkMesonModelGrandPotential.h \
	   src/su2_quark_meson_model/SU2QuarkMesonModelGrandPotentialCalculator.h \
	   src/su2_quark_meson_model/SU2QuarkMesonModelPhaseDiagram.h \
	   src/command_line_processor.h

OBJ =  obj/main.o \
	   obj/grid/derivative_finite_differences.o \
	   obj/grid/OneDimensionalGrid.o \
	   obj/gsl_wrapper/root_solver_gsl.o \
	   obj/gsl_wrapper/InterpolationGSL1Dim.o \
	   obj/math_utils/OneVariableFunction.o \
	   obj/interpolation_methods/LinearInterpolation.o \
	   obj/interpolation_methods/QuadraticInterpolationGivenExtremum.o \
	   obj/ini_file_parser/IniFileParser.o \
	   obj/su2_quark_meson_model/SU2QuarkMesonModelGrandPotentialFileParser.o \
	   obj/su2_quark_meson_model/SU2QuarkMesonModelFlowEquations.o \
	   obj/su2_quark_meson_model/SU2QuarkMesonModelEffectiveAction.o \
	   obj/su2_quark_meson_model/SU2QuarkMesonModelGrandPotential.o \
	   obj/su2_quark_meson_model/SU2QuarkMesonModelGrandPotentialCalculator.o \
	   obj/su2_quark_meson_model/SU2QuarkMesonModelPhaseDiagram.o \
	   obj/command_line_processor.o


obj/%.o: src/%.cpp $(DEPS)
	$(CXX) $(INCLUDE_DIRS) -c $< -o $@

obj/interpolation_methods/%.o: src/interpolation_methods/%.cpp $(DEPS)
	@mkdir -p $(dir $@)
	$(CXX) $(INCLUDE_DIRS) -c $< -o $@

obj/ini_file_parser/%.o: src/ini_file_parser/%.cpp $(DEPS)
	@mkdir -p $(dir $@)
	$(CXX) $(INCLUDE_DIRS) -c $< -o $@

obj/gsl_wrapper/%.o: src/gsl_wrapper/%.cpp $(DEPS)
	@mkdir -p $(dir $@)
	$(CXX) $(INCLUDE_DIRS) -c $< -o $@

obj/grid/%.o: src/grid/%.cpp $(DEPS)
	@mkdir -p $(dir $@)
	$(CXX) $(INCLUDE_DIRS) -c $< -o $@

obj/math_utils/%.o: src/math_utils/%.cpp $(DEPS)
	@mkdir -p $(dir $@)
	$(CXX) $(INCLUDE_DIRS) -c $< -o $@

obj/su2_quark_meson_model/%.o: src/su2_quark_meson_model/%.cpp $(DEPS)
	@mkdir -p $(dir $@)
	$(CXX) $(INCLUDE_DIRS) -c $< -o $@

run: $(OBJ) 
	$(CXX) $(OBJ) $(INCLUDE_DIRS) -L/usr/local/lib -lgsl -lgslcblas -o bin/qmFRG.out

clean: 
	rm -f bin/qmFRG.out $(OBJ)
