# Phase Diagram Tools and Scripts

This folder contains tools and scripts to build, analyze, and plot phase diagrams based on data obtained by solving the Functional Renormalization Group (FRG) flow equations of the SU(2) Quark Meson Model.

The tools are mainly written using Python and some Bash scripts.

At the moment the following features are incorporated in this folder:
1. Build and plot the phase diagram of the SU(2) Quark Meson Model, within the FRG formalism, using standard parameters. This is accomplished by solving the flow equations of the model for fixed values of temperature and quark chemical potentials ($\mu_q=\mu_u=\mu_d$).



## Directory Structure

### phase_diagram/

This directory contains Python modules and scripts for building and analyzing phase diagrams. The key components include:
- phase_diagram_builder.py: Core script for constructing the phase diagram based on provided data.
- phase_diagram_data.py: Handles data structures and parsing for phase diagram calculations.
- phase_transition_analyzer_fixed_temperature.py: Analyzes phase transitions at fixed temperatures.


### tests_standard_params_kIR80/

This directory contains all the scripts and data necessary to reproduce and visualize phase diagrams using the standard parameter set (the 'standard' defined in this documentation refers to the parameter set used by Tripolt et al, in [10.1103/PhysRevD.97.034022](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.97.034022), see also [here](https://arxiv.org/abs/1709.05991)).  Such parameter set primarily differs in the infrared cutoff, which is set to $k_{\mathrm{IR}}=0.080 \, \mathrm{GeV}$.

Additionally, this directory is designed for this specific set of parameters but is structured to allow adaptation to other parameter sets.

The main components are:
- old_results/: Contains previous phase diagram results and comparison plots (mainly from Renan's PhD thesis).
- input_data/: Pre-configured input data files (e.g., grand potential files for various temperatures) and configuration settings for the model.
- output/: Directory where generated phase diagram data and plots are saved.
- build_phase_diagram.py: Python script to process the Grand Potential data into phase diagram data.
- plot_phase_diagram.py: Python script to plot the phase diagram from the computed data.
- run_grand_potential_calculation.sh: Bash script to compile and execute the C++ code that evaluates the Grand Potential for the parameters defined in the input_data/standard_kIR80_config.ini file.
- run_phase_diagram_calculation.sh: Bash script to generate and plot the phase diagram after computing the Grand Potential.

For detailed instructions on how to use the tests_standard_params_kIR80/ folder, refer to the specific README file inside that directory.

## Getting Started

To work with this directory, you will need:
1. C++ Compiler and Libraries: Ensure you have the necessary prerequisites to compile the C++ code found in the sister directory src/. This includes having a C++ compiler, make, and the GNU Scientific Library (GSL) installed.
2. Python and Dependencies: You need Python 3 and some libraries.

You can install the Python dependency with:
```
python3 -m pip install scipy
python3 -m pip install matplotlib
```

