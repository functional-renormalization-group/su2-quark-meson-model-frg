import argparse
import os
import sys
import matplotlib.pyplot as plt
import numpy as np

# Get the parent directory of the current script (two levels up) and add the parent directory to sys.path
# This is necessary in order to import the PhaseTransitionAnalyzerAtFixedTemperature class
parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(parent_dir)

from phase_diagram.phase_diagram_data import *
from phase_diagram.phase_diagram_builder import extract_suffix


# Function to plot the phase diagram
def plot_phase_diagram(output_folder, data, filename):
    plt.figure(figsize=(10, 6))
    plt.figure(figsize=(6, 6), dpi=500)
    
    # Plot lower spinodal bound
    plt.plot(data.spinodals_lower, data.temperatures, label="Spinodal lines", color="red", linewidth=2)
    
    # Plot upper spinodal bound
    plt.plot(data.spinodals_upper, data.temperatures, color='red', linewidth=2.5)

    # Plot phase transition point
    plt.plot(data.phase_transitions, data.temperatures, label="1st order line", color="black", linewidth=2)

    # Plot Critical End point
    plt.plot(data.phase_transitions[-1], data.temperatures[-1], marker='o', markersize=12, color='black')

    # Add labels and title
    plt.xlabel(r'$\mu_q\,[\mathrm{GeV}]$', fontsize=16)
    plt.ylabel(r'$\mathrm{T}\,[\mathrm{GeV}]$', fontsize=16)
    #plt.title("Phase Diagram")

    # Show legend
    plt.legend(loc='upper left')

    plt.grid(True)
    
    # Set x and y limits to a given range
    xmin = 0.325
    xmax = 0.355
    ymin = 0.005
    ymax = 0.050
    plt.xlim([xmin, xmax])
    plt.ylim([ymin, ymax])

    # Setup x-axis ticks
    x_num_ticks = 5
    x_ticks = np.linspace(xmin, xmax, x_num_ticks)
        
    plt.xticks(x_ticks)
    plt.gca().set_xticklabels([f'{tick:.3f}' for tick in x_ticks])

    # Setup y-axis ticks
    y_num_ticks = 10
    y_ticks = np.linspace(ymin, ymax, y_num_ticks)
    plt.yticks(y_ticks)
    plt.gca().set_yticklabels([f'{tick:.3f}' for tick in y_ticks])


    # Make the axis and tics lines thicker
    for axis in ['top','bottom','left','right']:
        plt.gca().spines[axis].set_linewidth(1.5)
    plt.tick_params(axis='both', width=1.5, length=6)  # Width for tick thickness and length for tick size
        
    # Automatically adjusts layout for label visibility
    plt.tight_layout()

    plt.savefig(output_folder + filename)
    plt.clf() # Clean up before creating another plot
    plt.close()  # Explicitly close the figure to free memory


def main(file_phase_diagram_path, folder_output_plot):
    # Read Phase Diagram Data from file
    data = PhaseDiagramData(file_phase_diagram_path)

    # Plot the phase diagram
    suffix = extract_suffix(file_phase_diagram_path)
    phase_diagram_filename = 'phase_diagram_' + suffix + '.png'
    plot_phase_diagram(folder_output_plot, data, phase_diagram_filename)

    return True


if __name__ == "__main__":
    # Argument parsing
    parser = argparse.ArgumentParser(description="Plot phase diagram using fixed temperature files.")
    parser.add_argument('--file_phase_diagram_path', type=str, default='output/phase_diagram_akimaGSL.dat', help="Folder that will be used to output the phase diagram data.")
    parser.add_argument('--folder_output_plot', type=str, default='output/', help="Folder that will be used to output the phase diagram plot.")
    args = parser.parse_args()
    
    main(args.file_phase_diagram_path, args.folder_output_plot)
