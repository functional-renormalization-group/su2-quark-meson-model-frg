#!/bin/bash

echo "Script that builds and executes the C++ code that evaluates the Grand Potential of the SU(2) Quark Meson Model using the Functional Renormalization Group"
echo ""

cd .. && cd ..

make

cp bin/qmFRG.out tools/tests_standard_params_kIR80/input_data

cd tools/tests_standard_params_kIR80/input_data

./qmFRG.out use-config-file standard_kIR80_config.ini

rm qmFRG.out

cd ..