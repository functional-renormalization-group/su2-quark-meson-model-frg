#!/bin/bash

echo "Script that builds and plots the phase diagram using the folder input_data"
echo ""

python3 build_phase_diagram.py

python3 plot_phase_diagram.py
