import argparse
import os
import sys
import matplotlib.pyplot as plt
import numpy as np

# Get the parent directory of the current script (two levels up) and add the parent directory to sys.path
# This is necessary in order to import the PhaseTransitionAnalyzerAtFixedTemperature class
parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(parent_dir)

from phase_diagram.phase_diagram_builder import *


def main(folder_input_data, files_prefix, files_suffix, interpolation_method, folder_intermediary_plots, folder_output_data):
    build_phase_diagram_using_fixed_temperature_files(folder_input_data, files_prefix, files_suffix, interpolation_method, folder_intermediary_plots, folder_output_data)


if __name__ == "__main__":
    # Argument parsing
    parser = argparse.ArgumentParser(description="Build phase diagram using fixed temperature files.")
    parser.add_argument('--folder_input_data', type=str, default="input_data", help="Folder containing the input files.")
    parser.add_argument('--files_prefix', type=str, default="grandPotential_", help="Prefix for files that will be used to build the phase diagram.")
    parser.add_argument('--files_suffix', type=str, default="akimaGSL", help="Suffix present in the files that indicates the interpolation method used in the C++ code.")
    parser.add_argument('--interpolation_method', type=str, default='akima', help="Interpolation method used to analyze each file.")
    parser.add_argument('--folder_intermediary_plots', type=str, default='', help="Folder that will be used to output the intermediary plots.")
    parser.add_argument('--folder_output_data', type=str, default='output/', help="Folder that will be used to output the phase diagram data.")
    args = parser.parse_args()

    # Run the main function with the provided arguments
    main(args.folder_input_data, 
         args.files_prefix, 
         args.files_suffix, 
         args.interpolation_method, 
         args.folder_intermediary_plots, 
         args.folder_output_data)