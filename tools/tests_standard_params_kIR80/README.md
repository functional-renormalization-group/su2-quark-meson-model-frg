# Tests Using the Standard Parameter Set

## How to Build and Plot the Phase Diagram

### Step 1: Calculate the Grand Potential

The script run_grand_potential_calculation.sh is used to compile and run the C++ code responsible for evaluating the Grand Potential based on the SU(2) Quark Meson Model.
What it does:
1. Navigates to the root directory of the project.
2. Compiles the C++ code using make, and copies the resulting executable (qmFRG.out) to the input_data/ folder.
3. Runs the executable using the configuration file standard_kIR80_config.ini to compute the Grand Potential.
4. Removes the executable after the calculation is complete.

To run the script:
```
./run_grand_potential_calculation.sh
```

### Step 2: Build and Plot the Phase Diagram

Once the Grand Potential data has been generated, you can use run_phase_diagram_calculation.sh to build and plot the phase diagram. The phase diagram data will be saved in output/phase_diagram_akimaGSL.dat. The visual plot will be saved as output/phase_diagram_akimaGSL.png.

What it does:
1. Calls the Python script build_phase_diagram.py to process the Grand Potential data and generate the phase diagram data.
2. Then calls plot_phase_diagram.py to create a visual representation of the phase diagram.

To run the script:
```
./run_phase_diagram_calculation.sh
```

The obtained phase diagram is the following:
![Phase diagram](output/phase_diagram_akimaGSL.png)

