class PhaseDiagramData:
    def __init__(self, file_path):
        self.file_path = file_path
        self.spinodals_lower = []
        self.phase_transitions = []
        self.spinodals_upper = []
        self.temperatures = []

        try:
            with open(file_path, 'r') as file:
                # Skip the header line
                file.readline()

                # Read each remaining line in the file
                for line in file:
                    # Split the line by whitespace
                    values = line.split()
                    # Convert each value to float and append to respective lists
                    self.spinodals_lower.append(float(values[0]))
                    self.phase_transitions.append(float(values[1]))
                    self.spinodals_upper.append(float(values[2]))
                    self.temperatures.append(float(values[3]))
        except FileNotFoundError:
            print(f"ERROR: File {file_path} not found!")
        except Exception as error:
            print(f"An unexpected error occurred: {error}")
