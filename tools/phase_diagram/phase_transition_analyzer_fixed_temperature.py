import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline, Akima1DInterpolator
from scipy.optimize import root_scalar
from typing import List, Callable


# Interpolates given x and y lists based on the chosen interpolation method.
def interpolate_given_lists_and_method(x_list: List[float], y_list: List[float], interpolation_method: str) -> callable:
    # Perform interpolation based on the selected method
    if interpolation_method == 'cubic':
        interpolated_function = CubicSpline(x_list, y_list)
    elif interpolation_method == 'akima':
        interpolated_function = Akima1DInterpolator(x_list, y_list)
    else:
        raise ValueError(f"ERROR: Unsupported interpolation method: {interpolation_method}")

    return interpolated_function


class PhaseTransitionAnalyzerAtFixedTemperature:
    def __init__(self, filename, interpolation_method, folder_intermediary_plots):
        self.filename = filename
        self.interpolation_method = interpolation_method
        self.root_finding_absolute_error = 1E-8
        self.folder_intermediary_plots = folder_intermediary_plots

        # If the path for intermediary plots provided is empty change appropriate flag
        self.build_intermediary_plots = True
        if( folder_intermediary_plots=='' ):
            self.build_intermediary_plots = False

        # Boolean variable to assess if the calculation was valid
        self.is_calculation_valid = True

        # Variables used in all plots
        self.figure_size = 6 # size of the width and height of the plot in inches
        self.figure_ppi = 100

        # Print some information regarding the instance of the class
        print("Analysing file", self.filename)
        print("Interpolations are going to be made using the", self.interpolation_method, "method")

        # Initialize lists to store the data
        self.T_GeV = []
        self.Cpu_GeV = []
        self.Cpd_GeV = []
        self.Nextrema = []
        self.BranchID = []
        self.sigma_GeV = []
        self.Mquark_GeV = []
        self.Mpion2_GeV2 = []
        self.Msigma2_GeV2 = []
        self.Omega_GeV4 = []
        self.dTOmega_GeV3 = []
        self.dCpuOmega_GeV3 = []
        self.dCpdOmega_GeV3 = []

        try:
            with open(filename, "r") as file:
                # Skip the header line
                file.readline()
                # Read each remaining line in the file
                for line in file:
                    # Split the line by whitespace
                    values = line.split()
                    # Convert each value to float and append to respective lists
                    self.T_GeV.append(float(values[0]))
                    self.Cpu_GeV.append(float(values[1]))
                    self.Cpd_GeV.append(float(values[2]))
                    self.Nextrema.append(float(values[3]))
                    self.BranchID.append(float(values[4]))
                    self.sigma_GeV.append(float(values[5]))
                    self.Mquark_GeV.append(float(values[6]))
                    self.Mpion2_GeV2.append(float(values[7]))
                    self.Msigma2_GeV2.append(float(values[8]))
                    self.Omega_GeV4.append(float(values[9]))
                    self.dTOmega_GeV3.append(float(values[10]))
                    self.dCpuOmega_GeV3.append(float(values[11]))
                    self.dCpdOmega_GeV3.append(float(values[12]))
        except FileNotFoundError:
            print(f"ERROR: File {filename} not found!")
        except Exception as error:
            print(f"An unexpected error occurred: {error}")

        # Check if the temperature is constant in the file
        self.temperature_GeV = self.T_GeV[0]
        for temperature in self.T_GeV:
            if(temperature>self.temperature_GeV or temperature<self.temperature_GeV):
                print("Problem building PhaseTransitionAnalyzerAtFixedTemperature! The temperature is not fixed!")
                self.is_calculation_valid = False
        if( self.is_calculation_valid==True ):
            print("The temperature is fixed to", self.temperature_GeV, "GeV")

        # Check data points for increasing sigma field after sorting, this must be dealt with
        self.points_with_increasing_sigma = 0
        for i in range(0, len(self.sigma_GeV)-1):
            if( self.sigma_GeV[::-1][i]>self.sigma_GeV[::-1][i+1] ):
                self.points_with_increasing_sigma += 1
        
        # Create filtered lists of variables ensuring sigma field decreases monotonically with increasing up quark chemical potential.
        self.sigma_GeV_filtered = []
        self.Cpu_GeV_filtered = []
        self.Omega_GeV4_filtered = []
        if( self.points_with_increasing_sigma>0 ):
            print("WARNING: I have detected that the sigma field is increasing after being sorted! This happened for", self.points_with_increasing_sigma, "points! I will create filtered variables corresponding to a monotone sigma field.") 
            for i in range(0, len(self.sigma_GeV)-1):
                if( self.sigma_GeV[i]>self.sigma_GeV[i+1] ):
                    self.sigma_GeV_filtered.append(self.sigma_GeV[i])
                    self.Cpu_GeV_filtered.append(self.Cpu_GeV[i])
                    self.Omega_GeV4_filtered.append(self.Omega_GeV4[i])
             # Check the last point and add it if it respects the monotonicity
            if( self.sigma_GeV[-2] > self.sigma_GeV[-1] ):
                self.sigma_GeV_filtered.append(self.sigma_GeV[-1])
                self.Cpu_GeV_filtered.append(self.Cpu_GeV[-1])
                self.Omega_GeV4_filtered.append(self.Omega_GeV4[-1])
        else:
            self.sigma_GeV_filtered = self.sigma_GeV
            self.Cpu_GeV_filtered = self.Cpu_GeV
            self.Omega_GeV4_filtered = self.Omega_GeV4

        # Initialize output variables
        self.phase_transition_Cpu_GeV = -1.0 # Default to a negative number which cannot occur for the calculation to be valid
        self.phase_transition_sigma_GeV = []
        self.spinodals_sigma_GeV = []
        self.spinodals_Cpu_GeV = []
        self.spinodals_Omega_GeV4 = []


    def png_plot_Cpu_vs_Omega(self, folder):
        plt.figure(figsize=(self.figure_size, self.figure_size), dpi=self.figure_ppi)
        
        plt.plot(self.Cpu_GeV, self.Omega_GeV4, linestyle='-', color='red', linewidth=2.5)
        plt.xlabel(r'$\mu_q\,[\mathrm{GeV}]$', fontsize=16)
        plt.ylabel(r'$\Omega\,[\mathrm{GeV}^4]$', fontsize=16)

        # Set x and y limits to a given range
        xmin = min(self.Cpu_GeV)
        xmax = max(self.Cpu_GeV)
        plt.xlim([xmin, xmax])

        # Setup x-axis ticks
        x_num_ticks = 6
        x_ticks = np.linspace(xmin, xmax, x_num_ticks)
        
        plt.xticks(x_ticks)
        plt.gca().set_xticklabels([f'{tick:.4f}' for tick in x_ticks])
        
        # Setup y-axis ticks
        y_num_ticks = 6 
        y_ticks = np.linspace(min(self.Omega_GeV4), max(self.Omega_GeV4), y_num_ticks)
        plt.yticks(y_ticks)
        plt.gca().set_yticklabels([f'{tick:.10f}' for tick in y_ticks])

        plt.title('T='+str(self.temperature_GeV)+'GeV', fontsize=16)
        plt.grid(True)

        # Make the axis and tics lines thicker
        for axis in ['top','bottom','left','right']:
            plt.gca().spines[axis].set_linewidth(1.5)
        plt.tick_params(axis='both', width=1.5, length=6)  # Width for tick thickness and length for tick size
        
        # Automatically adjusts layout for label visibility
        plt.tight_layout()

        plot_name = 'Cpu_vs_Omega_plot_T'+str(self.temperature_GeV)+'.png'
        plt.savefig(folder + plot_name)
        plt.clf() # Clean up before creating another plot
        plt.close()  # Explicitly close the figure to free memory


    def png_plot_Cpu_vs_Omega_zoom_transition(self, folder):
        if( len(self.spinodals_Cpu_GeV)>0 and len(self.spinodals_Omega_GeV4)>0 ):
            plt.figure(figsize=(self.figure_size, self.figure_size), dpi=self.figure_ppi)
            
            plt.plot(self.Cpu_GeV, self.Omega_GeV4, linestyle='-', color='red', linewidth=2.5)
            plt.xlabel(r'$\mu_q\,[\mathrm{GeV}]$', fontsize=16)
            plt.ylabel(r'$\Omega\,[\mathrm{GeV}^4]$', fontsize=16)

            # Set x and y limits to a given range
            xmin = min(self.spinodals_Cpu_GeV)
            xmax = max(self.spinodals_Cpu_GeV)
            ymin = min(self.spinodals_Omega_GeV4)
            ymax = max(self.spinodals_Omega_GeV4)
            plt.xlim([xmin, xmax])
            plt.ylim([ymin, ymax])

            # Setup x-axis ticks
            x_num_ticks = 6
            x_ticks = np.linspace(xmin, xmax, x_num_ticks)
            
            plt.xticks(x_ticks)
            plt.gca().set_xticklabels([f'{tick:.4f}' for tick in x_ticks])
            
            # Setup y-axis ticks
            y_num_ticks = 6 
            y_ticks = np.linspace(ymin, ymax, y_num_ticks)
            plt.yticks(y_ticks)
            plt.gca().set_yticklabels([f'{tick:.10f}' for tick in y_ticks])

            plt.title('T='+str(self.temperature_GeV)+'GeV', fontsize=16)
            plt.grid(True)

            # Make the axis and tics lines thicker
            for axis in ['top','bottom','left','right']:
                plt.gca().spines[axis].set_linewidth(1.5)
            plt.tick_params(axis='both', width=1.5, length=6)  # Width for tick thickness and length for tick size

            # Automatically adjusts layout for label visibility
            plt.tight_layout()

            plot_name = 'Cpu_vs_Omega_plot_T'+str(self.temperature_GeV)+'_zoom.png'
            plt.savefig(folder + plot_name)
            plt.clf() # Clean up before creating another plot
            plt.close()  # Explicitly close the figure to free memory
        
        else:
            print("ERROR: I was not able to build the Cpu_vs_Omega zoomed! I am lacking the calculation of certain values!")


    def png_plot_Cpu_vs_Omega_plot_branches(self, branch_1_Cpu_GeV, branch_1_Omega_GeV4, branch_2_Cpu_GeV, branch_2_Omega_GeV4, plot_name):
        plt.figure(figsize=(self.figure_size, self.figure_size), dpi=self.figure_ppi)
        
        plt.plot(branch_1_Cpu_GeV, branch_1_Omega_GeV4, marker='o', linestyle='-', color='red', linewidth=2.5, markersize=4)
        plt.plot(branch_2_Cpu_GeV, branch_2_Omega_GeV4, marker='o', linestyle='-', color='blue', linewidth=2.5, markersize=4)
        plt.xlabel(r'$\mu_q\,[\mathrm{GeV}]$', fontsize=16)
        plt.ylabel(r'$\Omega\,[\mathrm{GeV}^4]$', fontsize=16)

        plt.title('T='+str(self.temperature_GeV)+'GeV', fontsize=16)
        plt.grid(True)

        # Make the axis and tics lines thicker
        for axis in ['top','bottom','left','right']:
            plt.gca().spines[axis].set_linewidth(1.5)
        plt.tick_params(axis='both', width=1.5, length=6)  # Width for tick thickness and length for tick size
        
        # Automatically adjusts layout for label visibility
        plt.tight_layout()

        plt.savefig(self.folder_intermediary_plots + plot_name)
        plt.clf() # Clean up before creating another plot
        plt.close()  # Explicitly close the figure to free memory


    def calculate_phase_transition_Cpu_GeV(self):
        # Check if there are at least 5 points inside the phase transition. A point inside the phase transition corresponds to 3 solutions, thus the division by 3!
        number_extrema_equal_1 = 0
        number_extrema_equal_3 = 0
        for number in self.Nextrema:
            if(number==1):
                number_extrema_equal_1 += 1
            elif(number==3):
                number_extrema_equal_3 +=1
        if( int(number_extrema_equal_3/3)<5 ):
            print("ERROR: Not enough points with inside the phase transition in order to define it!")
            self.is_calculation_valid = False
            return
        
        # check if there are enough chemical potentials in order to proceed with the calculation
        if( len(self.Cpu_GeV) < 2 ):
            print("ERROR: Not enough data points to proceed with calculations! Less then 3 chemical potentials (up quark)")
            self.is_calculation_valid = False
            return

        # Create two lists to separate branches in the Gibbs construction
        branch_1_Cpu_GeV = []
        branch_2_Cpu_GeV = []
        branch_1_Omega_GeV4 = []
        branch_2_Omega_GeV4 = []

        in_branch_1 = True
        in_branch_2 = False
        for i in range(1, len(self.Cpu_GeV)-2):
            if( self.Cpu_GeV[i]==self.Cpu_GeV[i-1] and in_branch_1 ):
                in_branch_1 = False
            
            if( in_branch_1 ):
                branch_1_Cpu_GeV.append(self.Cpu_GeV[i])
                branch_1_Omega_GeV4.append(self.Omega_GeV4[i])

            if( self.Cpu_GeV[i]==self.Cpu_GeV[i+1] and not in_branch_1 ):
                in_branch_2 = True
            
            if( in_branch_2 ):
                branch_2_Cpu_GeV.append(self.Cpu_GeV[i+1])
                branch_2_Omega_GeV4.append(self.Omega_GeV4[i+1])

        # Build plot for branches that will be used to get the transition point
        if(self.build_intermediary_plots==True):
            plot_name = 'Cpu_vs_Omega_plot_branches_T'+str(self.temperature_GeV)+'.png'
            self.png_plot_Cpu_vs_Omega_plot_branches(branch_1_Cpu_GeV, branch_1_Omega_GeV4, 
                                                     branch_2_Cpu_GeV, branch_2_Omega_GeV4, 
                                                     plot_name)

        # Calculate the minimum and maximum in the X pattern and subtract branches
        ini_Cpu_GeV = 0
        if( branch_1_Cpu_GeV[0]<=branch_2_Cpu_GeV[0] ):
            ini_Cpu_GeV = branch_2_Cpu_GeV[0]
        elif( branch_2_Cpu_GeV[0]<branch_1_Cpu_GeV[0] ):
            ini_Cpu_GeV = branch_1_Cpu_GeV[0]

        fin_Cpu_GeV = 0
        if( branch_1_Cpu_GeV[len(branch_1_Cpu_GeV)-1]<=branch_2_Cpu_GeV[len(branch_2_Cpu_GeV)-1] ):
            fin_Cpu_GeV = branch_1_Cpu_GeV[len(branch_1_Cpu_GeV)-1]
        elif( branch_2_Cpu_GeV[len(branch_2_Cpu_GeV)-1]<branch_1_Cpu_GeV[len(branch_1_Cpu_GeV)-1] ):
            fin_Cpu_GeV = branch_2_Cpu_GeV[len(branch_2_Cpu_GeV)-1]

        cut_branch_1_Cpu_GeV = []
        cut_branch_1_Omega_GeV4 = []
        for i in range(0, len(branch_1_Cpu_GeV)):
            if( branch_1_Cpu_GeV[i]>=ini_Cpu_GeV and branch_1_Cpu_GeV[i]<=fin_Cpu_GeV ):
                cut_branch_1_Cpu_GeV.append(branch_1_Cpu_GeV[i])
                cut_branch_1_Omega_GeV4.append(branch_1_Omega_GeV4[i])
            
        cut_branch_2_Cpu_GeV = []
        cut_branch_2_Omega_GeV4 = []
        for i in range(0, len(branch_2_Cpu_GeV)):
            if( branch_2_Cpu_GeV[i]>=ini_Cpu_GeV and branch_2_Cpu_GeV[i]<=fin_Cpu_GeV ):
                cut_branch_2_Cpu_GeV.append(branch_2_Cpu_GeV[i])
                cut_branch_2_Omega_GeV4.append(branch_2_Omega_GeV4[i])

        # Build plot for branches that will be used to get the transition point after cutting
        if(self.build_intermediary_plots==True):
            plot_name = 'Cpu_vs_Omega_plot_cut_branches_T' + str(self.temperature_GeV) + '.png'
            self.png_plot_Cpu_vs_Omega_plot_branches(cut_branch_1_Cpu_GeV, cut_branch_1_Omega_GeV4, 
                                                     cut_branch_2_Cpu_GeV, cut_branch_2_Omega_GeV4, 
                                                     plot_name)

        branch_subtraction_Omega_GeV4  = []
        for i in range (0, len(cut_branch_1_Cpu_GeV)):
            branch_subtraction_Omega_GeV4.append( cut_branch_1_Omega_GeV4[i] - cut_branch_2_Omega_GeV4[i] )

        # Create interpolation function and find its zero
        x_list = cut_branch_1_Cpu_GeV
        y_list = branch_subtraction_Omega_GeV4
        interpolated_function = interpolate_given_lists_and_method(x_list, y_list, self.interpolation_method)

        lower_bound = cut_branch_1_Cpu_GeV[0]
        upper_bound = cut_branch_1_Cpu_GeV[len(cut_branch_1_Cpu_GeV)-1]
        result = root_scalar(interpolated_function, bracket=[lower_bound, upper_bound], method='brentq', xtol=self.root_finding_absolute_error)

        if(result.converged):
            self.phase_transition_Cpu_GeV = result.root
            print("The phase transition chemical potential (up quark) is [GeV] =", self.phase_transition_Cpu_GeV)
        else:
            print("ERROR: I was not able to find phase transition chemical potential (up quark) this temperature!")
            self.is_calculation_valid = False
            return


    def calculate_phase_transition_sigma_GeV(self):
        if( self.phase_transition_Cpu_GeV<0 ):
            print("ERROR: The phase transition chemical potential is negative! Something went wrong! I will not be able to calculate the sigma field at the transition!")
            self.is_calculation_valid = False
            return

        # Create list containing the subtraction of the list of chemical potentials with the chemical potential at the transition
        Cpu_minus_Cpu_at_transition_GeV = []
        for i in range (0, len(self.Cpu_GeV_filtered)):
            Cpu_minus_Cpu_at_transition_GeV.append(self.Cpu_GeV_filtered[i] - self.phase_transition_Cpu_GeV)

        # Find the indexes at which the previous list changes sign: these correspond 
        index_before_change_of_sign = []
        for i in range(0, len(Cpu_minus_Cpu_at_transition_GeV)-1):
            if( Cpu_minus_Cpu_at_transition_GeV[i]>0 and Cpu_minus_Cpu_at_transition_GeV[i+1]<0 ):
                index_before_change_of_sign.append(i)
            elif( Cpu_minus_Cpu_at_transition_GeV[i]<0 and Cpu_minus_Cpu_at_transition_GeV[i+1]>0 ):
                index_before_change_of_sign.append(i)

        # Create interpolation of the sigma field versus the subtraction list created above
        x_list = self.sigma_GeV_filtered[::-1]
        y_list = Cpu_minus_Cpu_at_transition_GeV[::-1]
        interpolated_function = interpolate_given_lists_and_method(x_list, y_list, self.interpolation_method)

        # Using the indexes at which the list changes sign, find the value of the sigma field at which the subtraction is zero
        for i in range (0, len(index_before_change_of_sign)):
            lower_bound_index = index_before_change_of_sign[i]
            upper_bound_index = lower_bound_index + 1
            lower_bound = self.sigma_GeV_filtered[lower_bound_index]
            upper_bound = self.sigma_GeV_filtered[upper_bound_index]
            result = root_scalar(interpolated_function, bracket=[lower_bound, upper_bound], method='brentq', xtol=self.root_finding_absolute_error)
            if(result.converged):
                self.phase_transition_sigma_GeV.append(result.root)
            else:
                print("ERROR: I was not able to find the value of the sigma field for the phase transition!")
                self.is_calculation_valid = False
                return
        
        print("The values of the sigma field at the phase transition are [GeV]:", self.phase_transition_sigma_GeV)
        
        # Check that exaclty 3 values for the sigma field were obtained
        if( len(self.phase_transition_sigma_GeV)!=3 ):
            print("ERROR: More than 3 values for the sigma field at the phase transition were obtained!")
            self.is_calculation_valid = False
            return


    def calculate_spinodals_sigma_GeV(self):
        # Interpolate sigma vs chemical potential
        x_list = self.sigma_GeV_filtered[::-1]
        y_list = self.Omega_GeV4_filtered[::-1]
        interpolated_function = interpolate_given_lists_and_method(x_list, y_list, self.interpolation_method)

        # Calculate the derivative of the interpolated function
        derivative_interpolated_function = interpolated_function.derivative()

        # Search for the index exactly before the extrema point by calculating the derivative
        index_before_extrema = []
        for i in range(0, len(self.sigma_GeV_filtered)-1):
            if( derivative_interpolated_function(self.sigma_GeV_filtered[i])>0 and derivative_interpolated_function(self.sigma_GeV_filtered[i+1])<0):
                index_before_extrema.append(i)
            elif( derivative_interpolated_function(self.sigma_GeV_filtered[i])<0 and derivative_interpolated_function(self.sigma_GeV_filtered[i+1])>0):
                index_before_extrema.append(i)

        # Use root finding algorithm to search for the zero of the derivative, i.e., the extrema point
        for i in range(0, len(index_before_extrema)):
            lower_bound_index = index_before_extrema[i]
            upper_bound_index = lower_bound_index + 1
            lower_bound = self.sigma_GeV_filtered[lower_bound_index]
            upper_bound = self.sigma_GeV_filtered[upper_bound_index]
            result = root_scalar(derivative_interpolated_function, bracket=[lower_bound, upper_bound], method='brentq', xtol=self.root_finding_absolute_error)
            if(result.converged):
                self.spinodals_sigma_GeV.append(result.root)
            else:
                print("ERROR: I was not able to calculate the sigma field of the spinodals!")
                self.is_calculation_valid = False
                return
        
        print("The sigma field of the spinodals are [GeV]:", self.spinodals_sigma_GeV)
        
        # Check that exaclty 2 values for the sigma field were obtained
        if( len(self.spinodals_sigma_GeV)!=2 ):
            print("ERROR: More than 2 values for the sigma field at the spinodals were obtained!")
            self.is_calculation_valid = False
            return


    def calculate_spinodals_Cpu_GeV(self):
        # Check if the sigma field of the spinodals were calculated
        if( len(self.spinodals_sigma_GeV)==0 ):
            print("ERROR: The list of sigma fields at the spinodals is empty! I will not be able to calculate the chemical potential (up quark) of the spinodals!")
            self.is_calculation_valid = False
            return

        # Interpolate sigma vs chemical potential
        x_list = self.sigma_GeV_filtered[::-1]
        y_list = self.Cpu_GeV_filtered[::-1]
        interpolated_function = interpolate_given_lists_and_method(x_list, y_list, self.interpolation_method)
        self.spinodals_Cpu_GeV = sorted( interpolated_function(self.spinodals_sigma_GeV) )

        print("The chemical potential (up quark) of the spinodals are [GeV]:", self.spinodals_Cpu_GeV)

        # Check that exaclty 2 values for the chemical potential were obtained
        if( len(self.spinodals_Cpu_GeV)!=2 ):
            print("ERROR: More than 2 values for the chemical potential (up quark) at the spinodals were obtained!")
            self.is_calculation_valid = False
            return


    def calculate_spinodals_Omega_GeV4(self):
        # Check if the sigma field of the spinodals were calculated
        if( len(self.spinodals_sigma_GeV)==0 ):
            print("ERROR: The list of sigma fields at the spinodals is empty! I will not be able to calculate the grand potential of the spinodals!")
            self.is_calculation_valid = False
            return
        
        # Interpolate sigma vs omega
        x_list = self.sigma_GeV_filtered[::-1]
        y_list = self.Omega_GeV4_filtered[::-1]
        interpolated_function = interpolate_given_lists_and_method(x_list, y_list, self.interpolation_method)
        self.spinodals_Omega_GeV4 = sorted( interpolated_function(self.spinodals_sigma_GeV) )

        print("The grand potential of the spinodals are [GeV]:", self.spinodals_Omega_GeV4)

        # Check that exaclty 2 values for the Omega were obtained
        if( len(self.spinodals_Omega_GeV4)!=2 ):
            print("ERROR: More than 2 values for the Omega at the spinodals were obtained!")
            self.is_calculation_valid = False
            return
                    

    def calculate_all(self, build_Cpu_vs_Omega_plots=False):
        # Only calculate stuff if the file indeed has the temperature fixed
        if( self.is_calculation_valid==True ):
            # Calculate all the quantities of interest
            self.calculate_phase_transition_Cpu_GeV()
            self.calculate_phase_transition_sigma_GeV()
            self.calculate_spinodals_sigma_GeV()
            self.calculate_spinodals_Cpu_GeV()
            self.calculate_spinodals_Omega_GeV4()

        if(self.is_calculation_valid == True and build_Cpu_vs_Omega_plots==True):
            self.png_plot_Cpu_vs_Omega(self.folder_intermediary_plots)
            self.png_plot_Cpu_vs_Omega_zoom_transition(self.folder_intermediary_plots)
        

