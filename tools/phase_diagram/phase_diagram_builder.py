import os
from phase_diagram.phase_transition_analyzer_fixed_temperature import *


# Returns a list of files in a folder that start with a given prefix.
def get_files_with_prefix(folder: str, prefix_word: str) -> List[str]:    
    files = [f for f in os.listdir(folder) if f.startswith(prefix_word)]
    if not files:
        print("ERROR: No files found with the specified prefix.")
        return []
    
    return files


# Extract the suffix of file, followed by an underscore but before the final dot
def extract_suffix(filename: str) -> str:
    if '_' not in filename or '.' not in filename:
        print(f"ERROR: Invalid file format: '{filename}'")
        return ""
    
    suffix = filename.split('_')[-1].split('.')[0]
    
    return suffix


# Check if a provided list of strings all have the same suffix
def check_file_suffix_consistency(files: List[str]) -> bool:
    # Extract suffix from the first file
    suffix = extract_suffix(files[0])

    # Cycle the files to check suffix
    mismatched_files = []  # To store files with a different suffix
    for file in files:
        file_suffix = extract_suffix(file)

        # Check if the file has a different suffix
        if file_suffix != suffix:
            mismatched_files.append(file)
        
    # If there are mismatched files, report them
    if mismatched_files:
        print(f"ERROR: The following files do not have the same suffix '{suffix}':")
        for file in mismatched_files:
            print(file)
        return False

    return True


# Returns a list of files in a folder that start with a given prefix and have a certain suffix (the suffix is followed by an underscore but before the final dot)
def get_files_with_prefix_suffix(folder: str, prefix_word: str, suffix_word: str) -> List[str]:    
    files = []
    for file in os.listdir(folder):
        if ( file.startswith(prefix_word) ):
            if ( extract_suffix(file)==suffix_word ):
                files.append(file)

    if not files:
        print("ERROR: No files found with the specified prefix and suffix.")
        return []
    
    return files


# Function that receives a list of PhaseTransitionAnalyzerAtFixedTemperature objects and outputs the a file containing the phase diagram data
def write_phase_diagram_data_file(phase_diagram_file_path: str, phase_transition_data: List[PhaseTransitionAnalyzerAtFixedTemperature]) -> bool:
    with open(phase_diagram_file_path, "w") as file:
        # Define the column headers with justified widths
        header = f"{'Lower Spinodal Cpu [GeV]':<30}{'Phase Transition Cpu [GeV]':<30}{'Upper Spinodal Cpu [GeV]':<30}{'Temperature [GeV]':<20}"
        file.write(header + "\n")
        print(header)

        # Loop through the data and format it accordingly
        for data in phase_transition_data:
            if ( data.is_calculation_valid==True ):
                # Justify each value to a specific width to maintain alignment
                output_string = f"{data.spinodals_Cpu_GeV[0]:<30}{data.phase_transition_Cpu_GeV:<30}{data.spinodals_Cpu_GeV[1]:<30}{data.temperature_GeV:<20}"
                file.write(output_string + "\n")
                print(output_string)

    return True


# Function that analyzes the fixed temperature files provided in a given folder and returns a file with the phase diagram data.
def build_phase_diagram_using_fixed_temperature_files(folder_input_data: str, files_prefix: str, files_suffix: str, interpolation_method: str, folder_intermediary_plots: str, folder_output_data: str) -> bool:
    print("\nBuilding the phase diagram using fixed temperature files...\n")

    # Ensure the folder exists
    if not os.path.exists(folder_input_data):
        print(f"ERROR: The folder '{folder_input_data}' does not exist.")
        return
    
    # Get the files with the specified prefix and suffix
    files = get_files_with_prefix_suffix(folder_input_data, files_prefix, files_suffix)

    if files:
        print("Files that will be used to build the phase diagram:")
        for file in files:
            print(file)
        print("\n")

        # Aanalyse each file, calculating all the necessary information
        phase_transition_data = []
        for file in files:
            file_path = os.path.join(folder_input_data, file)
            analyzer = PhaseTransitionAnalyzerAtFixedTemperature(file_path, interpolation_method, folder_intermediary_plots)
            analyzer.calculate_all()
            phase_transition_data.append(analyzer)
            print("\n")

        # Open file to output the phase diagram data
        phase_diagram_file_path = os.path.join(folder_output_data, "phase_diagram_" + files_suffix + ".dat")
        write_phase_diagram_data_file(phase_diagram_file_path, phase_transition_data)

        return True
    
    else:
        print("ERROR: No files have been provided to builder!")
        return

