#include <cmath>
#include <iostream>
#include <cstdlib>
#include <vector>
#include "LinearInterpolation.h"
#include "LinearInterpolationTests.h"

using namespace std;


double LinearInterpolationTests::linearPolynomial(double m, double b, double x)
{
	double y = m*x + b;
	return y;
}

LinearInterpolationTests::TestResult LinearInterpolationTests::testSlopeParameter(double xA, double yA, double xB, double yB, double expected, double absError) 
{
    LinearInterpolation interp(xA, yA, xB, yB);
	double result = interp.slopeParameter();
	if ( fabs( (result-expected) )<absError ) 
	{
        std::cout << "testSlopeParameter PASSED\n";
		return PASSED;
    } 
	else 
	{
        std::cout << "testSlopeParameter FAILED: Expected " << expected << ", got " << result << ", the difference is: " << fabs( (result-expected) ) << "\n";
		return FAILED;
    }
}

LinearInterpolationTests::TestResult LinearInterpolationTests::testYInterceptParameter(double xA, double yA, double xB, double yB, double expected, double absError) 
{
    LinearInterpolation interp(xA, yA, xB, yB);
    double result = interp.yInterceptParameter();
    if ( fabs( (result-expected) )<absError ) 
	{
        std::cout << "testYInterceptParameter PASSED\n";
		return PASSED;
    } 
	else 
	{
        std::cout << "testYInterceptParameter FAILED: Expected " << expected << ", got " << result << ", the difference is: " << fabs( (result-expected) ) << "\n";
		return FAILED;
    }
}

LinearInterpolationTests::TestResult LinearInterpolationTests::testEvaluate(double xA, double yA, double xB, double yB, double x, double expected, double absError) 
{
    LinearInterpolation interp(xA, yA, xB, yB);
    double result = interp.evaluate(x);
    if ( fabs( (result-expected) )<absError ) 
	{
        std::cout << "testEvaluate PASSED\n";
		return PASSED;
    } 
	else 
	{
        std::cout << "testEvaluate FAILED: Expected " << expected << ", got " << result << ", the difference is: " << fabs( (result-expected) ) << "\n";
		return FAILED;
    }
}

LinearInterpolationTests::TestResult LinearInterpolationTests::testEvaluateZeroCrossing(double xA, double yA, double xB, double yB, double expected, double absError) 
{
 	LinearInterpolation interp(xA, yA, xB, yB);
	double result = interp.evaluateZeroCrossing();
    if ( fabs( (result-expected) )<absError ) 
	{
        std::cout << "testEvaluateZeroCrossing PASSED\n";
		return PASSED;
    } 
	else 
	{
    	std::cout << "testEvaluateZeroCrossing FAILED: Expected " << expected << ", got " << result << ", the difference is: " << fabs( (result-expected) ) << "\n";
		return FAILED;
	}
}

LinearInterpolationTests::TestResult LinearInterpolationTests::testEvaluateNoZeroCrossing(double xA, double yA, double xB, double yB) 
{
    LinearInterpolation interp(xA, yA, xB, yB);
    double result = interp.evaluateZeroCrossing();
    if ( std::isinf(result) ) 
	{
        std::cout << "testEvaluateNoZeroCrossing PASSED\n";
		return PASSED;
    } 
	else 
	{
        std::cout << "testEvaluateNoZeroCrossing FAILED: Expected infinity, got " << result << "\n";
		return FAILED;
    }
}

LinearInterpolationTests::TestResult LinearInterpolationTests::testBoundaryCases(double xA, double yA, double xB, double yB) 
{	
	// Test with points where xA == xB, should handle division by zero
	TestResult result_test1;
    try 
	{
        LinearInterpolation interp(xA, yA, xA, yB);
        interp.slopeParameter();
        std::cout << "testBoundaryCases FAILED: Expected exception for identical x values.\n";
		result_test1 = FAILED;
    } 
	catch (...) 
	{
        std::cout << "testBoundaryCases PASSED for identical x values\n";
		result_test1 = PASSED;
    }

    // Test where yA == yB, no zero crossing
	TestResult result_test2;
    LinearInterpolation interp2(xA, yA, xB, yA);
    double result = interp2.evaluateZeroCrossing();
    if ( std::isinf(result) ) 
	{
        std::cout << "testBoundaryCases PASSED for identical y values\n";
		result_test2 = PASSED;
    } 
	else 
	{
        std::cout << "testBoundaryCases FAILED: Expected infinity, got " << result << "\n";
		result_test2 = FAILED;
    }

	if ( result_test1==PASSED && result_test2==PASSED )
	{
		return PASSED;
	}
	else
	{
		return FAILED;
	}
}

LinearInterpolationTests::TestResult LinearInterpolationTests::testAllHardcodedExample()
{	
	double absError = 1E-12;
	double xA, yA, xB, yB, expected;

	xA = 1.0; yA = 2.0; xB = 3.0; yB = 4.0;
	expected = 1.0;
    TestResult result_testSlopeParameter = testSlopeParameter(xA, yA, xB, yB, expected, absError);

	xA = 1.0; yA = 2.0; xB = 3.0; yB = 4.0;
	expected = 1.0;
    TestResult result_testYInterceptParameter = testYInterceptParameter(xA, yA, xB, yB, expected, absError);

	double x = 2.0;
	xA = 1.0; yA = 2.0; xB = 3.0; yB = 4.0;
	expected = 3.0;
    TestResult result_testEvaluate = testEvaluate(xA, yA, xB, yB, x, expected, absError);
    
	xA = 1.0; yA = -1.0; xB = 3.0; yB = 1.0;
	expected = 2.0;
	TestResult result_testEvaluateZeroCrossing = testEvaluateZeroCrossing(xA, yA, xB, yB, expected, absError);

	xA = 1.0; yA = 2.0; xB = 3.0; yB = 4.0;
	TestResult result_testEvaluateNoZeroCrossing = testEvaluateNoZeroCrossing(xA, yA, xB, yB);

	xA = 1.0; yA = 2.0; xB = 3.0; yB = 4.0;
    TestResult result_testBoundaryCases = testBoundaryCases(xA, yA, xB, yB);

	if ( result_testSlopeParameter==PASSED && 
		 result_testYInterceptParameter==PASSED &&
		 result_testEvaluate==PASSED &&
		 result_testEvaluateZeroCrossing==PASSED &&
		 result_testEvaluateNoZeroCrossing==PASSED &&
		 result_testBoundaryCases==PASSED )
	{	
		std::cout << "testAllHardcodedExample PASSED" << "\n";
		return PASSED;
	}
	else
	{	
		std::cout << "testAllHardcodedExample FAILED" << "\n";
		return FAILED;
	}
}

LinearInterpolationTests::TestResult LinearInterpolationTests::testAllGivenParameters(double m, double b, double delta, double alpha, double xToEvaluateAt, double absError)
{
	//validate that m is not zero
	bool mDiffZero = (m<0 || m>0);
	if ( !mDiffZero )
	{ 
		cout << "The parameter m cannot be equal to zero! It must be a first order polynomial! Aborting!\n";
		abort();
	}

	//validate that delta is bigger than zero
	if ( delta<=0 )
	{ 	
		cout << "The parameter delta must be bigger than zero! Aborting!\n";
		abort();
	}

	//validate that alpha in ]0,1[
	if( !(alpha>0 && alpha<1) )
	{
		cout << "The parameter alpha must belong to the interval ]0,1[! Aborting!\n";
		abort();
	}

	//evaluate the root of the linear equation x0 = -b/m
	double x0 = -b/m;

	//build the values of xA, xB and xBTilde from root and parameters
	double xA = x0 - delta;
	double xB = x0 + alpha*delta;
	double xBTilde = x0 - alpha*delta;

	double yA = linearPolynomial(m, b, xA);
	double yB = linearPolynomial(m, b, xB);
	double yBTilde = linearPolynomial(m, b, xBTilde);

	TestResult result_testSlopeParameter = testSlopeParameter(xA, yA, xB, yB, m, absError);
	TestResult result_testYInterceptParameter = testYInterceptParameter(xA, yA, xB, yB, b, absError);
	TestResult result_testEvaluate = testEvaluate(xA, yA, xB, yB, xToEvaluateAt, linearPolynomial(m, b, xToEvaluateAt), absError);
	TestResult result_testEvaluateZeroCrossing = testEvaluateZeroCrossing(xA, yA, xB, yB, x0, absError);
	TestResult result_testEvaluateNoZeroCrossing = testEvaluateNoZeroCrossing(xA, yA, xBTilde, yBTilde);
	TestResult result_testBoundaryCases = testBoundaryCases(xA, yA, xB, yB);

	if ( result_testSlopeParameter==PASSED && 
		 result_testYInterceptParameter==PASSED &&
		 result_testEvaluate==PASSED &&
		 result_testEvaluateZeroCrossing==PASSED &&
		 result_testEvaluateNoZeroCrossing==PASSED &&
		 result_testBoundaryCases==PASSED )
	{	
		std::cout << "testAllGivenParameters PASSED" << "\n";
		return PASSED;
	}
	else
	{	
		std::cout << "testAllGivenParameters FAILED" << "\n";
		return FAILED;
	}
}

LinearInterpolationTests::TestResult LinearInterpolationTests::testAllGivenParametersHardcoded(double absError)
{	
	double m, b, delta, alpha, xToEvaluateAt;

	m = 0.5;
	b = 2.0;
	delta = 1.0;
	alpha = 0.5;
	xToEvaluateAt = 4.0;

	return testAllGivenParameters(m, b, delta, alpha, xToEvaluateAt, absError);
}

LinearInterpolationTests::TestResult LinearInterpolationTests::testAllGivenParametersRandom(int numberOfTests, double absError)
{	
	srand(static_cast<unsigned int>(time(0))); // Seed the random number generator

	std::vector<TestResult> result;
	std::vector<std::vector<double>> resultParameters(numberOfTests);
	
	for (int i = 0; i < numberOfTests; i++)
	{
		double mSign = + 1;
		double bSign = + 1;
		double xToEvaluateAtSign = + 1;
		if( (rand() % 2)==0 ){ mSign = -1.0; }
		if( (rand() % 2)==0 ){ bSign = -1.0; }
		if( (rand() % 2)==0 ){ xToEvaluateAtSign = -1.0; }
		
		double m = mSign*( rand() % 10 + ( 1.0 + rand() % 99 )/1E2 );
		double b = bSign*( rand() % 10 + ( 1.0 + rand() % 99 )/1E2 );
		double delta = ( rand() % 10 + ( 1.0 + rand() % 99 )/1E2 );
		double alpha = ( 1.0 + rand() % 99999 )/1E5;
		double xToEvaluateAt = xToEvaluateAtSign*( rand() % 10 + ( 1.0 + rand() % 99 )/1E2 );
		resultParameters[i].push_back(m);
		resultParameters[i].push_back(b);
		resultParameters[i].push_back(delta);
		resultParameters[i].push_back(alpha);
		resultParameters[i].push_back(xToEvaluateAt);

		std::cout << "m=" << resultParameters[i][0] << "\n";
		std::cout << "b=" << resultParameters[i][1] << "\n";
		std::cout << "delta=" << resultParameters[i][2] << "\n";
		std::cout << "alpha=" << resultParameters[i][3] << "\n";
		std::cout << "xToEvaluateAt=" << resultParameters[i][4] << "\n";

		result.push_back( testAllGivenParameters(m, b, delta, alpha, xToEvaluateAt, absError) );
	}

	TestResult totalResult = PASSED;
	for (int i = 0; i < int(result.size()); i++)
	{	
		if ( result[i]==PASSED )
		{
			totalResult = PASSED;
		}
		else
		{
			totalResult = FAILED;
			break;
		}
	}

	if ( totalResult==FAILED )
	{	
		std::cout << "testAllGivenParametersRandom FAILED! It failed for the following cases: \n";

		for (int i = 0; i < int(result.size()); i++)
		{	
			if ( result[i]==FAILED )
			{
				std::cout << "m=" << resultParameters[i][0] << "\n";
				std::cout << "b=" << resultParameters[i][1] << "\n";
				std::cout << "delta=" << resultParameters[i][2] << "\n";
				std::cout << "alpha=" << resultParameters[i][3] << "\n";
				std::cout << "xToEvaluateAt=" << resultParameters[i][4] << "\n";
			}
		}
	}
	else
	{
		std::cout << "testAllGivenParametersRandom PASSED for " << numberOfTests << " random cases!\n";
	}
	
	return totalResult;
}
