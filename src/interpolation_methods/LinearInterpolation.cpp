#include <cmath>
#include <iostream>
#include <cstdlib>
#include <vector>
#include "LinearInterpolation.h"

using namespace std;


double LinearInterpolation::slopeParameter()
{	
	double m = ( yA - yB )/( xA - xB );

	return m;
}

double LinearInterpolation::yInterceptParameter()
{	
	double b = ( -xB*yA + xA*yB )/( xA - xB );

	return b;
}

double LinearInterpolation::evaluate(double x)
{	
	double m = slopeParameter();
	double b = yInterceptParameter();

	double y = m*x + b;

	return y;
}

// x = -(b/m) = ( xB*yA - xA*yB )/( yA - yB )
double LinearInterpolation::evaluateZeroCrossing()
{	
	double x = 1.0/0.0;

	// In order to have a zero crossing, yA and yB cannot be equal
	if ( yA<yB || yA>yB )
	{
		if ( yA*yB>0 ) //case where there is no obvious zero crossing, since both endpoints have the same sign
		{	
			//cout << "Region in-between data points provided to Liner Interpolation class have the same sign! Returning inf for the zero crossing.\n";
			x = 1.0/0.0;
		}
		else if ( yA*yB<0 ) //case when there is a zero crossing, since the endpoints change sign
		{
			x = ( xB*yA - xA*yB )/( yA - yB );
		}
		else // exceptional case when one of the endpoints is already zero
		{
			if ( pow(yA,2)>0 ){ x = xB; }
			else if ( pow(yB,2)>0 ){ x = xA; }
			else{ x = 1.0/0.0; }
		}
	}

	return x;
}
