#include "LinearInterpolationTests.h"
#include "QuadraticInterpolationGivenExtremumTests.h"

using namespace std;

int main(void)
{
    cout << "Running Tests... \n\n\n";

    LinearInterpolationTests::testAllHardcodedExample();
	LinearInterpolationTests::testAllGivenParametersHardcoded(1E-6);
	LinearInterpolationTests::testAllGivenParametersRandom(1000, 1E-6);
	cout << "\n";
	QuadraticInterpolationGivenExtremumTests::testAllHardcodedExample();
	QuadraticInterpolationGivenExtremumTests::testAllGivenParametersHardcoded(1E-6);
	QuadraticInterpolationGivenExtremumTests::testAllGivenParametersRandom(1000, 1E-6);

    return 0;
}