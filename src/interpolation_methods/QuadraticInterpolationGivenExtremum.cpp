#include <cmath>
#include <iostream>
#include <cstdlib>
#include <vector>
#include "QuadraticInterpolationGivenExtremum.h"

using namespace std;


double QuadraticInterpolationGivenExtremum::secondOrderCoefficient()
{
	double A = (yA - yB)/( (xA - xB)*(xA + xB - 2.0*xExtremum ) );
	return A;
}

double QuadraticInterpolationGivenExtremum::firstOrderCoefficient()
{	
	double B = -2.0*secondOrderCoefficient()*xExtremum;
	return B;
}

double QuadraticInterpolationGivenExtremum::zerothOrderCoefficient()
{
	double C = ( -xB*(xB - 2.0*xExtremum)*yA + xA*(xA - 2.0*xExtremum)*yB )/( (xA - xB)*(xA + xB - 2.0*xExtremum) );
	return C;
}

double QuadraticInterpolationGivenExtremum::evaluate(double x)
{	
	double a = secondOrderCoefficient();
	double b = firstOrderCoefficient();
	double c = zerothOrderCoefficient();

	double y = a*x*x + b*x + c;

	return y;
}

double QuadraticInterpolationGivenExtremum::evaluateExtremumOrdinate()
{	
	double numerator = pow(xA-xExtremum, 2)*yB - pow(xB-xExtremum, 2)*yA;
	double denominator = ( xA - xB )*( xA + xB - 2.0*xExtremum );
	
	double yExtremum = numerator/denominator;

	return yExtremum;
}
