#ifndef QUADRATICINTERPOLATIONGIVENEXTREMUMTESTS_H
#define QUADRATICINTERPOLATIONGIVENEXTREMUMTESTS_H

#include <iostream>

class QuadraticInterpolationGivenExtremumTests
{
public:
    enum TestResult { FAILED, PASSED };

public:
    static double quadraticPolynomial(double , double , double , double);
    static TestResult testSecondOrderCoefficient(double , double , double , double , double , double , double );
    static TestResult testFirstOrderCoefficient(double , double , double , double , double , double , double );
    static TestResult testZerothOrderCoefficient(double , double , double , double , double , double , double );
    static TestResult testEvaluate(double , double , double , double , double , double , double , double );
    static TestResult testEvaluateExtremumOrdinate(double , double , double , double , double , double , double );
    static TestResult testBoundaryCases(double , double , double , double , double );
    static TestResult testAllHardcodedExample();
    static TestResult testAllGivenParameters(double , double , double , double , double , double , double );
    static TestResult testAllGivenParametersHardcoded(double );
    static TestResult testAllGivenParametersRandom(int , double );
};

#endif