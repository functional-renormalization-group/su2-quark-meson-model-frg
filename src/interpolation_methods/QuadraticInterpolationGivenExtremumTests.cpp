#include <cmath>
#include <iostream>
#include <cstdlib>
#include <vector>
#include "QuadraticInterpolationGivenExtremum.h"
#include "QuadraticInterpolationGivenExtremumTests.h"

using namespace std;


double QuadraticInterpolationGivenExtremumTests::quadraticPolynomial(double a, double b, double c, double x)
{
	double y = a*x*x + b*x + c;
	return y;
}

QuadraticInterpolationGivenExtremumTests::TestResult QuadraticInterpolationGivenExtremumTests::testSecondOrderCoefficient(double xA, double yA, double xB, double yB, double xExtremum, double expected, double absError) 
{
    QuadraticInterpolationGivenExtremum interp(xA, yA, xB, yB, xExtremum);
    double result = interp.secondOrderCoefficient();
    if ( fabs( (result-expected) )<absError ) 
	{
        std::cout << "testSecondOrderCoefficient PASSED\n";
		return PASSED;
    } 
	else 
	{
        std::cout << "testSecondOrderCoefficient FAILED: Expected " << expected << ", got " << result << ", the difference is: " << fabs( (result-expected) ) << "\n";
		return FAILED;
    }
}

QuadraticInterpolationGivenExtremumTests::TestResult QuadraticInterpolationGivenExtremumTests::testFirstOrderCoefficient(double xA, double yA, double xB, double yB, double xExtremum, double expected, double absError) 
{
    QuadraticInterpolationGivenExtremum interp(xA, yA, xB, yB, xExtremum);
    double result = interp.firstOrderCoefficient();
    if ( fabs( (result-expected) )<absError ) 
	{
        std::cout << "testFirstOrderCoefficient PASSED\n";
		return PASSED;
    } 
	else 
	{
        std::cout << "testFirstOrderCoefficient FAILED: Expected " << expected << ", got " << result << ", the difference is: " << fabs( (result-expected) ) << "\n";
		return FAILED;
    }
}

QuadraticInterpolationGivenExtremumTests::TestResult QuadraticInterpolationGivenExtremumTests::testZerothOrderCoefficient(double xA, double yA, double xB, double yB, double xExtremum, double expected, double absError) 
{
    QuadraticInterpolationGivenExtremum interp(xA, yA, xB, yB, xExtremum);
    double result = interp.zerothOrderCoefficient();
    if ( fabs( (result-expected) )<absError ) 
	{
        std::cout << "testZerothOrderCoefficient PASSED\n";
		return PASSED;
    } 
	else 
	{	
        std::cout << "testZerothOrderCoefficient FAILED: Expected " << expected << ", got " << result << ", the difference is: " << fabs( (result-expected) ) << "\n";
		return FAILED;
    }
}

QuadraticInterpolationGivenExtremumTests::TestResult QuadraticInterpolationGivenExtremumTests::testEvaluate(double xA, double yA, double xB, double yB, double xExtremum, double x, double expected, double absError)
{
    QuadraticInterpolationGivenExtremum interp(xA, yA, xB, yB, xExtremum);
    double result = interp.evaluate(x);
    if ( fabs( (result-expected) )<absError ) 
	{
        std::cout << "testEvaluate PASSED\n";
		return PASSED;
    } 
	else 
	{
        std::cout << "testEvaluate FAILED: Expected " << expected << ", got " << result << ", the difference is: " << fabs( (result-expected) ) << "\n";
		return FAILED;
    }
}

QuadraticInterpolationGivenExtremumTests::TestResult QuadraticInterpolationGivenExtremumTests::testEvaluateExtremumOrdinate(double xA, double yA, double xB, double yB, double xExtremum, double expected, double absError)
{
    QuadraticInterpolationGivenExtremum interp(xA, yA, xB, yB, xExtremum);
    double result = interp.evaluateExtremumOrdinate();
    if ( fabs( (result-expected) )<absError ) 
	{
        std::cout << "testEvaluateExtremumOrdinate PASSED\n";
		return PASSED;
    } 
	else 
	{
        std::cout << "testEvaluateExtremumOrdinate FAILED: Expected " << expected << ", got " << result << ", the difference is: " << fabs( (result-expected) ) << "\n";
		return FAILED;
    }
}

QuadraticInterpolationGivenExtremumTests::TestResult QuadraticInterpolationGivenExtremumTests::testBoundaryCases(double xA, double yA, double xB, double yB, double xExtremum)
{
    // Test with points where xA == xB, should throw an exception
	TestResult result_test1;
    try 
	{
        QuadraticInterpolationGivenExtremum interp(xA, yA, xA, yB, xExtremum);
        std::cout << "testBoundaryCases FAILED: Expected exception for identical x values (xA == xB).\n";
		result_test1 = FAILED;
    } 
	catch (const std::invalid_argument& e) 
	{
        std::cout << "testBoundaryCases PASSED for identical x values (xA == xB)\n";
		result_test1 = PASSED;
    }

	// Test where xA == xExtremum, should throw an exception
	TestResult result_test2;
    try 
	{
        QuadraticInterpolationGivenExtremum interp(xA, yA, xB, yB, xA);
        std::cout << "testBoundaryCases FAILED: Expected exception for identical x values (xA == xExtremum).\n";
		result_test2 = FAILED;
    } 
	catch (const std::invalid_argument& e) 
	{
        std::cout << "testBoundaryCases PASSED for identical x values (xA == xExtremum)\n";
		result_test2 = PASSED;
    }

	// Test where xB == xExtremum, should throw an exception
	TestResult result_test3;
    try 
	{
        QuadraticInterpolationGivenExtremum interp(xA, yA, xB, yB, xB);
        std::cout << "testBoundaryCases FAILED: Expected exception for identical x values (xB == xExtremum).\n";
		result_test3 = FAILED;
    } 
	catch (const std::invalid_argument& e) 
	{
        std::cout << "testBoundaryCases PASSED for identical x values (xB == xExtremum)\n";
		result_test3 = PASSED;
    }

	// Test where xA + xB == 2 * xExtremum, should throw an exception
	TestResult result_test4;
    try 
	{
        QuadraticInterpolationGivenExtremum interp(xA, yA, xB, yB, (xA+xB)/2.0);
        std::cout << "testBoundaryCases FAILED: Expected exception for xA + xB == 2 * xExtremum.\n";
		result_test4 = FAILED;
    } 
	catch (const std::invalid_argument& e) 
	{
        std::cout << "testBoundaryCases PASSED for xA + xB == 2 * xExtremum\n";
		result_test4 = PASSED;
    }

	// Test where yA == yB, should throw an exception
	TestResult result_test5;
    try 
	{
        QuadraticInterpolationGivenExtremum interp(xA, yA, xB, yA, xExtremum);
        std::cout << "testBoundaryCases FAILED: Expected exception for identical y values (yA == yB).\n";
		result_test5 = FAILED;
    } 
	catch (const std::invalid_argument& e) 
	{
        std::cout << "testBoundaryCases PASSED for identical y values (yA == yB)\n";
		result_test5 = PASSED;
    }

	if ( result_test1==PASSED && 
		 result_test2==PASSED &&
		 result_test3==PASSED &&
		 result_test4==PASSED &&
		 result_test5==PASSED )
	{
		return PASSED;
	}
	else
	{
		return FAILED;
	}
}

QuadraticInterpolationGivenExtremumTests::TestResult QuadraticInterpolationGivenExtremumTests::testAllHardcodedExample()
{	
	double absError, xA, yA, xB, yB, xExtremum, expected, x;
	absError = 1E-12;
	xA = 1.0;
	yA = 2.0;
	xB = 3.0;
	yB = 4.0;
	xExtremum = 5.0;

	expected = -1.0/6.0;
	TestResult result_testSecondOrderCoefficient = testSecondOrderCoefficient(xA, yA, xB, yB, xExtremum, expected, absError);

	expected = 5.0/3.0;
	TestResult result_testFirstOrderCoefficient = testFirstOrderCoefficient(xA, yA, xB, yB, xExtremum, expected, absError);

	expected = 0.5;
	TestResult result_testZerothOrderCoefficient = testZerothOrderCoefficient(xA, yA, xB, yB, xExtremum, expected, absError);

	x = 3.0;
	expected = 4.0;
	TestResult result_testEvaluate = testEvaluate(xA, yA, xB, yB, xExtremum, x, expected, absError);

	expected = 14.0/3.0;
	TestResult result_testEvaluateExtremumOrdinate = testEvaluateExtremumOrdinate(xA, yA, xB, yB, xExtremum, expected, absError);
	
	TestResult result_testBoundaryCases = testBoundaryCases(xA, yA, xB, yB, xExtremum);

	if ( result_testSecondOrderCoefficient==PASSED && 
		 result_testFirstOrderCoefficient==PASSED &&
		 result_testZerothOrderCoefficient==PASSED &&
		 result_testEvaluate==PASSED &&
		 result_testEvaluateExtremumOrdinate==PASSED &&
		 result_testBoundaryCases==PASSED )
	{	
		std::cout << "testAllHardcodedExample PASSED" << "\n";
		return PASSED;
	}
	else
	{	
		std::cout << "testAllHardcodedExample FAILED" << "\n";
		return FAILED;
	}
}

QuadraticInterpolationGivenExtremumTests::TestResult QuadraticInterpolationGivenExtremumTests::testAllGivenParameters(double a, double b, double c, double delta, double alpha, double xToEvaluateAt, double absError)
{		
	//make sure a is not zero
	bool aDiffZero = (a<0 || a>0);
	if ( !aDiffZero )
	{ 
		cout << "The parameter a cannot be equal to zero! It must be second order polynomial! Aborting!\n";
		abort(); 
	}

	//validate that delta is bigger than zero
	if ( delta<=0 )
	{ 	
		cout << "The parameter delta must be bigger than zero! Aborting!\n";
		abort();
	}

	//alpha cannot be equal to 0, +/-1
	bool alphaDiffZero = (alpha<0 || alpha>0);
	bool alphaModulusDiffOne = (fabs(alpha)<1 || fabs(alpha)>1);
	if ( !alphaDiffZero || !alphaModulusDiffOne )
	{ 
		cout << "The parameter alpha cannot be equal to 0, +/-1!\n";
		abort();
	}
	
	//evaluate necessary quantities
	double xExtremum = -b/(2.0*a);
	
	//build the values of xA and xB from xExtremum and parameters
	double xA = xExtremum - delta;
	double xB = xExtremum + alpha*delta;
	
	double yA = quadraticPolynomial(a, b, c, xA);
	double yB = quadraticPolynomial(a, b, c, xB);
	double yExtremum = quadraticPolynomial(a, b, c, xExtremum);

	TestResult result_testSecondOrderCoefficient = testSecondOrderCoefficient(xA, yA, xB, yB, xExtremum, a, absError);
	TestResult result_testFirstOrderCoefficient = testFirstOrderCoefficient(xA, yA, xB, yB, xExtremum, b, absError);
	TestResult result_testZerothOrderCoefficient = testZerothOrderCoefficient(xA, yA, xB, yB, xExtremum, c, absError);
	TestResult result_testEvaluate = testEvaluate(xA, yA, xB, yB, xExtremum, xToEvaluateAt, quadraticPolynomial(a, b, c, xToEvaluateAt), absError);
	TestResult result_testEvaluateExtremumOrdinate = testEvaluateExtremumOrdinate(xA, yA, xB, yB, xExtremum, yExtremum, absError);
	TestResult result_testBoundaryCases = testBoundaryCases(xA, yA, xB, yB, xExtremum);

	if ( result_testSecondOrderCoefficient==PASSED && 
		 result_testFirstOrderCoefficient==PASSED &&
		 result_testZerothOrderCoefficient==PASSED &&
		 result_testEvaluate==PASSED &&
		 result_testEvaluateExtremumOrdinate==PASSED &&
		 result_testBoundaryCases==PASSED )
	{	
		std::cout << "testAllGivenParameters PASSED" << "\n";
		return PASSED;
	}
	else
	{	
		std::cout << "testAllGivenParameters FAILED" << "\n";
		return FAILED;
	}
}

QuadraticInterpolationGivenExtremumTests::TestResult QuadraticInterpolationGivenExtremumTests::testAllGivenParametersHardcoded(double absError)
{	
	double a, b, c, delta, alpha, xToEvaluateAt;

	a = 1.0;
	b = 2.0;
	c = 3.0;
	delta = 3.14159;
	alpha = 0.5;
	xToEvaluateAt = 3.0;
	return testAllGivenParameters(a, b, c, delta, alpha, xToEvaluateAt, absError);
}

QuadraticInterpolationGivenExtremumTests::TestResult QuadraticInterpolationGivenExtremumTests::testAllGivenParametersRandom(int numberOfTests, double absError)
{	
	srand(static_cast<unsigned int>(time(0))); // Seed the random number generator

	std::vector<TestResult> result;
	std::vector<std::vector<double>> resultParameters(numberOfTests);
	
	for (int i = 0; i < numberOfTests; i++)
	{	
		double aSign = + 1;
		double bSign = + 1;
		double cSign = + 1;
		double alphaSign = +1;
		double xToEvaluateAtSign = + 1;
		if( (rand() % 2)==0 ){ aSign = -1.0; }
		if( (rand() % 2)==0 ){ bSign = -1.0; }
		if( (rand() % 2)==0 ){ cSign = -1.0; }
		if( (rand() % 2)==0 ){ alphaSign = -1.0; }
		if( (rand() % 2)==0 ){ xToEvaluateAtSign = -1.0; }
		
		double a = aSign*( rand() % 10 + ( 1.0 + rand() % 9 )/1E1 );
		double b = bSign*( rand() % 10 + ( 0.0 + rand() % 99 )/1E2 );
		double c = cSign*( rand() % 10 + ( 0.0 + rand() % 99 )/1E2 );
		double delta = (rand() % 10 + (5.0 + rand() % 5) / 1E1);
		double alpha = alphaSign*( rand() % 5 + ( 1.0 + rand() % 99 )/1E2 );
		double xToEvaluateAt = xToEvaluateAtSign*( rand() % 10 + ( 1.0 + rand() % 99 )/1E2 );
		resultParameters[i].push_back(a);
		resultParameters[i].push_back(b);
		resultParameters[i].push_back(c);
		resultParameters[i].push_back(delta);
		resultParameters[i].push_back(alpha);
		resultParameters[i].push_back(xToEvaluateAt);

		std::cout << "a=" << resultParameters[i][0] << "\n";
		std::cout << "b=" << resultParameters[i][1] << "\n";
		std::cout << "c=" << resultParameters[i][2] << "\n";
		std::cout << "delta=" << resultParameters[i][3] << "\n";
		std::cout << "alpha=" << resultParameters[i][4] << "\n";
		std::cout << "xToEvaluateAt=" << resultParameters[i][5] << "\n";

		result.push_back( testAllGivenParameters(a, b, c, delta, alpha, xToEvaluateAt, absError) );
	}

	TestResult totalResult = PASSED;
	for (int i = 0; i < int(result.size()); i++)
	{	
		if ( result[i]==PASSED )
		{
			totalResult = PASSED;
		}
		else
		{
			totalResult = FAILED;
			break;
		}
	}

	if ( totalResult==FAILED )
	{	
		std::cout << "testAllGivenParametersRandom FAILED! It failed for the following cases: \n";

		for (int i = 0; i < int(result.size()); i++)
		{	
			if ( result[i]==FAILED )
			{
				std::cout << "a=" << resultParameters[i][0] << "\n";
				std::cout << "b=" << resultParameters[i][1] << "\n";
				std::cout << "c=" << resultParameters[i][2] << "\n";
				std::cout << "delta=" << resultParameters[i][3] << "\n";
				std::cout << "alpha=" << resultParameters[i][4] << "\n";
				std::cout << "xToEvaluateAt=" << resultParameters[i][5] << "\n";
			}
		}
	}
	else
	{
		std::cout << "testAllGivenParametersRandom PASSED for " << numberOfTests << " random cases!\n";
	}

	return totalResult;
}
