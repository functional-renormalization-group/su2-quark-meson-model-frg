#ifndef LINEARINTERPOLATIONTESTS_H
#define LINEARINTERPOLATIONTESTS_H

#include <iostream>


class LinearInterpolationTests
{
public:
    enum TestResult { FAILED, PASSED };

public:
    static double linearPolynomial(double , double , double );
    static TestResult testSlopeParameter(double , double , double , double , double , double );
    static TestResult testYInterceptParameter(double , double , double , double , double , double );
    static TestResult testEvaluate(double , double , double , double , double , double , double );
    static TestResult testEvaluateZeroCrossing(double , double , double , double , double , double );
    static TestResult testEvaluateNoZeroCrossing(double , double , double , double );
    static TestResult testBoundaryCases(double , double , double , double );
    static TestResult testAllHardcodedExample();
    static TestResult testAllGivenParameters(double , double , double , double , double , double );
    static TestResult testAllGivenParametersHardcoded(double );
    static TestResult testAllGivenParametersRandom(int , double );
};


#endif