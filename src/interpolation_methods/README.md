# Interpolation Methods



## Introduction

In this folder the following Interpolation Methods are built from 'scratch':
- Linear Interpolation
- Quadratic Interpolation Given Extremum

Each method is defined by an isolated class, in its own isolated file and tests are also written for each class.

[[_TOC_]]



## Linear Interpolation

The LinearInterpolation class provides methods to define the linear interpolation between two points $(x_A,y_A)$ and $(x_B,y_B)$ using a linear polynomial,
$$
\begin{align}
y(x) = m x + b .
\end{align}
$$
Such class includes functionality to calculate the slope ($m$), $y$ intercept of the line defined by these two points ($b$), evaluate the line at any given $x$ coordinate, and determine the $x$ coordinate at which the line crosses the $y=0$ axis (zero-crossing).

In order to obtain the necessary parameters to define the interpolation function, consider the two user provided points:
$$
\begin{align}
y_A = y(x_A) = m x_A + b ,
\\
y_B = y(x_B) = m x_B + b .
\end{align}
$$
Solving this system yields the following solutions for the parameters $m$ (slope parameter) and $b$ ($y$ intercept parameter):
$$
\begin{align}
m = \frac{ y_A - y_B }{ x_A - x_B },
\\
b = \frac{ x_A y_B - x_B y_A }{ x_A - x_B } .
\end{align}
$$

The zero-crossing point is easily obatined via the interpolation parameters:
$$
\begin{align}
y( x_0 ) = 0 = m x_0 + b \Rightarrow x_0 = - \frac{b}{m} = \frac{ x_B y_A - x_A y_B }{ y_A - y_B }.
\end{align}
$$

Analysing these expressions leads to the following conclusions:
- The user provided interpolations points, $x_A$ and $x_B$, cannot be the same: $x_A \neq x_B$ and $y_A \neq y_B$. 



### Tests

A testing class for the LinearInterpolation class is also provided, LinearInterpolationTests. In this class, several methods are provided to test the LinearInterpolation methods. This includes verifying the correctness of slope and y-intercept calculations, evaluating interpolated values, and handling edge cases.

Features:
- Boundary and Edge Case Testing: Methods to ensure robustness by testing the behavior of the LinearInterpolation class at boundaries and edge cases.
- Hardcoded Tests: Predefined tests with known inputs and expected outputs to verify fundamental functionality.
- Parameterized Tests: Methods that accept parameters to test a wide range of input scenarios.
- Randomized Testing: A method that uses parameterized tests to run a series of tests with randomly generated inputs, increasing test coverage.

In the randomized testing, the method is built in such a way to span the most general set of possible parameters that can be used to build instances of the LinearInterpolation.



## Quadratic Interpolation Given Extremum

The QuadraticInterpolationGivenExtremum class provides methods to perform quadratic interpolation given two points $(x_A,y_A)$ and $(x_B,y_B)$, and an extremum point $x_\mathrm{E}​$ (point at which the derivative vanishes of the function vanishes). Thus, the quadratic polynomial that is being used to define the interpolation is:
$$
\begin{align}
y(x) = a x^2 + b x + c .
\end{align}
$$
Such class calculates the coefficients, $a$ (second order coefficient), $b$ (first order coefficient) and $c$ (zeroth order coefficient) evaluates the quadratic function at any given $x$ coordinate, as well as at the extremum ordinate, $y(x_\mathrm{E}) = y_\mathrm{E}​$.

In order to evaluate the parameters defined above, consider the two user provided points, $(x_A,y_A)$ and $(x_B,y_B)$, and the condition that the derivative of the interpolation function is zero at $x_\mathrm{E}​$. These conditions translate into:
$$
\begin{align}
y(x_A) = y_A = a x_A^2 + b x_A + c ,
\\
y(x_B) = y_B =  a x_B^2 + b x_B + c ,
\\
\left.\frac{dy}{dx}\right|_{x_\mathrm{E}=0} = 0 \Rightarrow 2 a x_\mathrm{E} + b = 0 .
\end{align}
$$
Solving this system of equations yields the following results for the parameters:
$$
\begin{align}
a = \frac{ y_A - y_B }{ (x_A - x_B)( x_A + x_B - 2 x_\mathrm{E} ) } ,
\\
b = - 2 a x_\mathrm{E} ,
\\
c = \frac{ -x_B ( x_B - 2 x_\mathrm{E} ) y_A + x_A ( x_A - 2 x_\mathrm{E} ) y_B }{ ( x_A - x_B ) ( x_A + x_B - 2 x_\mathrm{E} ) } .
\end{align}
$$

The value of the interpolation at the extremum, $y_\mathrm{E}$, can be calculated explicitly to yield:
$$
\begin{align}
y_\mathrm{E} = \frac{ y_B ( x_A - x_\mathrm{E} )^2 - y_A ( x_B - x_\mathrm{E} )^2 }{ ( x_A - x_B ) ( x_A + x_B - 2 x_\mathrm{E} ) }.
\end{align}
$$

Analysing these expressions leads to the following conclusions:
- The user provided interpolations points, $x_A$ and $x_B$, cannot be the same: $x_A \neq x_B$. Additionally, each point cannot be themselves the same as the user provided extremum point, $x_\mathrm{E}$, i.e., $x_A \neq x_\mathrm{E}$ and $x_B \neq x_\mathrm{E}$.
- The user provided interpolations points, $x_A$ and $x_B$, cannot be symmstrical with respect to the extremum point, $x_\mathrm{E}$. If these points are symmetrical, then one can show that: $x_A + x_B - 2 x_\mathrm{E} = 0 $ and that $y_A=y_B$.



### Tests

A testing class for the QuadraticInterpolationGivenExtremum class is also provided, QuadraticInterpolationGivenExtremumTests. In this class several methods are provided to test the QuadraticInterpolationGivenExtremum methods. This includes verifying the correctness of the parameters, evaluating interpolated values, and handling edge cases.

Features:
- Boundary and Edge Case Testing: Methods to ensure robustness by testing the behavior of the QuadraticInterpolationGivenExtremum class at boundaries and edge cases.
- Hardcoded Tests: Predefined tests with known inputs and expected outputs to verify fundamental functionality.
- Parameterized Tests: Methods that accept parameters to test a wide range of input scenarios.
- Randomized Testing: A method that uses parameterized tests to run a series of tests with randomly generated inputs, increasing test coverage.

In the randomized testing, the method is built in such a way to span the most general set of possible parameters that can be used to build instances of the QuadraticInterpolationGivenExtremum.



## How to run

In order to use these methods in a code, simply copy the folder interpolation_methods to the root of your other `.cpp` files and include the class in your code using:
```bash
#include "LinearInterpolation.h"
```
For the compilation used the appropriate glag (e.g., `-Isrc/interpolation_methods`).



## How to execute the tests

In order to execute the tests for each class, a `.cpp` file is provided which runs all tests for an hardcoded example, an hardcoded example providing the parameters and a randomized testing. To run the tests, simply compile the code in this folder using the Makefile:
```bash
cd interpolation_methods
make && ./runTests.out
make clean
```



