#ifndef LINEARINTERPOLATION_H
#define LINEARINTERPOLATION_H

#include <iostream>


// Linear interpolation between 2 points: y = m*x + b
class LinearInterpolation
{	
private:
    double xA = 1.0/0.0;
    double yA = 1.0/0.0;
    
    double xB = 1.0/0.0;
    double yB = 1.0/0.0;
	
public:
    LinearInterpolation(double xAAux, double yAAux, double xBAux, double yBAux)
    {   
        // Make sure that the provided points do not have the same x coordinate
        if ( xAAux<xBAux || xAAux>xBAux )
        {
            xA = xAAux;
            yA = yAAux;

            xB = xBAux;
            yB = yBAux;
        }
        else
        {
            throw std::invalid_argument("Problem! xA==xB! I cannot define the LinearInterpolation if the points have the same x value!\n");
        }
    };

    double slopeParameter();
    double yInterceptParameter();
    double evaluate(double );
    double evaluateZeroCrossing();
};

#endif