#ifndef QUADRATICINTERPOLATIONGIVENEXTREMUM_H
#define QUADRATICINTERPOLATIONGIVENEXTREMUM_H

#include <iostream>


// Quadratic interpolation between 2 points + extrema: y = A*x^2 + B*x + C
class QuadraticInterpolationGivenExtremum
{	
private:
    double xA = 1.0/0.0;
    double xB = 1.0/0.0;

    double yA = 1.0/0.0;
    double yB = 1.0/0.0;

    double xExtremum = 1.0/0.0;
	
public:
    QuadraticInterpolationGivenExtremum(double xAAux, double yAAux, double xBAux, double yBAux, double xExtremumAux)
    {   
        bool xADiffxB = (xAAux<xBAux || xAAux>xBAux);
        bool xADiffxE = (xAAux<xExtremumAux || xAAux>xExtremumAux);
        bool xBDiffxE = (xBAux<xExtremumAux || xBAux>xExtremumAux);
        bool xAPlusxBDiff2xE = ( (xAAux+xBAux)<2.0*xExtremumAux || (xAAux+xBAux)>2.0*xExtremumAux );
        bool yADiffyB = (yAAux<yBAux || yAAux>yBAux);

        // Make sure that the provided points do not have the same x coordinate
        if ( xADiffxB && xADiffxE && xBDiffxE && xAPlusxBDiff2xE && yADiffyB )
        {
            xA = xAAux;
            yA = yAAux;

            xB = xBAux;
            yB = yBAux;

            xExtremum = xExtremumAux;
        }
        else
        {   
            std::string issues = "";
            if ( !xADiffxB )       { issues = issues + " xA=xB"; }else{ issues = issues + ""; }
            if ( !xADiffxE )       { issues = issues + " xA=xE"; }else{ issues = issues + ""; }
            if ( !xBDiffxE )       { issues = issues + " xB=xE";  }else{ issues = issues + ""; }
            if ( !xAPlusxBDiff2xE ){ issues = issues + " xA+xB=2xE"; }else{ issues = issues + ""; }
            if ( !yADiffyB )       { issues = issues + " yA=yB"; }else{ issues = issues + ""; }
            issues = "Problem! I cannot define the QuadraticInterpolationGivenExtremum:" + issues + "\n";
            throw std::invalid_argument(issues);
        }
    };

    double secondOrderCoefficient();
    double firstOrderCoefficient();
    double zerothOrderCoefficient();
    double evaluate(double );
    double evaluateExtremumOrdinate();
};

#endif