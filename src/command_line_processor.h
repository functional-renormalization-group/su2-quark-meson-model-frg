#ifndef COMMAND_LINE_PROCESSOR_H
#define COMMAND_LINE_PROCESSOR_H

#include "IniFileParser.h"


int commandLineArgsProcessor(int , char* []);

void selectPathBasedOnFileDetails(const IniFileParser& );


#endif