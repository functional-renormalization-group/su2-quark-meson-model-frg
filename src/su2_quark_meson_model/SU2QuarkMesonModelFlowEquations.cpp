#include <cmath>
#include "SU2QuarkMesonModelFlowEquations.h"

using namespace std;


//Fermi-Dirac distribution function
double fermiDistribution(double temperature, double energy)
{
    double n = 1.0/( exp( energy/temperature ) + 1.0 );

    return n;
}


//Bose-Einstein distribution
double boseDistribution(double temperature, double energy)
{
    double n = 1.0/( exp( energy/temperature ) - 1.0 );

    return n;
}


double su2QuarkMesonModelUVPotential(double m, double lambda, double sigma)
{	
	double uvPotential = 0.5*pow(m, 2)*pow(sigma, 2) + 0.25*lambda*pow(sigma, 4);

	return uvPotential;
}


double su2QuarkMesonModelEnergySigma(double k, double d2Udsigma2)
{	
	double Esigma = sqrt( pow(k,2) + d2Udsigma2 );

	return Esigma;
}


double su2QuarkMesonModelEnergyPion(double k, double sigma, double dUdsigma)
{	
	double Epion = sqrt( pow(k,2) + dUdsigma/sigma  );

	return Epion;
}


double su2QuarkMesonModelEnergyQuark(double k, double sigma, double h)
{
	double Equark = sqrt( pow(k,2) + pow(h,2)*pow(sigma,2) );

	return Equark;
}


double su2QuarkMesonModelOptimalStepYokotaFVacuum(double k, double Epion, double sigma)
{
	double F = -pow(k,5)/( 8.0*pow(M_PI,2)*sigma*pow(Epion,3) );

	return F;
}


double su2QuarkMesonModelOptimalStepYokotaF(double T, double k, double Epion, double sigma)
{
	double auxF = ( 1.0 + 2.0*boseDistribution(T, Epion) ) + ( Epion/(2.0*T) )*( 4.0*boseDistribution(T, Epion)*( 1.0 + boseDistribution(T, Epion) ) );
	double F = -pow(k,5)*auxF/( 8.0*pow(M_PI,2)*sigma*pow(Epion,3) );

	return F;
}


double su2QuarkMesonModelOptimalStepYokotaGVacuum(double k, double Esigma)
{
	double G = -pow(k,5)/( 24.0*pow(M_PI,2)*pow(Esigma,3) );

	return G;
}


double su2QuarkMesonModelOptimalStepYokotaG(double T, double k, double Esigma)
{
	double aux_G = ( 1.0 + 2.0*boseDistribution(T, Esigma) ) + (Esigma/(2.0*T))*( 4.0*boseDistribution(T, Esigma)*( 1.0 + boseDistribution(T, Esigma) ) );		
	double G = -pow(k,5)*aux_G/( 24.0*pow(M_PI,2)*pow(Esigma,3) );

	return G;
}


double su2QuarkMesonModelRHSFlowEqPotentialVacuum(double k, double Esigma, double Epion, double Equark)
{	
	const double Nc = 3.0;

	double rhs = 0.0;
	rhs = ( pow(k,5)/( 12.0*pow(M_PI,2) ) )*( 1.0/Esigma + 3.0/Epion - 8.0*Nc/Equark ); 

	return rhs;
}


double su2QuarkMesonModelRHSFlowEqPotential(double T, double cpu, double cpd, double k, double Esigma, double Epion, double Equark)
{	
	const double Nc = 3.0;

	double rhs = 0.0;
	rhs = + 1.0*( 1.0 + 2.0*boseDistribution(T, Esigma) )/( Esigma ) 
		  + 3.0*( 1.0 + 2.0*boseDistribution(T, Epion)  )/( Epion ) 
		  - 4.0*Nc*( 1.0 - fermiDistribution(T, Equark + cpu) - fermiDistribution(T, Equark - cpu) )/( Equark )
		  - 4.0*Nc*( 1.0 - fermiDistribution(T, Equark + cpd) - fermiDistribution(T, Equark - cpd) )/( Equark );     
	rhs = ( pow(k,5)/( 12.0*pow(M_PI,2) ) )*rhs;

	return rhs;
}


double su2QuarkMesonModelRHSFlowEqDTPotential(double T, double cpu, double cpd, double k, double Esigma, double Epion, double Equark, double sigma, double dsigmaDTU, double dsigma2DTU)
{	
	const double Nc = 3.0;

	double rhs = 0.0;
	rhs = + 2.0*boseDistribution(T, Esigma)*( 1.0 + boseDistribution(T, Esigma) )*( 1.0/pow(T,2) - dsigma2DTU/( 2.0*T*pow(Esigma,2) ) )
		  - 1.0*( 1.0 + 2.0*boseDistribution(T, Esigma) )*dsigma2DTU/( 2.0*pow(Esigma,3) )
		  + 6.0*boseDistribution(T, Epion)*( 1.0 + boseDistribution(T, Epion) )*( 1.0/pow(T,2) - dsigmaDTU/( 2.0*T*pow(Epion,2)*sigma ) )
		  - 3.0*( 1.0 + 2.0*boseDistribution(T, Epion) )*dsigmaDTU/( 2.0*pow(Epion,3)*sigma )
		  + 8.0*Nc*( Equark + cpu )*fermiDistribution(T, Equark + cpu )*( 1.0 - fermiDistribution(T, Equark + cpu ) )/( 2.0*pow(T,2)*Equark )
		  + 8.0*Nc*( Equark - cpu )*fermiDistribution(T, Equark - cpu )*( 1.0 - fermiDistribution(T, Equark - cpu ) )/( 2.0*pow(T,2)*Equark )
		  + 8.0*Nc*( Equark + cpd )*fermiDistribution(T, Equark + cpd )*( 1.0 - fermiDistribution(T, Equark + cpd ) )/( 2.0*pow(T,2)*Equark )
		  + 8.0*Nc*( Equark - cpd )*fermiDistribution(T, Equark - cpd )*( 1.0 - fermiDistribution(T, Equark - cpd ) )/( 2.0*pow(T,2)*Equark );
	rhs = ( pow(k,5)/( 12.0*pow(M_PI,2) ) )*rhs;

	return rhs;
}


double su2QuarkMesonModelRHSFlowEqDCpfPotential(double T, double cpf, double k, double Esigma, double Epion, double Equark, double sigma, double dsigmaDCpfPotential, double dsigma2DCpfPotential)
{	
	const double Nc = 3.0;

	double rhs = 0.0;
	rhs = - ( dsigma2DCpfPotential/( 2.0*pow(Esigma,2) ) )*( ( 1.0 + 2.0*boseDistribution(T, Esigma) )/Esigma + 2.0*boseDistribution(T, Esigma)*( 1.0 + boseDistribution(T, Esigma) )/T )
		  - ( dsigmaDCpfPotential/( 2.0*pow(Epion,2)*sigma ) )*( 3.0*( 1.0 + 2.0*boseDistribution(T, Epion))/Epion + 6.0*boseDistribution(T, Epion)*( 1.0 + boseDistribution(T, Epion) )/T )
		  - 4.0*Nc*fermiDistribution(T, Equark + cpf )*( 1.0 - fermiDistribution(T, Equark + cpf ) )/( T*Equark )
		  + 4.0*Nc*fermiDistribution(T, Equark - cpf )*( 1.0 - fermiDistribution(T, Equark - cpf ) )/( T*Equark );
	rhs = ( pow(k,5)/( 12.0*pow(M_PI,2) ) )*rhs;

	return rhs;
}



