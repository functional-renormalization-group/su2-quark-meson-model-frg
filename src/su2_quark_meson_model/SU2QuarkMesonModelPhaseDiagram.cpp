#include <cmath>
#include <iostream>
#include <algorithm>
#include "SU2QuarkMesonModelPhaseDiagram.h"

using namespace std;


void SU2QuarkMesonModelPhaseDiagramAtFixedTemperature::checkIfTemperatureIsFixed()
{
	for (int i = 0; i < grandPotential.getLength(); i++)
	{	
		if( fabs(grandPotential.getTemperature(i)-grandPotential.getTemperature(0))>0 ){ cout << "Temperature is not fixed!" << "\n"; abort(); }
	}
}


bool SU2QuarkMesonModelPhaseDiagramAtFixedTemperature::checkNumberOf3Solutions()
{   
    bool test = true;
	//check if there are enough 3 solution points
	int number3SolutionPoints = 0;
	for (int i = 0; i < grandPotential.getLength(); i++)
	{
		if( grandPotential.getNumberOfExtrema(i)==3 ){ number3SolutionPoints = number3SolutionPoints + 1; }
	}

	if ( number3SolutionPoints<2 )
	{
		cout << "Too few points of 3 solutions provided in order to calculate the phase transition point. \n";
        test = false;
	}

    return test;
}


SU2QuarkMesonModelPhaseDiagramAtFixedTemperature::SU2QuarkMesonModelPhaseDiagramAtFixedTemperature(SU2QuarkMesonModelGrandPotential grandPotentialAux, InterpolationGSL1DimMethod method)
{
    //check if temperature is fixed
    checkIfTemperatureIsFixed();

    grandPotential = grandPotentialAux;

    interpolationMethod = method;
}


SU2QuarkMesonModelPhaseDiagramAtFixedTemperature::SU2QuarkMesonModelPhaseDiagramAtFixedTemperature(string fileName, InterpolationGSL1DimMethod method)
{
    //check if temperature is fixed
    checkIfTemperatureIsFixed();

    grandPotential = SU2QuarkMesonModelGrandPotential(fileName);

    interpolationMethod = method;
}


double SU2QuarkMesonModelPhaseDiagramAtFixedTemperature::calculateUpQuarkChemicalPotentialOfPhaseTransition()
{
	if (checkNumberOf3Solutions()==false){ return 0.0; }
	
	//create two lists to separate branches in the Gibbs construction
    vector<tuple<double, double> > branch1; 
    vector<tuple<double, double> > branch2;

    bool onBranch1 = true;
    bool onBranch2 = false;
    for (int i = 1; i < grandPotential.getLength()-1; ++i)
    {   
		double aux1 = (grandPotential.getChemicalPotentialUpQuark(i)-grandPotential.getChemicalPotentialUpQuark(i-1))/grandPotential.getChemicalPotentialUpQuark(0);
        if ( aux1<=0 && onBranch1==true ){ onBranch1 = false; }
        if (onBranch1==true)
        {
            branch1.push_back( make_tuple( grandPotential.getChemicalPotentialUpQuark(i), grandPotential.getGrandPotentialExtremum(i) ) );
        }
        
		double aux2 = (grandPotential.getChemicalPotentialUpQuark(i)-grandPotential.getChemicalPotentialUpQuark(i+1))/grandPotential.getChemicalPotentialUpQuark(0);
        if ( aux2<=0 && onBranch1==false ){ onBranch2 = true; }
        if (onBranch2==true)
        {
            branch2.push_back( make_tuple( grandPotential.getChemicalPotentialUpQuark(i+1), grandPotential.getGrandPotentialExtremum(i+1) ) );
        }
    }

    //sort lists in chemical potential
    sort(branch1.begin(), branch1.end()); 
    sort(branch2.begin(), branch2.end()); 
	int sizeBranch1 = int(branch1.size());
	int sizeBranch2 = int(branch2.size());

	//obtain the minimum in the cross pattern
    double cpUpQuarkInitial = 0.0;
    if ( get<0>(branch1[0])<=get<0>(branch2[0]) )
    {
        cpUpQuarkInitial = get<0>(branch2[0]);
    }
    else if (get<0>(branch2[0])<get<0>(branch1[0]))
    {
        cpUpQuarkInitial = get<0>(branch1[0]);
    }

	//obtain the maximum in the cross pattern
    double cpUpQuarkFinal = 0.0;
    if ( get<0>(branch1[sizeBranch1-1])<=get<0>(branch2[sizeBranch2-1]) )
    {
        cpUpQuarkFinal = get<0>(branch1[sizeBranch1-1]);
    }
    else if (get<0>(branch2[sizeBranch2-1])<get<0>(branch1[sizeBranch1-1]))
    {
        cpUpQuarkFinal = get<0>(branch2[sizeBranch2-1]);
    }

	//create two lists with the correct size
    vector<tuple<double, double> > branch1Modified; 
    vector<tuple<double, double> > branch2Modified;
    for (int i = 0; i < sizeBranch1; ++i)
    {
        if (get<0>(branch1[i])>=cpUpQuarkInitial && get<0>(branch1[i])<=cpUpQuarkFinal)
        {
            branch1Modified.push_back( make_tuple( get<0>(branch1[i]), get<1>(branch1[i]) ) );
        }
    }
    for (int i = 0; i < sizeBranch2; ++i)
    {
        if (get<0>(branch2[i])>=cpUpQuarkInitial && get<0>(branch2[i])<=cpUpQuarkFinal)
        {
            branch2Modified.push_back( make_tuple( get<0>(branch2[i]), get<1>(branch2[i]) ) );
        }
    }
	
	//subract the branches, interpolate such subtraction and search for its zero
	vector<double> chemicalPotential(int(branch1Modified.size()),0.0);
	vector<double> branchSubtraction(int(branch1Modified.size()),0.0);
	for (int i = 0; i < int(branch1Modified.size()); ++i)
    {
        chemicalPotential[i] = get<0>(branch1Modified[i]);
        branchSubtraction[i] = get<1>(branch2Modified[i]) - get<1>(branch1Modified[i]);
    }

	InterpolationGSL1Dim transition(interpolationMethod, chemicalPotential, branchSubtraction);
	vector<double> phaseTransitionPoint = transition.findRoots(rootFindingMethodAux, rootFindingPrecision);

    //check that there is only one transition point
	if( int(phaseTransitionPoint.size())>1 ){ cout << "More than one crossing point in the branch subtraction!\n"; abort(); }

	return phaseTransitionPoint[0];
}



vector<double> SU2QuarkMesonModelPhaseDiagramAtFixedTemperature::calculateUpQuarkChemicalPotentialOfSpinodals()
{	
	if (checkNumberOf3Solutions()==false){ return {0.0, 0.0}; }

	//consider only data points where the sigma field increases as a function of the up quark chemical potential: necessary for interpolation
	int sigmaMaxIndex = 0;
	for (int i = 0; i < grandPotential.getLength(); i++)
	{
		if( grandPotential.getSigmaExtremum(i)>grandPotential.getSigmaExtremum(sigmaMaxIndex) ){ sigmaMaxIndex = i; }
	}

	//the sigma field is organized from large->small it must be small-large for the InterpolationGSL1Dim class
	vector<double> increasingSigma;
	vector<double> chemicalPotential;
	for (int i = grandPotential.getLength()-1; i >= sigmaMaxIndex; i--)
	{	
		increasingSigma.push_back( grandPotential.getSigmaExtremum(i) );
		chemicalPotential.push_back( grandPotential.getChemicalPotentialUpQuark(i) );
	}
	
	//interpolate chemical potential as a function of the sigma field
	InterpolationGSL1Dim transition(interpolationMethod, increasingSigma, chemicalPotential);
	vector<double> spinodalSigma;
	vector<double> spinodalChemicalPotential;
	spinodalSigma = transition.findRoots1stDerivative(rootFindingMethodAux, rootFindingPrecision);
	for (int i = 0; i < int(spinodalSigma.size()); i++)
	{
		spinodalChemicalPotential.push_back( transition.evaluate( spinodalSigma[i] ) );
	}
	sort(spinodalChemicalPotential.begin(), spinodalChemicalPotential.end()); 

	//check that there are only two spinodal points
	if( int(spinodalChemicalPotential.size())>2 ){ cout << "More than two spinodal points!\n"; abort(); }

	return spinodalChemicalPotential;
}