#ifndef SU2QUARKMESONMODELEFFECTIVEACTION_H
#define SU2QUARKMESONMODELEFFECTIVEACTION_H

#include <tuple>
#include "OneDimensionalGrid.h"
#include "InterpolationGSL1Dim.h"


std::vector<double> vectorFromSet(std::vector<std::vector<double>*> , const int );

std::vector<double> vectorSum(std::vector<double> &, double , std::vector<double> &);


double yStepRK4(double , double , double , double , double , double );

double tStep(double , double );


class SU2QuarkMesonModelParameters
{	
private:
	double modelCutoff = 0.0;
	double yukawaCoupling = 0.0;
	
	double c2UVPotential = 0.0;
	double c4UVPotential = 0.0;
	double c1SymmetryBreaking = 0.0;

	double temperature = 0.0;
	double chemicalPotentialUpQuark = 0.0;
	double chemicalPotentialDownQuark = 0.0;

public:
	SU2QuarkMesonModelParameters(){};
	SU2QuarkMesonModelParameters(SU2QuarkMesonModelParameters* );
	SU2QuarkMesonModelParameters(double, double, double, double, double, double, double, double);

	double getModelCutoff(){ return modelCutoff; }
	double getYukawaCoupling(){ return yukawaCoupling; }

	double getC2UVPotential(){ return c2UVPotential; }
	double getC4UVPotential(){ return c4UVPotential; }
	double getC1SymmetryBreaking(){ return c1SymmetryBreaking; }
	
	double getTemperature(){ return temperature; }
	double getChemicalPotentialUpQuark(){ return chemicalPotentialUpQuark; }
	double getChemicalPotentialDownQuark(){ return chemicalPotentialDownQuark; }
};


enum QuantitiesToFlow 
{ 
	pot, 
	potDTPot, 
	potDCpPot, 
	potDTPotDCpPot,
	QuantitiesToFlowCount // Used as the boundary, add new methods above this value! 
};

std::string toStringQuantitiesToFlow(QuantitiesToFlow );

QuantitiesToFlow stringToQuantitiesToFlow(const std::string& );


class QuarkMesonFlowParameters
{	
private:
	double uvMomentumCutoff = 0;
	double irMomentumCutoff = 0;

	double initialRKStepSize = -1E-3;
    
    bool displayIterativeStepsOnConsole = false;
    
    bool saveFlowEvolution = false;
    double saveFlowEvolutionAtEachDeltaK = 0.020;

	QuantitiesToFlow quantitiesToFlow = potDTPotDCpPot;

public:
	QuarkMesonFlowParameters(){};
	QuarkMesonFlowParameters(double , double );
	QuarkMesonFlowParameters(double , double , QuantitiesToFlow );
	
	double getUVMomentumCutoff(){ return uvMomentumCutoff; }
	double getIRMomentumCutoff(){ return irMomentumCutoff; }
	
	double getInitialRKStepSize(){ return initialRKStepSize; }
	void setInitialRKStepSize(double initialRKStepSizeAux){ initialRKStepSize = initialRKStepSizeAux;}
	
	bool getDisplayIterativeStepsOnConsole(){ return displayIterativeStepsOnConsole; }
	void setDisplayIterativeStepsOnConsole(bool displayIterativeStepsOnConsoleAux){ displayIterativeStepsOnConsole = displayIterativeStepsOnConsoleAux; }
	
	bool getSaveFlowEvolution(){ return saveFlowEvolution; }
	
	double getSaveFlowEvolutionAtEachDeltaK(){ return saveFlowEvolutionAtEachDeltaK; }
	void setSaveFlowEvolutionAtEachDeltaK(int saveFlowEvolutionAtEachDeltaKAux){ saveFlowEvolutionAtEachDeltaK = saveFlowEvolutionAtEachDeltaKAux; }

	QuantitiesToFlow getQuantitiesToFlow(){ return quantitiesToFlow; }
	void setQuantitiesToFlow(QuantitiesToFlow quantitiesToFlowAux){ quantitiesToFlow = quantitiesToFlowAux; }
};


class SU2QuarkMesonModelEffectiveAction
{
private:
	SU2QuarkMesonModelParameters modelParams;
	OneDimensionalGrid sigmaGrid;
	QuarkMesonFlowParameters flowParams;

	std::vector<double> sigma;

	std::vector<double> potential;
	std::vector<double> dTPotential;
	std::vector<double> dCpuPotential;
	std::vector<double> dCpdPotential;
	
	std::vector<double> potentialWithSymmetryBreakingTerm;

	std::vector<double> scaleEvolution;
	std::vector<std::vector<double>> potentialEvolution;
	std::vector<std::vector<double>> dTPotentialEvolution;
	std::vector<std::vector<double>> dCpuPotentialEvolution;
	std::vector<std::vector<double>> dCpdPotentialEvolution;

	int numberIterativeSteps = 0;

private:
	void setUVPotential();
	void setSigmaField(){ sigma = sigmaGrid.getGridPoints(); }
	void setNumberIterativeSteps(int numberIterativeStepsAux){ numberIterativeSteps = numberIterativeStepsAux; }

	void nextStepFromRK4Pot(double , double , std::vector<double> & );
	void nextStepFromRK4PotDTPot(double , double , std::vector<double> & , std::vector<double> & );
	void nextStepFromRK4PotDCpPot(double , double , std::vector<double> &, std::vector<double> &, std::vector<double> &);
	void nextStepFromRK4PotDTPotDCpPot(double , double , std::vector<double> &, std::vector<double> &, std::vector<double> &, std::vector<double> &);

	void saveFlowPropertiesAtThisMomentumScale(double , double , int &);

	void rhsFlowEqsPot(double , std::vector<double> , std::vector<double> &);
	void rhsFlowEqsPotDTPot(double , std::vector<double> , std::vector<double> &, 
									 std::vector<double> , std::vector<double> & );
	void rhsFlowEqsPotDCpPot(double , std::vector<double> , std::vector<double> &, 
									  std::vector<double> , std::vector<double> &, 
									  std::vector<double> , std::vector<double> & );
	void rhsFlowEqsPotDTPotDCpPot(double , std::vector<double> , std::vector<double> &, 
										   std::vector<double> , std::vector<double> &, 
										   std::vector<double> , std::vector<double> &, 
										   std::vector<double> , std::vector<double> & );
	
	void addUVContributionFromHighMomentumModes();
	void addExplicitSymmetryBreakingTerm();

public:
	SU2QuarkMesonModelEffectiveAction(){};
	SU2QuarkMesonModelEffectiveAction(SU2QuarkMesonModelParameters , OneDimensionalGrid , QuarkMesonFlowParameters );
	
	SU2QuarkMesonModelParameters getModelParams(){ return modelParams; }
	OneDimensionalGrid getSigmaGrid(){ return sigmaGrid; }
	QuarkMesonFlowParameters getFlowParams(){ return flowParams; }

	int getNumberIterativeSteps(){ return numberIterativeSteps; }
	std::vector<double> getSigmaField(){ return sigma; }
	std::vector<double> getPotential(){ return potential; }
	std::vector<double> getDTPotential(){ return dTPotential; }
	std::vector<double> getDCpuPotential(){ return dCpuPotential; }
	std::vector<double> getDCpdPotential(){ return dCpdPotential; }
	std::vector<double> getPotentialWithSymmetryBreakingTerm(){ return potentialWithSymmetryBreakingTerm; }
	double getSigmaField(int i){ return sigma[i]; }
	double getPotential(int i){ return potential[i]; }
	double getDTPotential(int i){ return dTPotential[i]; }
	double getDCpuPotential(int i){ return dCpuPotential[i]; }
	double getDCpdPotential(int i){ return dCpdPotential[i]; }
	double getPotentialWithSymmetryBreakingTerm(int i){ return potentialWithSymmetryBreakingTerm[i]; }
	
	void solveFlowEqsLPA();

	double optimalStepYokota(double, double );

	void logParametersToFile(std::string );
	void logEffectiveActionToFile(std::string );
	void logFlowEvolutionToFile(std::string );
	void logAllToFiles(std::string );

	void printPotentialVsSigmaOnConsole();
};


#endif

