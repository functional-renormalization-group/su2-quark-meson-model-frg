#ifndef SU2QUARKMESONMODELGRANDPOTENTIALFILEPARSER_H
#define SU2QUARKMESONMODELGRANDPOTENTIALFILEPARSER_H

#include <vector>
#include "IniFileParser.h"


class SU2QuarkMesonModelGrandPotentialFileParser 
{
private:
    const IniFileParser& parser;

    std::string sectionModelParameters = "ModelParameters";
    std::string sectionGridParameters = "GridParameters";
    std::string sectionFlowParameters = "FlowParameters";
    std::string sectionInterpolationParameters = "InterpolationParameters";
    std::string sectionParallelComputingParameters = "ParallelComputingParameters";
    std::string sectionThermodynamicParameters = "ThermodynamicParameters";
    
    std::string invalidFileMessage = "Error: Invalid configuration found in the " + parser.getFilename() + " file.";

public:
    SU2QuarkMesonModelGrandPotentialFileParser(const IniFileParser& p) : parser(p) {}

     bool validateFileQuality() const;

private:
    std::string conditionMustBeSatisfiedMessage(const std::string , const std::string ) const;
    bool validateModelParameters() const;
    bool validateGridParameters() const;
    bool validateFlowParameters() const;
    bool validateInterpolationParameters() const;
    bool validateParallelComputingParameters() const;
    bool validateThermodynamicParameters(const std::pair<std::string, std::map<std::string, std::string>> ) const;
    bool validateThermodynamicParameters() const ;
};


#endif
