#include "SU2QuarkMesonModelGrandPotentialFileParser.h"
#include "OneDimensionalGrid.h"
#include "SU2QuarkMesonModelEffectiveAction.h"
#include "SU2QuarkMesonModelGrandPotential.h"


using namespace std;


string SU2QuarkMesonModelGrandPotentialFileParser::conditionMustBeSatisfiedMessage(const string section, const string condition) const
{   
    string message = "In the section '" + section + "', the following condition must be satisfied: " + condition + " .";
    return message;
}


bool SU2QuarkMesonModelGrandPotentialFileParser::validateFileQuality() const 
{    
    // Check for missing sections
    bool allRequiredSectionsPresent = true;
    vector<string> requiredSections = {
        sectionModelParameters, 
        sectionGridParameters, 
        sectionFlowParameters,
        sectionInterpolationParameters, 
        sectionParallelComputingParameters
    };
    for (int i = 0; i < int(requiredSections.size()); ++i) 
    {
        string section = requiredSections[i];
        if (parser.getSectionsData(section).empty()) 
        {   
            allRequiredSectionsPresent = false;
            cout << "Missing required section: " << section << endl;
        }
    }

    // Check for at least one [ThermodynamicParameters] section
    bool hasThermoSection = false;
    vector<map<string, string>> thermoSections = parser.getSectionsData(sectionThermodynamicParameters);
    for (int i = 0; i < int(thermoSections.size()); ++i) 
    {
        hasThermoSection = true;
        break;
    }
    if ( hasThermoSection==false )
    { 
        cout << invalidFileMessage << endl;
        cout << "At least one '" <<  sectionThermodynamicParameters << "' section is required" << endl; 
    }

    // Validate individual sections
    bool areModelParametersValid = validateModelParameters();
    bool areGridParametersValid = validateGridParameters();
    bool areFlowParametersValid = validateFlowParameters();
    bool areInterpolationParametersValid = validateInterpolationParameters();
    bool areParallelComputingParametersValid = validateParallelComputingParameters();
    bool areThermodynamicParametersValid = validateThermodynamicParameters();

    // The function return true only if all tests passed
    if ( allRequiredSectionsPresent &&
         hasThermoSection &&
         areModelParametersValid && 
         areGridParametersValid &&
         areFlowParametersValid &&
         areInterpolationParametersValid &&
         areParallelComputingParametersValid &&
         areThermodynamicParametersValid )
    { 
        return true; 
    }
    else{ return false; }
}


bool SU2QuarkMesonModelGrandPotentialFileParser::validateModelParameters() const 
{   
    string section = sectionModelParameters;

    // Ensure Lambda>0
    bool isLambdaValid = true;
    double Lambda = parser.getDouble(section, "Lambda", -1.0);
    if (Lambda <= 0)
    { 
        isLambdaValid = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section, "Lambda>0") << endl;
    }

    // The function return true only if all tests passed
    if ( isLambdaValid )
    { 
        return true; 
    }
    else{ return false; }
}


bool SU2QuarkMesonModelGrandPotentialFileParser::validateGridParameters() const 
{   
    string section = sectionGridParameters;

    // Ensure sigmaMin>0
    bool isSigmaMinValid = true;
    double sigmaMin = parser.getDouble(section, "sigmaMin", -1.0);
    if (sigmaMin <= 0)
    {   
        isSigmaMinValid = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section, "sigmaMin>0") << endl;
    }

    // Ensure sigmaMax>0
    bool isSigmaMaxValid = true;
    double sigmaMax = parser.getDouble(section, "sigmaMax", -1.0);
    if (sigmaMax <= 0)
    {   
        isSigmaMaxValid = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section, "sigmaMax>0") << endl;
    }

    // Ensure sigmaMax>kIR
    bool isSigmaMaxBiggerThanSigmaMin = true;
    if ( (sigmaMax-sigmaMin)<=0)
    {   
        isSigmaMaxBiggerThanSigmaMin = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section, "sigmaMax>sigmaMin") << endl;
    }

    // Ensure numberOfGridPoints>0
    bool isNumberOfGridPointsValid = true;
    int numberOfGridPoints = parser.getInt(section, "numberOfGridPoints", -10);
    if (numberOfGridPoints <= 0)
    { 
        isNumberOfGridPointsValid = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section, "numberOfGridPoints>0") << endl;
    }

    // Ensure derivativeMethodOnGrid belongs to the list of DifferentiationMethod1D 
    bool isDerivativeMethodOnGridValid = false;
    string derivativeMethodOnGrid = parser.getValue(section, "derivativeMethodOnGrid");
    int numberOfMethods = static_cast<int>(DifferentiationMethod1D(DifferentiationMethod1DCount));
    for (int i = 1; i < numberOfMethods; ++i) 
    {
        if ( derivativeMethodOnGrid==toStringDifferentiationMethod1D(static_cast<DifferentiationMethod1D>(i)) )
        {
            isDerivativeMethodOnGridValid = true;
            break;
        }
    }
    if ( isDerivativeMethodOnGridValid==false )
    {   
        cout << invalidFileMessage << "\n";
        cout << "In the section '" << section << "', the parameter '" << derivativeMethodOnGrid << "' is not found in the list of allowed methods.\n";
        cout << "The allowed values are:\n";
        for (int i = 1; i < numberOfMethods; i++)
        {   
            cout << toStringDifferentiationMethod1D(static_cast<DifferentiationMethod1D>(i)) << "\n";
        }
    }

    // The function return true only if all tests passed
    if ( isSigmaMinValid && 
         isSigmaMaxValid &&
         isSigmaMaxBiggerThanSigmaMin &&
         isNumberOfGridPointsValid &&
         isDerivativeMethodOnGridValid )
    { 
        return true; 
    }
    else{ return false; }
}


bool SU2QuarkMesonModelGrandPotentialFileParser::validateFlowParameters() const 
{   
    string section = sectionFlowParameters;

    // Ensure kUV>0
    bool isKUVValid = true;
    double kUV = parser.getDouble(section, "kUV", -1.0);
    if (kUV <= 0)
    {   
        isKUVValid = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section, "kUV>0") << endl;
    }
    
    // Ensure kIR>0
    bool isKIRValid = true;
    double kIR = parser.getDouble(section, "kIR", -1.0);
    if ( kIR<=0 )
    {   
        isKIRValid = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section, "kIR>0") << endl;
    }

    // Ensure kUV>kIR
    bool isKUVBiggerThanKIR = true;
    if ( (kUV-kIR)<=0)
    {   
        isKUVBiggerThanKIR = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section, "kUV>kIR") << endl;
    }

    // Ensure quantitiesToFlow belongs to the list of QuantitiesToFlow 
    bool isQuantitiesToFlowValid = false;
    string quantitiesToFlow = parser.getValue(section, "quantitiesToFlow");
    int numberOfMethods = static_cast<int>(QuantitiesToFlow(QuantitiesToFlowCount));
    for (int i = 0; i < numberOfMethods; ++i) 
    {
        if ( quantitiesToFlow==toStringQuantitiesToFlow(static_cast<QuantitiesToFlow>(i)) )
        {
            isQuantitiesToFlowValid = true;
            break;
        }
    }
    if ( isQuantitiesToFlowValid==false )
    {   
        cout << invalidFileMessage << "\n";
        cout << "In the section '" << section << "', the parameter '" << quantitiesToFlow << "' is not found in the list of existing types of flows.\n";
        cout << "The allowed values are:\n";
        for (int i = 0; i < numberOfMethods; i++)
        {   
            cout << toStringQuantitiesToFlow(static_cast<QuantitiesToFlow>(i)) << "\n";
        }
    }

    // The function return true only if all tests passed
    if ( isKUVValid &&
         isKIRValid &&
         isKUVBiggerThanKIR &&
         isQuantitiesToFlowValid )
    { 
        return true; 
    }
    else{ return false; }
}


bool SU2QuarkMesonModelGrandPotentialFileParser::validateInterpolationParameters() const 
{   
    string section = sectionInterpolationParameters;

    // Ensure type belongs to the list of QuantitiesToFlow 
    bool isTypeValid = false;
    string type = parser.getValue(section, "type");
    int numberOfMethods = static_cast<int>(SU2QuarkMesonModelInterpolation::InterpolationMethod(SU2QuarkMesonModelInterpolation::InterpolationMethodCount));
    for (int i = 0; i < numberOfMethods; ++i) 
    {   
        if ( type==SU2QuarkMesonModelInterpolation::toStringInterpolationMethod(static_cast<SU2QuarkMesonModelInterpolation::InterpolationMethod>(i)) )
        {
            isTypeValid = true;
            break;
        }
    }
    if ( isTypeValid==false )
    {   
        cout << invalidFileMessage << "\n";
        cout << "In the section '" << section << "', the parameter '" << type << "' is not found in the list of allowed methods.\n";
        cout << "The allowed values are:\n";
        for (int i = 0; i < numberOfMethods; i++)
        {   
            cout << SU2QuarkMesonModelInterpolation::toStringInterpolationMethod(static_cast<SU2QuarkMesonModelInterpolation::InterpolationMethod>(i)) << endl;
        }
    }

    // The function return true only if all tests passed
    if ( isTypeValid )
    { 
        return true; 
    }
    else{ return false; }
}


bool SU2QuarkMesonModelGrandPotentialFileParser::validateParallelComputingParameters() const 
{   
    string section = sectionParallelComputingParameters;

    // Ensure numberOfCPUs>0
    bool isNumberOfCPUsValid = true;
    int numberOfCPUs = parser.getInt(section, "numberOfCPUs", -10);
    if (numberOfCPUs <= 0)
    { 
        isNumberOfCPUsValid = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section, "numberOfCPUs>0") << endl;
    }

    // The function return true only if all tests passed
    if ( isNumberOfCPUsValid )
    { 
        return true; 
    }
    else{ return false; }
}


bool SU2QuarkMesonModelGrandPotentialFileParser::validateThermodynamicParameters(const pair< string, map<string, string> > section) const 
{   
    // Ensure temperature>0
    bool isTemperatureValid = true;
    double temperature = parser.getDouble(section.second, "temperature");
    if (temperature <= 0)
    {   
        isTemperatureValid = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section.first, "temperature>0") << endl;
    }

    // Ensure cpMin>0
    bool isCpMinValid = true;
    double cpMin = parser.getDouble(section.second, "cpMin", -1.0);
    if (cpMin <= 0)
    {   
        isCpMinValid = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section.first, "cpMin>0") << endl;
    }

    // Ensure cpMax>0
    bool isCpMaxValid = true;
    double cpMax = parser.getDouble(section.second, "cpMax", -1.0);
    if (cpMax <= 0)
    {   
        isCpMaxValid = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section.first, "cpMax>0") << endl;
    }

    // Ensure CpMax>cpMin
    bool isCpMaxBiggerThanCpMin = true;
    if ( (cpMax-cpMin)<=0)
    {   
        isCpMaxBiggerThanCpMin = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section.first, "cpMax>cpMin") << endl;
    }

    // Ensure numberCp>0
    bool isNumberCpValid = true;
    int numberCp = parser.getInt(section.second, "numberCp", -10);
    if (numberCp <= 0)
    { 
        isNumberCpValid = false;
        cout << invalidFileMessage << "\n" << conditionMustBeSatisfiedMessage(section.first, "numberCp>0") << endl;
    }

    // The function return true only if all tests passed
    if ( isTemperatureValid &&
         isCpMinValid &&
         isCpMaxValid &&
         isCpMaxBiggerThanCpMin &&
         isNumberCpValid )
    { 
        return true; 
    }
    else{ return false; }
}


bool SU2QuarkMesonModelGrandPotentialFileParser::validateThermodynamicParameters() const 
{   
    const std::vector<std::pair<std::string, std::map<std::string, std::string>>> thermodynamicData = parser.getSections("ThermodynamicParameters");

    bool areThermodynamicParametersSectionValid = true;
    for (int i = 0; i < int(thermodynamicData.size()); ++i) 
	{   
        areThermodynamicParametersSectionValid = validateThermodynamicParameters(thermodynamicData[i]) && areThermodynamicParametersSectionValid;
    }
    
    return areThermodynamicParametersSectionValid;
}
