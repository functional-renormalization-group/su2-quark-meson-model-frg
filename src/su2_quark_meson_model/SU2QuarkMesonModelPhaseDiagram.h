#ifndef SU2QUARKMESONMODELPHASEDIAGRAM_H
#define SU2QUARKMESONMODELPHASEDIAGRAM_H

#include <vector>
#include "SU2QuarkMesonModelEffectiveAction.h"
#include "SU2QuarkMesonModelGrandPotential.h"


class SU2QuarkMesonModelPhaseDiagramAtFixedTemperature
{
private:
	SU2QuarkMesonModelGrandPotential grandPotential;
	InterpolationGSL1DimMethod interpolationMethod;
	RootFindingMethod rootFindingMethodAux = brent;
	double rootFindingPrecision = 1E-8;

public:
	SU2QuarkMesonModelPhaseDiagramAtFixedTemperature(SU2QuarkMesonModelGrandPotential , InterpolationGSL1DimMethod );
	SU2QuarkMesonModelPhaseDiagramAtFixedTemperature(std::string , InterpolationGSL1DimMethod );

	void setRootFindingMethod(RootFindingMethod method){ rootFindingMethodAux = method; }
	void setRootFindingPrecision(double precision){ rootFindingPrecision = precision; }

	void checkIfTemperatureIsFixed();
	bool checkNumberOf3Solutions();

	double calculateUpQuarkChemicalPotentialOfPhaseTransition();
	std::vector<double> calculateUpQuarkChemicalPotentialOfSpinodals();
};



#endif