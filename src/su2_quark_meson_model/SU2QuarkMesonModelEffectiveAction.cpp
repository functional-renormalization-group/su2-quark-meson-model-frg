#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <omp.h>
#include <algorithm>
#include "SU2QuarkMesonModelFlowEquations.h"
#include "SU2QuarkMesonModelEffectiveAction.h"

using namespace std;


vector<double> vectorFromSet(vector<vector<double>*> setOfVectors, const int index)
{	
	const int length = setOfVectors.size();
	if ( index < 0 || index >= length ){ cout << "Index in readVector out of range!\n"; }
	vector<double> vector = *setOfVectors[index];
	
	return vector;
}


//function that calculates the sum of two vectors as: sum = u + alpha*v
vector<double> vectorSum(vector<double> &u, double alpha, vector<double> &v)
{	
	const int Nu = u.size();
	const int Nv = v.size();
	if ( Nu!=Nv ){ cout << "Trying to sum vectors of different dimensions!\n"; abort(); }
	else
	{
		if ( alpha>0.0 || alpha<0.0 )
		{ 	
			vector<double> vectorAux(Nu,0);
			for (int i = 0; i < Nu; ++i)
			{ 
				vectorAux[i] = u[i] + alpha*v[i]; 
			}
			
			return vectorAux;
		}
		else{ return u; }
	}
}


//Runge-Kutta 4th order method: function evaluation at the next step
double yStepRK4(double y, double dt, double k1, double k2, double k3, double k4)
{	
    double yNext = y + dt*( 1.0*k1 + 2.0*k2 + 2.0*k3 + 1.0*k4 )/6.0;

    return yNext;
}


//Runge-Kutta 4th order method: time step
double tStep(double t, double dt)
{
    double tNext = t + dt;

    return tNext;
}


SU2QuarkMesonModelParameters::SU2QuarkMesonModelParameters(double Lambda, double gS, double m, double lambda, double c, double T, double cpu, double cpd)
{	
	modelCutoff = Lambda;
	yukawaCoupling = gS;

	c2UVPotential = m;
	c4UVPotential = lambda;
	c1SymmetryBreaking = c;

	temperature = T;
	chemicalPotentialUpQuark = cpu;
	chemicalPotentialDownQuark = cpd;
}


SU2QuarkMesonModelParameters::SU2QuarkMesonModelParameters(SU2QuarkMesonModelParameters* modelParams)
{
	modelCutoff = ((class SU2QuarkMesonModelParameters *)(modelParams))->modelCutoff;
	yukawaCoupling = ((class SU2QuarkMesonModelParameters *)(modelParams))->yukawaCoupling;

	c2UVPotential = ((class SU2QuarkMesonModelParameters *)(modelParams))->c2UVPotential;
	c4UVPotential = ((class SU2QuarkMesonModelParameters *)(modelParams))->c4UVPotential;
	c1SymmetryBreaking = ((class SU2QuarkMesonModelParameters *)(modelParams))->c1SymmetryBreaking;

	temperature = ((class SU2QuarkMesonModelParameters *)(modelParams))->temperature;
	chemicalPotentialUpQuark = ((class SU2QuarkMesonModelParameters *)(modelParams))->chemicalPotentialUpQuark;
	chemicalPotentialDownQuark = ((class SU2QuarkMesonModelParameters *)(modelParams))->chemicalPotentialDownQuark;
};

std::string toStringQuantitiesToFlow(QuantitiesToFlow quantitiesToFlow)
{
    switch (quantitiesToFlow)
    {
        case pot:
            return "pot";
        case potDTPot:
            return "potDTPot";
        case potDCpPot:
            return "potDCpPot";
        case potDTPotDCpPot:
            return "potDTPotDCpPot";
        default:
            std::cout << "The QuantitiesToFlow given is not listed in the toString method! Returning an empty string!\n";
            return "";
    }
}

QuantitiesToFlow stringToQuantitiesToFlow(const string& quantitiesToFlow) 
{
    if (quantitiesToFlow=="pot"){ return pot; } 
	else if (quantitiesToFlow=="potDTPot"){ return potDTPot; } 
	else if (quantitiesToFlow=="potDCpPot"){ return potDCpPot; }
	else if (quantitiesToFlow=="potDTPotDCpPot"){ return potDTPotDCpPot; }
	else
	{ 	
		cout << "Invalid input string to convert to 'QuantitiesToFlow'! Providing: " + quantitiesToFlow + ". Returning 'pot'!\n"; 
		return pot; 
	}
}


QuarkMesonFlowParameters::QuarkMesonFlowParameters(double kUV, double kIR)
{	
	uvMomentumCutoff = kUV;
	irMomentumCutoff = kIR;
}


QuarkMesonFlowParameters::QuarkMesonFlowParameters(double kUV, double kIR, QuantitiesToFlow quantitiesToFlowAux)
{	
	uvMomentumCutoff = kUV;
	irMomentumCutoff = kIR;
	quantitiesToFlow = quantitiesToFlowAux;
}


void SU2QuarkMesonModelEffectiveAction::setUVPotential()
{	
	const double m = modelParams.getC2UVPotential();
	const double lambda = modelParams.getC4UVPotential();
	const int N = sigmaGrid.getGridLength();

	for (int i = 0; i < N; ++i)
	{	
		potential[i] = su2QuarkMesonModelUVPotential(m, lambda, sigma[i]);
	}
}


SU2QuarkMesonModelEffectiveAction::SU2QuarkMesonModelEffectiveAction(SU2QuarkMesonModelParameters modelParamsAux, OneDimensionalGrid sigmaGridAux, QuarkMesonFlowParameters flowParamsAux)
{	
	modelParams = modelParamsAux;
	sigmaGrid = sigmaGridAux;
	flowParams = flowParamsAux;

	int N = sigmaGrid.getGridLength();

	//set sigma field in vector
	setSigmaField();

	//set potential field in vector
	potential.assign(N, 0.0);
	setUVPotential();

	//set potential field in vector
	potentialWithSymmetryBreakingTerm.assign(N, 0.0);

	//set potential derivatives fields in vector
	dTPotential.assign(N, 0.0);
	dCpuPotential.assign(N, 0.0);
	dCpdPotential.assign(N, 0.0);
}


void SU2QuarkMesonModelEffectiveAction::solveFlowEqsLPA()
{
	//parameters
	const double Lambda = modelParams.getModelCutoff();
	const double kUV = flowParams.getUVMomentumCutoff();
	const double kIR = flowParams.getIRMomentumCutoff();


	//renormalization group "time" parameters
    double t = log(kUV/Lambda); //initial momentum scale converted to time
    double dt = flowParams.getInitialRKStepSize();


    //auxiliar accumulators
    int saveFlowAux = 0; //auxiliar accumulator used to save the flow results


    //apply the Runge-Kutta method until the IR momentum is reached
	while( Lambda*exp(t) >= kIR )
    {  
        //calculate the optimal step size and compare with previous step Yokota et. al.
		dt = -optimalStepYokota(t, dt);

		//solve the flow equations using the Runge-Kutta method
		if ( flowParams.getQuantitiesToFlow()==pot )
		{
			nextStepFromRK4Pot(t, dt, potential);
		}
		else if ( flowParams.getQuantitiesToFlow()==potDTPot )
		{
			nextStepFromRK4PotDTPot(t, dt, potential, dTPotential);
		}
		else if ( flowParams.getQuantitiesToFlow()==potDCpPot )
		{
			nextStepFromRK4PotDCpPot(t, dt, potential, dCpuPotential, dCpdPotential);
		}
		else if ( flowParams.getQuantitiesToFlow()==potDTPotDCpPot )
		{
			nextStepFromRK4PotDTPotDCpPot(t, dt, potential, dTPotential, dCpuPotential, dCpdPotential);
		}
		else{ cout << "Problem with the choice of which Quantities to Flow in solveFlowEqsLPA() function!"; abort(); }

        t = tStep(t, dt);


        //save flow properties at specific momentum shells
        if ( flowParams.getSaveFlowEvolution() ){ saveFlowPropertiesAtThisMomentumScale(Lambda, t, saveFlowAux); }


        //increase the accumulator for the number of iterative steps
		setNumberIterativeSteps( getNumberIterativeSteps() + 1 );


        //if "displayRKStepsOnConsole" is true, print the current scale and Runge-Kutta step size
        if ( flowParams.getDisplayIterativeStepsOnConsole() ){ printf("k = %.15f \t t = %.15f \t dt = %.15f \n", Lambda*exp(t), t, dt); }
    }
	
	addExplicitSymmetryBreakingTerm();
}


void SU2QuarkMesonModelEffectiveAction::saveFlowPropertiesAtThisMomentumScale(double Lambda, double t, int &saveFlowAux)
{	
	//convert renormalization time to momentum
	const double k = Lambda*exp(t);

    if ( k < Lambda-saveFlowAux*flowParams.getSaveFlowEvolutionAtEachDeltaK() )
    {	
        scaleEvolution.push_back( k );
        potentialEvolution.push_back( potential );

		if ( flowParams.getQuantitiesToFlow()==potDTPot )
		{ 
			dTPotentialEvolution.push_back( dTPotential ); 
		}
		else if ( flowParams.getQuantitiesToFlow()==potDCpPot )
		{ 
			dCpuPotentialEvolution.push_back( dCpuPotential ); 
			dCpdPotentialEvolution.push_back( dCpdPotential ); 
		}
		else if ( flowParams.getQuantitiesToFlow()==potDTPotDCpPot )
		{ 
			dTPotentialEvolution.push_back( dTPotential ); 
			dCpuPotentialEvolution.push_back( dCpuPotential ); 
			dCpdPotentialEvolution.push_back( dCpdPotential ); 
		}
        
        saveFlowAux = saveFlowAux + 1;
    }
}


double SU2QuarkMesonModelEffectiveAction::optimalStepYokota(double t, double dt)
{	
	//necessary grid parameters
	const int N = sigmaGrid.getGridLength();
	const double sigmaDelta = sigmaGrid.getGridDelta();

	//necessary model parameters
	const double Lambda = modelParams.getModelCutoff();
	const double T = modelParams.getTemperature();

	//convert renormalization time to momentum
	const double k = Lambda*exp(t);


	//calculate potential derivatives
	vector<double> dUdsigma = sigmaGrid.calculate1stDerivative(potential);
	vector<double> d2Udsigma2 = sigmaGrid.calculate2ndDerivative(potential);


	//define F(sigma) and G(sigma): Threshold functions for numerical stabilities
	vector<double> F(N,0.0);
	vector<double> G(N,0.0);

	//calculate F(sigma) and G(sigma)
	if ( T>0 )
	{
		for (int i = 0; i < N; ++i)
		{	
			double Esigma = su2QuarkMesonModelEnergySigma(k, d2Udsigma2[i]);
			double Epion = su2QuarkMesonModelEnergyPion(k, sigma[i], dUdsigma[i]);
			F[i] = su2QuarkMesonModelOptimalStepYokotaF(T, k, Epion, sigma[i]);
			G[i] = su2QuarkMesonModelOptimalStepYokotaG(T, k, Esigma);
		}
	}
	else
	{
		for (int i = 0; i < N; ++i)
		{	
			double Esigma = su2QuarkMesonModelEnergySigma(k, d2Udsigma2[i]);
			double Epion = su2QuarkMesonModelEnergyPion(k, sigma[i], dUdsigma[i]);
			F[i] = su2QuarkMesonModelOptimalStepYokotaFVacuum(k, Epion, sigma[i]);
			G[i] = su2QuarkMesonModelOptimalStepYokotaGVacuum(k, Esigma);
		}	
	}

	//CHECK IF THE MODULUS OF THE NEXT CALCULATED STEP IS SMALLER THAN THE MODULUS OF THE PREVIOUS STEP
	double condition1, condition2;
	dt = fabs(dt);

	//CYCLE TO CHOOSE THE SMALLEST STEP SIZE NEEDED TO PROCEDE IN A STABLE WAY
	for (int i = 0; i < N; ++i)
	{	
		condition1 = 2*fabs(G[i])/pow(F[i],2); //condition 1 proposed by YOKOTA ET AL
		if ( condition1 < dt )
		{
			dt = condition1;
		}
		condition2 = pow(sigmaDelta,2)/( 2*fabs(G[i]) ); //condition 2 proposed by YOKOTA ET AL
		if ( condition2 < dt )
		{
			dt = condition2;
		}
	}

	return dt;
}


void SU2QuarkMesonModelEffectiveAction::nextStepFromRK4Pot(double t, double dt, vector<double> &U)
{
	const int N = sigmaGrid.getGridLength();

	const double c1 = 0.0; 
	const double c2 = 0.5; 
	const double c3 = 0.5; 
	const double c4 = 1.0; 

	vector<double> k1U(N,0.0);
	vector<double> k2U(N,0.0);
	vector<double> k3U(N,0.0);
	vector<double> k4U(N,0.0);

	rhsFlowEqsPot(tStep(t, c1*dt), vectorSum(U, c1*dt, k1U), k1U);
	rhsFlowEqsPot(tStep(t, c2*dt), vectorSum(U, c2*dt, k1U), k2U);
	rhsFlowEqsPot(tStep(t, c3*dt), vectorSum(U, c3*dt, k2U), k3U);
	rhsFlowEqsPot(tStep(t, c4*dt), vectorSum(U, c4*dt, k3U), k4U);

	for (int i = 0; i < N; ++i)
    {
    	U[i] = yStepRK4(U[i], dt, k1U[i], k2U[i], k3U[i], k4U[i]);
    }
}


void SU2QuarkMesonModelEffectiveAction::nextStepFromRK4PotDTPot(double t, double dt, vector<double> &U, vector<double> &DTU)
{
	const int N = sigmaGrid.getGridLength();

	const double c1 = 0.0; 
	const double c2 = 0.5; 
	const double c3 = 0.5; 
	const double c4 = 1.0; 

	vector<double> k1U(N,0.0);
	vector<double> k2U(N,0.0);
	vector<double> k3U(N,0.0);
	vector<double> k4U(N,0.0);

	vector<double> k1DTU(N,0.0);
	vector<double> k2DTU(N,0.0);
	vector<double> k3DTU(N,0.0);
	vector<double> k4DTU(N,0.0);

	rhsFlowEqsPotDTPot(tStep(t, c1*dt), vectorSum(U,   c1*dt, k1U),   k1U, 
										vectorSum(DTU, c1*dt, k1DTU), k1DTU);

	rhsFlowEqsPotDTPot(tStep(t, c2*dt), vectorSum(U,   c2*dt, k1U),   k2U, 
										vectorSum(DTU, c2*dt, k1DTU), k2DTU);

	rhsFlowEqsPotDTPot(tStep(t, c3*dt), vectorSum(U,   c3*dt, k2U),   k3U, 
										vectorSum(DTU, c3*dt, k2DTU), k3DTU);

	rhsFlowEqsPotDTPot(tStep(t, c4*dt), vectorSum(U,   c4*dt, k3U),   k4U, 
										vectorSum(DTU, c4*dt, k3DTU), k4DTU);

	for (int i = 0; i < N; ++i)
    {
	    U[i]   = yStepRK4(U[i],   dt, k1U[i],   k2U[i],   k3U[i],   k4U[i]);
        DTU[i] = yStepRK4(DTU[i], dt, k1DTU[i], k2DTU[i], k3DTU[i], k4DTU[i]);
    }
}


void SU2QuarkMesonModelEffectiveAction::nextStepFromRK4PotDCpPot(double t, double dt, vector<double> &U, vector<double> &DCpuU, vector<double> &DCpdU)
{
	const int N = sigmaGrid.getGridLength();

	const double c1 = 0.0; 
	const double c2 = 0.5; 
	const double c3 = 0.5; 
	const double c4 = 1.0; 

	vector<double> k1U(N,0.0);
	vector<double> k2U(N,0.0);
	vector<double> k3U(N,0.0);
	vector<double> k4U(N,0.0);

	vector<double> k1DCpuU(N,0.0);
	vector<double> k2DCpuU(N,0.0);
	vector<double> k3DCpuU(N,0.0);
	vector<double> k4DCpuU(N,0.0);

	vector<double> k1DCpdU(N,0.0);
	vector<double> k2DCpdU(N,0.0);
	vector<double> k3DCpdU(N,0.0);
	vector<double> k4DCpdU(N,0.0);

	rhsFlowEqsPotDCpPot(tStep(t, c1*dt), vectorSum(U,     c1*dt, k1U), 	   k1U, 
										 vectorSum(DCpuU, c1*dt, k1DCpuU), k1DCpuU,
										 vectorSum(DCpdU, c1*dt, k1DCpdU), k1DCpdU);

	rhsFlowEqsPotDCpPot(tStep(t, c2*dt), vectorSum(U,     c2*dt, k1U), 	   k2U, 
										 vectorSum(DCpuU, c2*dt, k1DCpuU), k2DCpuU,
										 vectorSum(DCpdU, c2*dt, k1DCpdU), k2DCpdU);

	rhsFlowEqsPotDCpPot(tStep(t, c3*dt), vectorSum(U,     c3*dt, k2U),     k3U, 
										 vectorSum(DCpuU, c3*dt, k2DCpuU), k3DCpuU,
										 vectorSum(DCpdU, c3*dt, k2DCpdU), k3DCpdU);

	rhsFlowEqsPotDCpPot(tStep(t, c4*dt), vectorSum(U,     c4*dt, k3U),     k4U, 
										 vectorSum(DCpuU, c4*dt, k3DCpuU), k4DCpuU,
										 vectorSum(DCpdU, c4*dt, k3DCpdU), k4DCpdU);

	for (int i = 0; i < N; ++i)
    {
	    U[i]     = yStepRK4(U[i],     dt, k1U[i], 	  k2U[i],     k3U[i],     k4U[i]);
        DCpuU[i] = yStepRK4(DCpuU[i], dt, k1DCpuU[i], k2DCpuU[i], k3DCpuU[i], k4DCpuU[i]);
		DCpdU[i] = yStepRK4(DCpdU[i], dt, k1DCpdU[i], k2DCpdU[i], k3DCpdU[i], k4DCpdU[i]);
    }
}


void SU2QuarkMesonModelEffectiveAction::nextStepFromRK4PotDTPotDCpPot(double t, double dt, vector<double> &U, vector<double> &DTU, vector<double> &DCpuU, vector<double> &DCpdU)
{
	const int N = sigmaGrid.getGridLength();

	const double c1 = 0.0; 
	const double c2 = 0.5; 
	const double c3 = 0.5; 
	const double c4 = 1.0; 

	vector<double> k1U(N,0.0);
	vector<double> k2U(N,0.0);
	vector<double> k3U(N,0.0);
	vector<double> k4U(N,0.0);

	vector<double> k1DTU(N,0.0);
	vector<double> k2DTU(N,0.0);
	vector<double> k3DTU(N,0.0);
	vector<double> k4DTU(N,0.0);

	vector<double> k1DCpuU(N,0.0);
	vector<double> k2DCpuU(N,0.0);
	vector<double> k3DCpuU(N,0.0);
	vector<double> k4DCpuU(N,0.0);

	vector<double> k1DCpdU(N,0.0);
	vector<double> k2DCpdU(N,0.0);
	vector<double> k3DCpdU(N,0.0);
	vector<double> k4DCpdU(N,0.0);

	rhsFlowEqsPotDTPotDCpPot(tStep(t, c1*dt), vectorSum(U,     c1*dt, k1U), 	k1U,
											  vectorSum(DTU,   c1*dt, k1DTU),   k1DTU,
										 	  vectorSum(DCpuU, c1*dt, k1DCpuU), k1DCpuU,
										 	  vectorSum(DCpdU, c1*dt, k1DCpdU), k1DCpdU);

	rhsFlowEqsPotDTPotDCpPot(tStep(t, c2*dt), vectorSum(U,     c2*dt, k1U), 	k2U,
											  vectorSum(DTU,   c2*dt, k1DTU),   k2DTU,
										      vectorSum(DCpuU, c2*dt, k1DCpuU), k2DCpuU,
										      vectorSum(DCpdU, c2*dt, k1DCpdU), k2DCpdU);

	rhsFlowEqsPotDTPotDCpPot(tStep(t, c3*dt), vectorSum(U,     c3*dt, k2U),     k3U,
											  vectorSum(DTU,   c3*dt, k2DTU), 	k3DTU,
										      vectorSum(DCpuU, c3*dt, k2DCpuU), k3DCpuU,
										      vectorSum(DCpdU, c3*dt, k2DCpdU), k3DCpdU);

	rhsFlowEqsPotDTPotDCpPot(tStep(t, c4*dt), vectorSum(U,     c4*dt, k3U),     k4U,
											  vectorSum(DTU,   c4*dt, k3DTU), 	k4DTU,
										      vectorSum(DCpuU, c4*dt, k3DCpuU), k4DCpuU,
										      vectorSum(DCpdU, c4*dt, k3DCpdU), k4DCpdU);

	for (int i = 0; i < N; ++i)
    {
	    U[i]     = yStepRK4(U[i],     dt, k1U[i], 	  k2U[i],     k3U[i],     k4U[i]);
		DTU[i]   = yStepRK4(DTU[i],   dt, k1DTU[i],   k2DTU[i],   k3DTU[i],   k4DTU[i]);
        DCpuU[i] = yStepRK4(DCpuU[i], dt, k1DCpuU[i], k2DCpuU[i], k3DCpuU[i], k4DCpuU[i]);
		DCpdU[i] = yStepRK4(DCpdU[i], dt, k1DCpdU[i], k2DCpdU[i], k3DCpdU[i], k4DCpdU[i]);
    }
}


void SU2QuarkMesonModelEffectiveAction::rhsFlowEqsPot(double t, vector<double> U, vector<double> &rhsU)
{	
	//necessary grid parameters
	const int N = sigmaGrid.getGridLength();

	//necessary model parameters
	const double Lambda = modelParams.getModelCutoff();
	const double gS = modelParams.getYukawaCoupling();
	const double T = modelParams.getTemperature();
	const double cpu = modelParams.getChemicalPotentialUpQuark();
	const double cpd = modelParams.getChemicalPotentialDownQuark();

	//convert renormalization time to momentum
	const double k = Lambda*exp(t);

	//calculate potential derivatives
	vector<double> dUdsigma = sigmaGrid.calculate1stDerivative(U);
	vector<double> d2Udsigma2 = sigmaGrid.calculate2ndDerivative(U);

	if ( T>0 )
	{
		for (int i = 0; i < N; ++i)
		{	
			double Esigma = su2QuarkMesonModelEnergySigma(k, d2Udsigma2[i]);
			double Epion = su2QuarkMesonModelEnergyPion(k, sigma[i], dUdsigma[i]);
			double Equark = su2QuarkMesonModelEnergyQuark(k, sigma[i], gS);

			rhsU[i] = su2QuarkMesonModelRHSFlowEqPotential(T, cpu, cpd, k, Esigma, Epion, Equark);
		}
	}
	else
	{
		for (int i = 0; i < N; ++i)
		{	
			double Esigma = su2QuarkMesonModelEnergySigma(k, d2Udsigma2[i]);
			double Epion = su2QuarkMesonModelEnergyPion(k, sigma[i], dUdsigma[i]);
			double Equark = su2QuarkMesonModelEnergyQuark(k, sigma[i], gS);

			rhsU[i] = su2QuarkMesonModelRHSFlowEqPotentialVacuum(k, Esigma, Epion, Equark);
		}	
	}
}


void SU2QuarkMesonModelEffectiveAction::rhsFlowEqsPotDTPot(double t, vector<double> U, vector<double> &rhsU, vector<double> DTU, vector<double> &rhsDTU)
{	
	//necessary grid parameters
	const int N = sigmaGrid.getGridLength();

	//necessary model parameters
	const double Lambda = modelParams.getModelCutoff();
	const double gS = modelParams.getYukawaCoupling();
	const double T = modelParams.getTemperature();
	const double cpu = modelParams.getChemicalPotentialUpQuark();
	const double cpd = modelParams.getChemicalPotentialDownQuark();

	//convert renormalization time to momentum
	const double k = Lambda*exp(t);

	//calculate potential derivatives with respect to sigma
	vector<double> dsigmaU = sigmaGrid.calculate1stDerivative(U);
	vector<double> dsigma2U = sigmaGrid.calculate2ndDerivative(U);

	//calculate derivative with respect to sigma of the temperature derivative of the potential
	vector<double> dsigmaDTU = sigmaGrid.calculate1stDerivative(DTU);
	vector<double> dsigma2DTU = sigmaGrid.calculate2ndDerivative(DTU);

	if ( T>0 )
	{
		for (int i = 0; i < N; ++i)
		{
			double Esigma = su2QuarkMesonModelEnergySigma(k, dsigma2U[i]);
			double Epion = su2QuarkMesonModelEnergyPion(k, sigma[i], dsigmaU[i]);
			double Equark = su2QuarkMesonModelEnergyQuark(k, sigma[i], gS);

			rhsU[i] = su2QuarkMesonModelRHSFlowEqPotential(T, cpu, cpd, k, Esigma, Epion, Equark);
			rhsDTU[i] = su2QuarkMesonModelRHSFlowEqDTPotential(T, cpu, cpd, k, Esigma, Epion, Equark, sigma[i], dsigmaDTU[i], dsigma2DTU[i]);
		}
	}
	else
	{
		cout << "No alternative to the finite temperature flow equation for the derivative of the potential w.r.t. temperature is found!\n"; 
		abort();
	}
}


void SU2QuarkMesonModelEffectiveAction::rhsFlowEqsPotDCpPot(double t, vector<double> U, vector<double> &rhsU, vector<double> DCpuU, vector<double> &rhsDCpuU, vector<double> DCpdU, vector<double> &rhsDCpdU)
{	
	//necessary grid parameters
	const int N = sigmaGrid.getGridLength();

	//necessary model parameters
	const double Lambda = modelParams.getModelCutoff();
	const double gS = modelParams.getYukawaCoupling();
	const double T = modelParams.getTemperature();
	const double cpu = modelParams.getChemicalPotentialUpQuark();
	const double cpd = modelParams.getChemicalPotentialDownQuark();

	//convert renormalization time to momentum
	const double k = Lambda*exp(t);

	//calculate potential derivatives with respect to sigma
	vector<double> dsigmaU = sigmaGrid.calculate1stDerivative(U);
	vector<double> dsigma2U = sigmaGrid.calculate2ndDerivative(U);

	//calculate derivative with respect to sigma of the chemical potential derivatives of the potential
	vector<double> dsigmaDCpuU = sigmaGrid.calculate1stDerivative(DCpuU);
	vector<double> dsigmaDCpdU = sigmaGrid.calculate1stDerivative(DCpdU);
	vector<double> dsigma2DCpuU = sigmaGrid.calculate2ndDerivative(DCpuU);
	vector<double> dsigma2DCpdU = sigmaGrid.calculate2ndDerivative(DCpdU);

	if ( T>0 )
	{
		for (int i = 0; i < N; ++i)
		{
			double Esigma = su2QuarkMesonModelEnergySigma(k, dsigma2U[i]);
			double Epion = su2QuarkMesonModelEnergyPion(k, sigma[i], dsigmaU[i]);
			double Equark = su2QuarkMesonModelEnergyQuark(k, sigma[i], gS);

			rhsU[i] = su2QuarkMesonModelRHSFlowEqPotential(T, cpu, cpd, k, Esigma, Epion, Equark);
			rhsDCpuU[i] = su2QuarkMesonModelRHSFlowEqDTPotential(T, cpu, cpd, k, Esigma, Epion, Equark, sigma[i], dsigmaDCpuU[i], dsigma2DCpuU[i]);
			rhsDCpdU[i] = su2QuarkMesonModelRHSFlowEqDTPotential(T, cpu, cpd, k, Esigma, Epion, Equark, sigma[i], dsigmaDCpdU[i], dsigma2DCpdU[i]);
		}
	}
	else
	{
		cout << "No alternative to the finite temperature flow equation for the derivative of the potential w.r.t. chemical potential is found!\n"; 
		abort();
	}
}


void SU2QuarkMesonModelEffectiveAction::rhsFlowEqsPotDTPotDCpPot(double t, vector<double> U,     vector<double> &rhsU, 
																		   vector<double> DTU,   vector<double> &rhsDTU, 
																		   vector<double> DCpuU, vector<double> &rhsDCpuU, 
																		   vector<double> DCpdU, vector<double> &rhsDCpdU)
{	
	//necessary grid parameters
	const int N = sigmaGrid.getGridLength();

	//necessary model parameters
	const double Lambda = modelParams.getModelCutoff();
	const double gS = modelParams.getYukawaCoupling();
	const double T = modelParams.getTemperature();
	const double cpu = modelParams.getChemicalPotentialUpQuark();
	const double cpd = modelParams.getChemicalPotentialDownQuark();

	//convert renormalization time to momentum
	const double k = Lambda*exp(t);

	//calculate potential derivatives with respect to sigma
	vector<double> dsigmaU = sigmaGrid.calculate1stDerivative(U);
	vector<double> dsigma2U = sigmaGrid.calculate2ndDerivative(U);

	//calculate derivative with respect to sigma of the temperature derivative of the potential
	vector<double> dsigmaDTU = sigmaGrid.calculate1stDerivative(DTU);
	vector<double> dsigma2DTU = sigmaGrid.calculate2ndDerivative(DTU);

	//calculate derivative with respect to sigma of the chemical potential derivatives of the potential
	vector<double> dsigmaDCpuU = sigmaGrid.calculate1stDerivative(DCpuU);
	vector<double> dsigmaDCpdU = sigmaGrid.calculate1stDerivative(DCpdU);
	vector<double> dsigma2DCpuU = sigmaGrid.calculate2ndDerivative(DCpuU);
	vector<double> dsigma2DCpdU = sigmaGrid.calculate2ndDerivative(DCpdU);

	if ( T>0 )
	{
		for (int i = 0; i < N; ++i)
		{
			double Esigma = su2QuarkMesonModelEnergySigma(k, dsigma2U[i]);
			double Epion = su2QuarkMesonModelEnergyPion(k, sigma[i], dsigmaU[i]);
			double Equark = su2QuarkMesonModelEnergyQuark(k, sigma[i], gS);

			rhsU[i] = su2QuarkMesonModelRHSFlowEqPotential(T, cpu, cpd, k, Esigma, Epion, Equark);
			rhsDTU[i] = su2QuarkMesonModelRHSFlowEqDTPotential(T, cpu, cpd, k, Esigma, Epion, Equark, sigma[i], dsigmaDTU[i], dsigma2DTU[i]);
			rhsDCpuU[i] = su2QuarkMesonModelRHSFlowEqDCpfPotential(T, cpu, k, Esigma, Epion, Equark, sigma[i], dsigmaDCpuU[i], dsigma2DCpuU[i]);
			rhsDCpdU[i] = su2QuarkMesonModelRHSFlowEqDCpfPotential(T, cpd, k, Esigma, Epion, Equark, sigma[i], dsigmaDCpdU[i], dsigma2DCpdU[i]);
		}
	}
	else
	{
		cout << "No alternative to the finite temperature flow equations for the derivative of the potential w.r.t. temperature and/or chemical potential is found!\n"; 
		abort();
	}
}


void SU2QuarkMesonModelEffectiveAction::addUVContributionFromHighMomentumModes(){}


void SU2QuarkMesonModelEffectiveAction::addExplicitSymmetryBreakingTerm()
{	
	//necessary grid parameters
	const int N = sigmaGrid.getGridLength();
	
	//necessary model parameters
	const double cBreaking = modelParams.getC1SymmetryBreaking();

	//simply add the symmetry braking term
	for (int i = 0; i < N; ++i){ potentialWithSymmetryBreakingTerm[i] = potential[i] - cBreaking*sigma[i]; }
}


void SU2QuarkMesonModelEffectiveAction::logFlowEvolutionToFile(string fileName)
{	
	int dataPrecision = 15;
	int colW = 25;

	std::ofstream fileFlowEvol;
    fileFlowEvol.open("flowEvolution_"+fileName+".dat", std::fstream::in | std::ofstream::out | std::ios::trunc);
    fileFlowEvol.precision(dataPrecision);

	fileFlowEvol.width(colW); fileFlowEvol << "k-scale[GeV]";
	fileFlowEvol.width(colW); fileFlowEvol << "sigma[GeV]";
    fileFlowEvol.width(colW); fileFlowEvol << "pot[GeV^4]";
	
	if ( flowParams.getQuantitiesToFlow()==potDTPot )
	{ 
		fileFlowEvol.width(colW); fileFlowEvol << "dTPot[GeV^3]";
	}
	else if ( flowParams.getQuantitiesToFlow()==potDCpPot )
	{ 
		fileFlowEvol.width(colW); fileFlowEvol << "dCpuPot[GeV^3]";
		fileFlowEvol.width(colW); fileFlowEvol << "dCpdPot[GeV^3]";
	}
	else if ( flowParams.getQuantitiesToFlow()==potDTPotDCpPot )
	{ 
		fileFlowEvol.width(colW); fileFlowEvol << "dTPot[GeV^3]";
		fileFlowEvol.width(colW); fileFlowEvol << "dCpuPot[GeV^3]";
		fileFlowEvol.width(colW); fileFlowEvol << "dCpdPot[GeV^3]";
	}
	
	fileFlowEvol << "\n";
	fileFlowEvol << "\n\n";

	const int flowLength = scaleEvolution.size();
	const int N = sigmaGrid.getGridLength();

	for (int i = 0; i < flowLength; ++i)
    {   
		for (int j = 0; j < N; j++)
		{	
			fileFlowEvol.width(colW); fileFlowEvol << scaleEvolution[i];
			fileFlowEvol.width(colW); fileFlowEvol << sigma[j];
        	fileFlowEvol.width(colW); fileFlowEvol << potentialEvolution[i][j];

			if ( flowParams.getQuantitiesToFlow()==potDTPot )
			{ 
				fileFlowEvol.width(colW); fileFlowEvol << dTPotentialEvolution[i][j]; 
			}
			else if ( flowParams.getQuantitiesToFlow()==potDCpPot )
			{ 
				fileFlowEvol.width(colW); fileFlowEvol << dCpuPotentialEvolution[i][j]; 
				fileFlowEvol.width(colW); fileFlowEvol << dCpdPotentialEvolution[i][j]; 
			}
			else if ( flowParams.getQuantitiesToFlow()==potDTPotDCpPot )
			{ 
				fileFlowEvol.width(colW); fileFlowEvol << dTPotentialEvolution[i][j]; 
				fileFlowEvol.width(colW); fileFlowEvol << dCpuPotentialEvolution[i][j]; 
				fileFlowEvol.width(colW); fileFlowEvol << dCpdPotentialEvolution[i][j]; 
			}

        	fileFlowEvol << "\n";
		}
		fileFlowEvol << "\n\n";
    }

	fileFlowEvol.close();

}


void SU2QuarkMesonModelEffectiveAction::logParametersToFile(string fileName)
{	
	int dataPrecision = 15;

	ofstream fileLog;
    fileLog.open("parameters_"+fileName+".dat", std::fstream::in | std::ofstream::out | std::ios::trunc);
    fileLog.precision(dataPrecision);
	
	const double cutoff = modelParams.getModelCutoff();

	fileLog << "SU2 Quark-Meson model parameters:\n";
    fileLog << "modelCutoff[GeV] = " << cutoff << "\n";
	fileLog << "yukawaCoupling = " << modelParams.getYukawaCoupling() << "\n";
	fileLog << "c2UVPotential/modelCutoff = " << modelParams.getC2UVPotential()/cutoff << "\n";
	fileLog << "c4UVPotential = " << modelParams.getC4UVPotential() << "\n";
	fileLog << "c1SymmetryBreaking/modelCutoff^3 = " << modelParams.getC1SymmetryBreaking()/( pow(cutoff,3) ) << "\n";
	fileLog << "temperature[GeV] = " << modelParams.getTemperature() << "\n";
	fileLog << "chemicalPotentialUpQuark[GeV] = " << modelParams.getChemicalPotentialUpQuark() << "\n";
	fileLog << "chemicalPotentialDownQuark[GeV] = " << modelParams.getChemicalPotentialDownQuark() << "\n";
	
	fileLog << "\n\n\n";

	fileLog << "OneDimensionalGrid parameters:\n";
    fileLog << "gridMin[GeV] = " << sigmaGrid.getGridMin() << "\n";
    fileLog << "gridMax[GeV] = " << sigmaGrid.getGridMax() << "\n";
	fileLog << "gridLength = " << sigmaGrid.getGridLength() << "\n";
	fileLog << "gridDelta[GeV] = " << sigmaGrid.getGridDelta() << "\n";
	fileLog << "DerivativeMethod1D = " << sigmaGrid.getDerivativeMethod1DString() << "\n";

	fileLog << "\n\n\n";

	fileLog << "Flow parameters:\n";
    fileLog << "uvMomentumCutoff[GeV] = " << flowParams.getUVMomentumCutoff() << "\n";
	fileLog << "irMomentumCutoff[GeV] = " << flowParams.getIRMomentumCutoff() << "\n";
	fileLog << "initialRKStepSize[adimensional] = " << flowParams.getInitialRKStepSize() << "\n";
	fileLog << "saveFlowEvolution = " << flowParams.getSaveFlowEvolution() << "\n";
	fileLog << "saveFlowEvolutionAtEachDeltaK[GeV] = " << flowParams.getSaveFlowEvolutionAtEachDeltaK() << "\n";
	fileLog << "QuantitiesToFlow = " << toStringQuantitiesToFlow(flowParams.getQuantitiesToFlow()) << "\n";

	fileLog << "\n\n\n";

	fileLog << "Flow properties:\n";
	fileLog << "numberIterativeSteps = " << getNumberIterativeSteps() << "\n";

	fileLog.close();
}


void SU2QuarkMesonModelEffectiveAction::logEffectiveActionToFile(string fileName)
{	
	/*
	Explicit symmetry breaking is done after the flow is solved, however this method can be used at any scale.
	So, here we calculate the potential with explicit chiral symmetry braking for the cases where this method 
	is called before the infrared is reached.
	*/
	addExplicitSymmetryBreakingTerm();

	int dataPrecision = 15;
	int colW = 25;

    std::ofstream fileSol;
    fileSol.open("effectiveAction_"+fileName+".dat", std::fstream::in | std::ofstream::out | std::ios::trunc);
    fileSol.precision(dataPrecision);

	fileSol.width(colW); fileSol << "sigma[GeV]";
    fileSol.width(colW); fileSol << "pot[GeV^4]";
	fileSol.width(colW); fileSol << "potSymBroken[GeV^4]";

	if ( flowParams.getQuantitiesToFlow()==potDTPot )
	{ 
		fileSol.width(colW); fileSol << "dTPot[GeV^3]";
	}
	else if ( flowParams.getQuantitiesToFlow()==potDCpPot )
	{ 
		fileSol.width(colW); fileSol << "dCpuPot[GeV^3]";
		fileSol.width(colW); fileSol << "dCpdPot[GeV^3]";
	}
	else if ( flowParams.getQuantitiesToFlow()==potDTPotDCpPot )
	{ 
		fileSol.width(colW); fileSol << "dTPot[GeV^3]";
		fileSol.width(colW); fileSol << "dCpuPot[GeV^3]";
		fileSol.width(colW); fileSol << "dCpdPot[GeV^3]";
	}

	fileSol << "\n";

	for (int i = 0; i < sigmaGrid.getGridLength(); ++i)
    {   
        fileSol.width(colW); fileSol << sigma[i];
        fileSol.width(colW); fileSol << potential[i];
		fileSol.width(colW); fileSol << potentialWithSymmetryBreakingTerm[i];

		if ( flowParams.getQuantitiesToFlow()==potDTPot )
		{ 
			fileSol.width(colW); fileSol << dTPotential[i]; 
		}
		else if ( flowParams.getQuantitiesToFlow()==potDCpPot )
		{ 
			fileSol.width(colW); fileSol << dCpuPotential[i]; 
			fileSol.width(colW); fileSol << dCpdPotential[i]; 
		}
		else if ( flowParams.getQuantitiesToFlow()==potDTPotDCpPot )
		{ 
			fileSol.width(colW); fileSol << dTPotential[i]; 
			fileSol.width(colW); fileSol << dCpuPotential[i]; 
			fileSol.width(colW); fileSol << dCpdPotential[i]; 
		}

        fileSol << "\n";
    }

	fileSol.close();
}


void SU2QuarkMesonModelEffectiveAction::logAllToFiles(string fileName)
{	
	logParametersToFile(fileName);
	logEffectiveActionToFile(fileName);
	if( flowParams.getSaveFlowEvolution() ){ logFlowEvolutionToFile(fileName); }
}


void SU2QuarkMesonModelEffectiveAction::printPotentialVsSigmaOnConsole()
{	
	const int N = sigmaGrid.getGridLength();
	
	for (int i = 0; i < N; ++i)
	{ 
		printf("%i \t %.10f \t %.10f \n", i, sigma[i], potential[i]);
	}
}

