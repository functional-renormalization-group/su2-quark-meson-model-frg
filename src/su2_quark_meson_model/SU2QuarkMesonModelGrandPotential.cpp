#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "SU2QuarkMesonModelGrandPotential.h"
#include "LinearInterpolation.h"
#include "QuadraticInterpolationGivenExtremum.h"


using namespace std;


branchIdentifier identifySolution(int a)
{
	if	    (a==0){ return singleSolution; }
	else if (a==1){ return highSolution; }
	else if (a==2){ return middleSolution; }
	else if (a==3){ return lowSolution; }
	else{ cout << "No such type of branch exists!"; abort(); }
}


InterpolationGSL1DimMethod SU2QuarkMesonModelInterpolation::convertMethod()
{
	switch (method)
	{
		case linearGSL: return linear;
		case steffenGSL: return steffen;
		case cubicGSL: return cubic;
		case akimaGSL: return akima;
		case polynomialGSL: return polynomial;
		default: cout << "Invalid InterpolationMethod value! Aborting!\n"; abort();
	}
}


bool SU2QuarkMesonModelInterpolation::usingGSLMethod()
{
	if (method==linearGSL || method==steffenGSL || method==cubicGSL || method==akimaGSL || method==polynomialGSL)
	{
		return true;
	}
	else
	{
		return false;
	}
}


string SU2QuarkMesonModelInterpolation::toStringInterpolationMethod(InterpolationMethod methodAux) 
{
	switch (methodAux) 
	{
		case hybrid1:
			return "hybrid1";
		case linearGSL:
			return "linearGSL";
		case steffenGSL:
			return "steffenGSL";
		case cubicGSL:
			return "cubicGSL";
		case akimaGSL:
			return "akimaGSL";
		case polynomialGSL:
			return "polynomialGSL";
		default:
			return "UnknownMethod";
	}
}


SU2QuarkMesonModelInterpolation::InterpolationMethod SU2QuarkMesonModelInterpolation::stringToInterpolationMethod(const string& methodName) 
{
	if (methodName == "hybrid1") 
	{
		return SU2QuarkMesonModelInterpolation::hybrid1;
	} 
	else if (methodName == "linearGSL") 
	{
		return SU2QuarkMesonModelInterpolation::linearGSL;
	} 
	else if (methodName == "steffenGSL") 
	{
		return SU2QuarkMesonModelInterpolation::steffenGSL;
	} 
	else if (methodName == "cubicGSL") 
	{
		return SU2QuarkMesonModelInterpolation::cubicGSL;
	} 
	else if (methodName == "akimaGSL") 
	{
		return SU2QuarkMesonModelInterpolation::akimaGSL;
	} 
	else if (methodName == "polynomialGSL") 
	{
		return SU2QuarkMesonModelInterpolation::polynomialGSL;
	} else 
	{
		// Handle unknown methods gracefully
		std::cerr << "Unknown interpolation method: " << methodName << std::endl;
		return SU2QuarkMesonModelInterpolation::hybrid1; // Default fallback
	}
}


vector<SU2QuarkMesonModelActionExtremum> evaluateExtremaInterpolationHybrid1(SU2QuarkMesonModelEffectiveAction effectiveAction)
{
	vector<SU2QuarkMesonModelActionExtremum> grandPotential;

	// Get parameters
	double temperature = effectiveAction.getModelParams().getTemperature();
	double chemicalPotentialUpQuark = effectiveAction.getModelParams().getChemicalPotentialUpQuark();
	double chemicalPotentialDownQuark = effectiveAction.getModelParams().getChemicalPotentialDownQuark();
	double g = effectiveAction.getModelParams().getYukawaCoupling();

	// Calculate  derivatives of the grand potential with respect to the sigma field
	vector<double> dSigmaPotentialFromFiniteDifferences = effectiveAction.getSigmaGrid().calculate1stDerivative(effectiveAction.getPotential());
	vector<double> dSigma2PotentialFromFiniteDifferences = effectiveAction.getSigmaGrid().calculate2ndDerivative(effectiveAction.getPotential());
	
	// Calculate the first derivative of the potential including the symmetry breaking term with respect to sigma, DSigmaPotentialWSB
	vector<double> DSigmaPotentialWSBVec = effectiveAction.getSigmaGrid().calculate1stDerivative(effectiveAction.getPotentialWithSymmetryBreakingTerm());

	// Check where the above derivative changes sign (the name guesses comes from the fact that these values could be used in a root finding algorithm + interpolation of the first derivative)
	vector<int> guesses;
	for (int i = 0; i < int(DSigmaPotentialWSBVec.size())-1; ++i)
	{	
		if      ( DSigmaPotentialWSBVec[i] < 0 && DSigmaPotentialWSBVec[i+1] > 0 ){ guesses.push_back(i); }
		else if ( DSigmaPotentialWSBVec[i] > 0 && DSigmaPotentialWSBVec[i+1] < 0 ){ guesses.push_back(i); }
	}
	
	// Evaluate the Sigma Field points at which the potential is an extremum using the custom linear interpolation of the derivative above
	vector<double> sigmaExtrema;
	for (int i = 0; i < int(guesses.size()); i++)
	{	
		int index = guesses[i];

		double sigmaXi = effectiveAction.getSigmaField(index);
		double sigmaXiPlusOne = effectiveAction.getSigmaField(index+1);

		LinearInterpolation DSigmaPotentialWSB(sigmaXi, DSigmaPotentialWSBVec[index], sigmaXiPlusOne, DSigmaPotentialWSBVec[index+1]);
		sigmaExtrema.push_back( DSigmaPotentialWSB.evaluateZeroCrossing() );
	}
	sort(sigmaExtrema.begin(), sigmaExtrema.end());

	for (int i = 0; i < int(sigmaExtrema.size()); i++)
	{	
		SU2QuarkMesonModelActionExtremum sol;

		// Set temperature, chemical potentials and number of extrema of this particular solution
		sol.setTemperature(temperature);
		sol.setChemicalPotentialUpQuark(chemicalPotentialUpQuark);
		sol.setChemicalPotentialDownQuark(chemicalPotentialDownQuark);
		sol.setNumberOfExtrema( int(sigmaExtrema.size()) );
		
		// Set type of solution
		if ( sol.getNumberOfExtrema()==1 ){ sol.setBranchIdentifier( singleSolution ); }
		else
		{
			if ( i==0 ){ sol.setBranchIdentifier( lowSolution ); }
			else if ( i==1 ){ sol.setBranchIdentifier( middleSolution ); }
			else if ( i==2 ){ sol.setBranchIdentifier( highSolution ); }
		}
		
		// Set value for the Sigma field and quark mass
		sol.setSigmaExtremum(sigmaExtrema[i]);
		sol.setQuarkMassExtremum(g*sigmaExtrema[i]);

		int index = guesses[i];
		double sigmaXi = effectiveAction.getSigmaField(index);
		double sigmaXiPlusOne = effectiveAction.getSigmaField(index+1);
		
		// Evaluate and set value of the effective potential at the extremum, including explicit symmetry breaking (using Quadratic Interpolation Given Extremum)
		double brokenPotentialAtXi = effectiveAction.getPotentialWithSymmetryBreakingTerm(index);
		double brokenPotentialAtXiPlusOne = effectiveAction.getPotentialWithSymmetryBreakingTerm(index+1);
		QuadraticInterpolationGivenExtremum GrandPotentialNearExtremum(sigmaXi, brokenPotentialAtXi, sigmaXiPlusOne, brokenPotentialAtXiPlusOne, sigmaExtrema[i]);
		sol.setGrandPotentialExtremum( GrandPotentialNearExtremum.evaluateExtremumOrdinate() );
		
		// Evaluate and set values for observables using the custom linear interpolation
		LinearInterpolation dSigmaPotential(sigmaXi, dSigmaPotentialFromFiniteDifferences[index], sigmaXiPlusOne, dSigmaPotentialFromFiniteDifferences[index+1]);
		LinearInterpolation dSigma2Potential(sigmaXi, dSigma2PotentialFromFiniteDifferences[index], sigmaXiPlusOne, dSigma2PotentialFromFiniteDifferences[index+1]);
		LinearInterpolation dTPotential(sigmaXi, effectiveAction.getDTPotential(index), sigmaXiPlusOne, effectiveAction.getDTPotential(index+1));
		LinearInterpolation dCpuPotential(sigmaXi, effectiveAction.getDCpuPotential(index), sigmaXiPlusOne, effectiveAction.getDCpuPotential(index+1));
		LinearInterpolation dCpdPotential(sigmaXi, effectiveAction.getDCpdPotential(index), sigmaXiPlusOne, effectiveAction.getDCpdPotential(index+1));

		sol.setPionCurvMass2Extremum( dSigmaPotential.evaluate(sigmaExtrema[i])/sigmaExtrema[i] );
		sol.setSigmaCurvMass2Extremum( dSigma2Potential.evaluate(sigmaExtrema[i]) );
		sol.setDTGrandPotentialExtremum( dTPotential.evaluate(sigmaExtrema[i]) );
		sol.setDCpuGrandPotentialExtremum( dCpuPotential.evaluate(sigmaExtrema[i]) );
		sol.setDCpdGrandPotentialExtremum( dCpdPotential.evaluate(sigmaExtrema[i]) );
		
		grandPotential.push_back( sol );
	}
	
	return grandPotential;
}


vector<SU2QuarkMesonModelActionExtremum> evaluateExtremaInterpolationGSL(SU2QuarkMesonModelEffectiveAction effectiveAction, InterpolationGSL1DimMethod method, RootFindingMethod methodRoot, double precision)
{
	vector<SU2QuarkMesonModelActionExtremum> grandPotential;

	// Get parameters
	double temperature = effectiveAction.getModelParams().getTemperature();
	double chemicalPotentialUpQuark = effectiveAction.getModelParams().getChemicalPotentialUpQuark();
	double chemicalPotentialDownQuark = effectiveAction.getModelParams().getChemicalPotentialDownQuark();
	double g = effectiveAction.getModelParams().getYukawaCoupling();
	
	// Interpolate grand potential with and without considering the symmetry breaking term
	InterpolationGSL1Dim sigmaVsPotential(method, effectiveAction.getSigmaField(), effectiveAction.getPotential());
	InterpolationGSL1Dim sigmaVsBrokenPotential(method, effectiveAction.getSigmaField(), effectiveAction.getPotentialWithSymmetryBreakingTerm());
	
	// Interpolate derivatives of the grand potential with respect to sigma
	vector<double> dSigmaPotentialFromFiniteDifferences = effectiveAction.getSigmaGrid().calculate1stDerivative(effectiveAction.getPotential());
	vector<double> dSigma2PotentialFromFiniteDifferences = effectiveAction.getSigmaGrid().calculate2ndDerivative(effectiveAction.getPotential());
	InterpolationGSL1Dim dSigmaPotential(method, effectiveAction.getSigmaField(), dSigmaPotentialFromFiniteDifferences);
	InterpolationGSL1Dim dSigma2Potential(method, effectiveAction.getSigmaField(), dSigma2PotentialFromFiniteDifferences);
	
	// Interpolate derivatives of the grand potential with respect to thermodynamic variables
	InterpolationGSL1Dim sigmaVsDTPotential(method, effectiveAction.getSigmaField(), effectiveAction.getDTPotential());
	InterpolationGSL1Dim sigmaVsDCpuPotential(method, effectiveAction.getSigmaField(), effectiveAction.getDCpuPotential());
	InterpolationGSL1Dim sigmaVsDCpdPotential(method, effectiveAction.getSigmaField(), effectiveAction.getDCpdPotential());

	// Obtain the Sigma Field points at which the potential is an extremum
	vector<double> sigmaExtrema = sigmaVsBrokenPotential.findRoots1stDerivative(methodRoot, precision);
	sort(sigmaExtrema.begin(), sigmaExtrema.end());
	
	// Evaluate the observables of interest at all extremum points
	for (int i = 0; i < int(sigmaExtrema.size()); i++)
	{	
		SU2QuarkMesonModelActionExtremum sol;

		// Set temperature, chemical potentials and number of extrema of this particular solution
		sol.setTemperature(temperature);
		sol.setChemicalPotentialUpQuark(chemicalPotentialUpQuark);
		sol.setChemicalPotentialDownQuark(chemicalPotentialDownQuark);
		sol.setNumberOfExtrema( int(sigmaExtrema.size()) );
		
		// Set type of solution
		if ( sol.getNumberOfExtrema()==1 ){ sol.setBranchIdentifier( singleSolution ); }
		else
		{
			if ( i==0 ){ sol.setBranchIdentifier( lowSolution ); }
			else if ( i==1 ){ sol.setBranchIdentifier( middleSolution ); }
			else if ( i==2 ){ sol.setBranchIdentifier( highSolution ); }
		}

		// Set value for the Sigma field and quark mass
		sol.setSigmaExtremum(sigmaExtrema[i]);
		sol.setQuarkMassExtremum(g*sigmaExtrema[i]);

		// Grand Potential
		sol.setGrandPotentialExtremum( sigmaVsBrokenPotential.evaluate(sigmaExtrema[i]) );
		
		// Derivatives of the grand potential with respect to sigma
		sol.setPionCurvMass2Extremum( dSigmaPotential.evaluate(sigmaExtrema[i])/(sigmaExtrema[i]) );
		sol.setSigmaCurvMass2Extremum( dSigma2Potential.evaluate(sigmaExtrema[i]) );

		// Derivatives of the grand potential with respect to thermodynamic variables
		sol.setDTGrandPotentialExtremum( sigmaVsDTPotential.evaluate(sigmaExtrema[i]) );
		sol.setDCpuGrandPotentialExtremum( sigmaVsDCpuPotential.evaluate(sigmaExtrema[i]) );
		sol.setDCpdGrandPotentialExtremum( sigmaVsDCpdPotential.evaluate(sigmaExtrema[i]) );
		

		grandPotential.push_back( sol );
	}
	
	return grandPotential;
}


SU2QuarkMesonModelGrandPotential::SU2QuarkMesonModelGrandPotential(SU2QuarkMesonModelEffectiveAction effectiveAction, SU2QuarkMesonModelInterpolation interpolation)
{ 	
	if ( interpolation.getMethod()==SU2QuarkMesonModelInterpolation::hybrid1 )
	{
		grandPotential = evaluateExtremaInterpolationHybrid1(effectiveAction);
	}
	else if ( interpolation.usingGSLMethod()==true )
	{
		grandPotential = evaluateExtremaInterpolationGSL(effectiveAction, interpolation.convertMethod(), interpolation.getMethodRootFindingGSL(), interpolation.getPrecisionRootFindingGSL());
	}
	else
	{
		cout << "The interpolation method selected was not correctly configured!\n";
	}
}


void SU2QuarkMesonModelGrandPotential::addExtrema(vector<SU2QuarkMesonModelActionExtremum> grandPotentialAux)
{
	grandPotential.insert(grandPotential.end(), grandPotentialAux.begin(), grandPotentialAux.end());
}
	

void SU2QuarkMesonModelGrandPotential::sortBySigma()
{	
	sort(
		grandPotential.begin(), grandPotential.end(), 
	    [](SU2QuarkMesonModelActionExtremum & a, SU2QuarkMesonModelActionExtremum & b) -> bool
		{ 	
			return a.getSigmaExtremum() > b.getSigmaExtremum(); 
		}
		);
}


void SU2QuarkMesonModelGrandPotential::sortBySigmaAndUpQuarkChemicalPotentialInTheHighBranch()
{	
	sortBySigma();
	
	vector<SU2QuarkMesonModelActionExtremum> highBranch;
	for (int i = 0; i < getLength(); i++)
	{
		if ( getExtrema()[i].getBranchIdentifier()==singleSolution )
		{
			highBranch.push_back( grandPotential[i] );
		}
		else if( getExtrema()[i].getBranchIdentifier()==highSolution )
		{
			highBranch.push_back( grandPotential[i] );
		}
		else{ break; }	
	}

	sort(
		highBranch.begin(), highBranch.end(), 
	    [](SU2QuarkMesonModelActionExtremum & a, SU2QuarkMesonModelActionExtremum & b) -> bool
		{ 	
			return a.getChemicalPotentialUpQuark() < b.getChemicalPotentialUpQuark(); 
		}
		);

	grandPotential.erase(grandPotential.begin(),grandPotential.begin()+highBranch.size());	
	grandPotential.insert(grandPotential.begin(), highBranch.begin(), highBranch.end());
}


void SU2QuarkMesonModelGrandPotential::sortBySigmaAndTemperatureInTheHighBranch()
{	
	sortBySigma();
	
	vector<SU2QuarkMesonModelActionExtremum> highBranch;
	for (int i = 0; i < getLength(); i++)
	{
		if ( getExtrema()[i].getBranchIdentifier()==singleSolution )
		{
			highBranch.push_back( grandPotential[i] );
		}
		else if( getExtrema()[i].getBranchIdentifier()==highSolution )
		{
			highBranch.push_back( grandPotential[i] );
		}
		else{ break; }	
	}

	sort(
		highBranch.begin(), highBranch.end(), 
	    [](SU2QuarkMesonModelActionExtremum & a, SU2QuarkMesonModelActionExtremum & b) -> bool
		{ 	
			return a.getTemperature() < b.getTemperature(); 
		}
		);

	grandPotential.erase(grandPotential.begin(),grandPotential.begin()+highBranch.size());	
	grandPotential.insert(grandPotential.begin(), highBranch.begin(), highBranch.end());
}


void SU2QuarkMesonModelGrandPotential::logGrandPotentialToFile(string fileName)
{	
	int dataPrecision = 15;
	int colW = 25;

    std::ofstream fileGrandPotential;
    fileGrandPotential.open("grandPotential_"+fileName+".dat", std::fstream::in | std::ofstream::out | std::ios::trunc);
    fileGrandPotential.precision(dataPrecision);

	fileGrandPotential.width(colW); fileGrandPotential << "T[GeV]";
    fileGrandPotential.width(colW); fileGrandPotential << "Cpu[GeV]";
	fileGrandPotential.width(colW); fileGrandPotential << "Cpd[GeV]";

	fileGrandPotential.width(colW); fileGrandPotential << "Nextrema";
    fileGrandPotential.width(colW); fileGrandPotential << "BranchID";
	fileGrandPotential.width(colW); fileGrandPotential << "sigma[GeV]";

	fileGrandPotential.width(colW); fileGrandPotential << "Mquark[GeV]";
    fileGrandPotential.width(colW); fileGrandPotential << "Mpion^2[GeV^2]";
	fileGrandPotential.width(colW); fileGrandPotential << "Msigma^2[GeV^2]";

	fileGrandPotential.width(colW); fileGrandPotential << "Omega[GeV^4]";
	fileGrandPotential.width(colW); fileGrandPotential << "dTOmega[GeV^3]";
	fileGrandPotential.width(colW); fileGrandPotential << "dCpuOmega[GeV^3]";
	fileGrandPotential.width(colW); fileGrandPotential << "dCpdOmega[GeV^3]";

	fileGrandPotential << "\n";

	for (int i = 0; i < getLength(); i++)
    {   
        fileGrandPotential.width(colW); fileGrandPotential << getTemperature(i);
        fileGrandPotential.width(colW); fileGrandPotential << getChemicalPotentialUpQuark(i);
		fileGrandPotential.width(colW); fileGrandPotential << getChemicalPotentialDownQuark(i);
		
		fileGrandPotential.width(colW); fileGrandPotential << getNumberOfExtrema(i);
		fileGrandPotential.width(colW); fileGrandPotential << getBranchIdentifier(i);
		fileGrandPotential.width(colW); fileGrandPotential << getSigmaExtremum(i);

		fileGrandPotential.width(colW); fileGrandPotential << getQuarkMassExtremum(i);
		fileGrandPotential.width(colW); fileGrandPotential << getPionCurvMass2Extremum(i);
		fileGrandPotential.width(colW); fileGrandPotential << getSigmaCurvMass2Extremum(i);

		fileGrandPotential.width(colW); fileGrandPotential << getGrandPotentialExtremum(i);
		fileGrandPotential.width(colW); fileGrandPotential << getDTGrandPotentialExtremum(i);
		fileGrandPotential.width(colW); fileGrandPotential << getDCpuGrandPotentialExtremum(i);
		fileGrandPotential.width(colW); fileGrandPotential << getDCpdGrandPotentialExtremum(i);

        fileGrandPotential << "\n";
    }

	fileGrandPotential.close();
}


SU2QuarkMesonModelGrandPotential::SU2QuarkMesonModelGrandPotential(SU2QuarkMesonModelParameters modelParams, OneDimensionalGrid sigmaGrid, QuarkMesonFlowParameters flowParams, SU2QuarkMesonModelInterpolation interpolation, int numberThreads, double temperature, vector<double> cpUpQuark, vector<double> cpDownQuark)
{	
	//Evaluating Grand Potential for fixed temperature and given a list of chemical potentials
	if( int(cpUpQuark.size())!=int(cpDownQuark.size()) )
	{ 
		cout << "The size of the lists of chemical potentials for the up and down quarks are not the same!";
		abort();
	}

	#pragma omp parallel num_threads(numberThreads)
	{
		SU2QuarkMesonModelGrandPotential grandPotFixedTempPrivate;
		#pragma omp for nowait
		for (int i = 0; i < int(cpUpQuark.size()); i++)
		{
			//quark-meson model parameters
			double Lambda = modelParams.getModelCutoff();
			double gS = modelParams.getYukawaCoupling();
			double m = modelParams.getC2UVPotential();
			double lambda = modelParams.getC4UVPotential();
			double c = modelParams.getC1SymmetryBreaking();
			SU2QuarkMesonModelParameters modelParamsAux(Lambda, gS, m, lambda, c, temperature, cpUpQuark[i], cpDownQuark[i]);	

			SU2QuarkMesonModelEffectiveAction action(modelParamsAux, sigmaGrid, flowParams);
			action.solveFlowEqsLPA();

			SU2QuarkMesonModelGrandPotential grandPot(action, interpolation);

			grandPotFixedTempPrivate.addExtrema( grandPot.getExtrema() );
		}

		#pragma omp critical
		{
			addExtrema( grandPotFixedTempPrivate.getExtrema() );
		}
	}

	//sort the calculations
	sortBySigmaAndUpQuarkChemicalPotentialInTheHighBranch();
}


SU2QuarkMesonModelGrandPotential::SU2QuarkMesonModelGrandPotential(string fileName)
{	
	double col[13];

    std::ifstream cin(fileName);
    std::string line;
    int lineCounter = 0; //line number counter
    while (std::getline(cin, line)) 
    {
        std::istringstream ss(line);
        
		if( lineCounter>0 )
		{	
			ss >> col[0] >> col[1] >> col[2] >> col[3] >> col[4] >> 
				  col[5] >> col[6] >> col[7] >> col[8] >> col[9] >> 
				  col[10] >> col[11] >> col[12];
		
			SU2QuarkMesonModelActionExtremum actionExtremum;
			actionExtremum.setTemperature(col[0]);
			actionExtremum.setChemicalPotentialUpQuark(col[1]);
			actionExtremum.setChemicalPotentialDownQuark(col[2]);
			actionExtremum.setNumberOfExtrema(col[3]);
			actionExtremum.setBranchIdentifier( identifySolution(int(col[4])) );
			actionExtremum.setSigmaExtremum(col[5]);
			actionExtremum.setQuarkMassExtremum(col[6]);
			actionExtremum.setPionCurvMass2Extremum(col[7]);
			actionExtremum.setSigmaCurvMass2Extremum(col[8]);
			actionExtremum.setGrandPotentialExtremum(col[9]);
			actionExtremum.setDTGrandPotentialExtremum(col[10]);
			actionExtremum.setDCpuGrandPotentialExtremum(col[11]);
			actionExtremum.setDCpdGrandPotentialExtremum(col[12]);

			vector<SU2QuarkMesonModelActionExtremum> actionExtremumAux;
			actionExtremumAux.push_back( actionExtremum );

			addExtrema( actionExtremumAux );
		}

        lineCounter = lineCounter + 1;
    }
    cin.close();
}


SU2QuarkMesonModelGrandPotential::SU2QuarkMesonModelGrandPotential(SU2QuarkMesonModelParameters modelParams, OneDimensionalGrid sigmaGrid, QuarkMesonFlowParameters flowParams, SU2QuarkMesonModelInterpolation interpolation, int numberThreads, vector<double> temperature, double cpUpQuark, double cpDownQuark)
{	
	//Evaluating Grand Potential for a list of temperatures and given values for the up and down chemical potentials
	#pragma omp parallel num_threads(numberThreads)
	{
		SU2QuarkMesonModelGrandPotential grandPotFixedTempPrivate;
		#pragma omp for nowait
		for (int i = 0; i < int(temperature.size()); i++)
		{
			//quark-meson model parameters
			double Lambda = modelParams.getModelCutoff();
			double gS = modelParams.getYukawaCoupling();
			double m = modelParams.getC2UVPotential();
			double lambda = modelParams.getC4UVPotential();
			double c = modelParams.getC1SymmetryBreaking();
			SU2QuarkMesonModelParameters modelParamsAux(Lambda, gS, m, lambda, c, temperature[i], cpUpQuark, cpDownQuark);	

			SU2QuarkMesonModelEffectiveAction action(modelParamsAux, sigmaGrid, flowParams);
			action.solveFlowEqsLPA();

			SU2QuarkMesonModelGrandPotential grandPot(action, interpolation);

			grandPotFixedTempPrivate.addExtrema( grandPot.getExtrema() );
		}

		#pragma omp critical
		{
			addExtrema( grandPotFixedTempPrivate.getExtrema() );
		}
	}

	//sort the calculations
	sortBySigmaAndTemperatureInTheHighBranch();
}
