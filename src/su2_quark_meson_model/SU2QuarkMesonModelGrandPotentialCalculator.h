#ifndef SU2QUARKMESONMODELGRANDPOTENTIALCALCULATOR_H
#define SU2QUARKMESONMODELGRANDPOTENTIALCALCULATOR_H

#include <vector>
#include "SU2QuarkMesonModelGrandPotential.h"
#include "IniFileParser.h"


namespace SU2QuarkMesonModelGrandPotentialCalculator
{

void evalGrandPotentialFixedTempListOfQuarkChemPot(SU2QuarkMesonModelParameters , OneDimensionalGrid , QuarkMesonFlowParameters , SU2QuarkMesonModelInterpolation , int , double , std::vector<double> &);

void evalGrandPotentialFixedTempRangeOfQuarkChemPot(SU2QuarkMesonModelParameters , OneDimensionalGrid , QuarkMesonFlowParameters , SU2QuarkMesonModelInterpolation , int , double , double , double , int );

void evalGrandPotentialFixedQuarkChemPotListOfTemp(SU2QuarkMesonModelParameters , OneDimensionalGrid , QuarkMesonFlowParameters , SU2QuarkMesonModelInterpolation , int , std::vector<double> &, double );

void evalGrandPotentialFixedQuarkChemPotpRangeOfTemp(SU2QuarkMesonModelParameters , OneDimensionalGrid , QuarkMesonFlowParameters , SU2QuarkMesonModelInterpolation , int , double , double , double , int );

void evalGrandPotentialFixedTempRangeOfQuarkChemPot(SU2QuarkMesonModelParameters , OneDimensionalGrid , QuarkMesonFlowParameters , std::vector<SU2QuarkMesonModelInterpolation> , int , double , double , double , int );

void evalGrandPotentialFixedTempRangeOfQuarkChemPot(SU2QuarkMesonModelParameters , OneDimensionalGrid , QuarkMesonFlowParameters , int , double , double , double , int );

void evalGrandPotentialFixedTemp(const IniFileParser& );

}


#endif