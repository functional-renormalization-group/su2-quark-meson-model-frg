#ifndef SU2QUARKMESONMODELGRANDPOTENTIAL_H
#define SU2QUARKMESONMODELGRANDPOTENTIAL_H

#include <vector>
#include "SU2QuarkMesonModelEffectiveAction.h"


enum branchIdentifier { singleSolution, highSolution, middleSolution, lowSolution };


branchIdentifier identifySolution(int );


class SU2QuarkMesonModelActionExtremum
{
private:
	double temperature = 0.0;
	double chemicalPotentialUpQuark = 0.0;
	double chemicalPotentialDownQuark = 0.0;
	
	int numberOfExtrema = 0;
	branchIdentifier extremumId;
	double sigmaExtremum = 0.0;

	double quarkMassExtremum = 0.0;
	double pionCurvMass2Extremum = 0.0;
	double sigmaCurvMass2Extremum = 0.0;

	double grandPotentialExtremum = 0.0;

	double dTGrandPotentialExtremum = 0.0;
	double dCpuGrandPotentialExtremum = 0.0;
	double dCpdGrandPotentialExtremum = 0.0;

public:
	SU2QuarkMesonModelActionExtremum(){};
	
	void setTemperature(double temperatureAux){ temperature = temperatureAux; }
	void setChemicalPotentialUpQuark(double chemicalPotentialUpQuarkAux){ chemicalPotentialUpQuark = chemicalPotentialUpQuarkAux; }
	void setChemicalPotentialDownQuark(double chemicalPotentialDownQuarkAux){ chemicalPotentialDownQuark = chemicalPotentialDownQuarkAux; }
	void setNumberOfExtrema(int numberOfExtremaAux){ numberOfExtrema = numberOfExtremaAux; }
	void setBranchIdentifier(branchIdentifier extremumIdAux){ extremumId = extremumIdAux; }
	void setSigmaExtremum(double sigmaExtremumAux){ sigmaExtremum = sigmaExtremumAux; }
	void setQuarkMassExtremum(double quarkMassExtremumAux){ quarkMassExtremum = quarkMassExtremumAux; }
	void setPionCurvMass2Extremum(double pionCurvMass2ExtremumAux){ pionCurvMass2Extremum = pionCurvMass2ExtremumAux; }
	void setSigmaCurvMass2Extremum(double sigmaCurvMass2ExtremumAux){ sigmaCurvMass2Extremum = sigmaCurvMass2ExtremumAux; }
	void setGrandPotentialExtremum(double grandPotentialExtremumAux){ grandPotentialExtremum = grandPotentialExtremumAux; }
	void setDTGrandPotentialExtremum(double dTGrandPotentialExtremumAux){ dTGrandPotentialExtremum = dTGrandPotentialExtremumAux; }
	void setDCpuGrandPotentialExtremum(double dCpuGrandPotentialExtremumAux){ dCpuGrandPotentialExtremum = dCpuGrandPotentialExtremumAux; }
	void setDCpdGrandPotentialExtremum(double dCpdGrandPotentialExtremumAux){ dCpdGrandPotentialExtremum = dCpdGrandPotentialExtremumAux; }
	
	double getTemperature(){ return temperature; }
	double getChemicalPotentialUpQuark(){ return chemicalPotentialUpQuark; }
	double getChemicalPotentialDownQuark(){ return chemicalPotentialDownQuark; }
	int getNumberOfExtrema(){ return numberOfExtrema; }
	branchIdentifier getBranchIdentifier(){ return extremumId; }
	double getSigmaExtremum(){ return sigmaExtremum; }
	double getQuarkMassExtremum(){ return quarkMassExtremum; }
	double getPionCurvMass2Extremum(){ return pionCurvMass2Extremum; }
	double getSigmaCurvMass2Extremum(){ return sigmaCurvMass2Extremum; }
	double getGrandPotentialExtremum(){ return grandPotentialExtremum; }
	double getDTGrandPotentialExtremum(){ return dTGrandPotentialExtremum; }
	double getDCpuGrandPotentialExtremum(){ return dCpuGrandPotentialExtremum; }
	double getDCpdGrandPotentialExtremum(){ return dCpdGrandPotentialExtremum; }
};


class SU2QuarkMesonModelInterpolation
{
public:
	enum InterpolationMethod 
	{ 
		hybrid1, 
		linearGSL, 
		steffenGSL, 
		cubicGSL, 
		akimaGSL, 
		polynomialGSL, 
		InterpolationMethodCount // Used as the boundary, add new methods above this value! 
	};

private:
	InterpolationMethod method;
	RootFindingMethod methodRootFindingGSL = brent;
	double precisionRootFindingGSL = 1E-8;

public:
	SU2QuarkMesonModelInterpolation(){};
	SU2QuarkMesonModelInterpolation(InterpolationMethod methodAux)
	{
		method = methodAux;
	}
	SU2QuarkMesonModelInterpolation(InterpolationMethod methodAux, RootFindingMethod methodRootFindingGSLAux, double precisionRootFindingGSLAux)
	{
		method = methodAux;
		methodRootFindingGSL = methodRootFindingGSLAux;
		precisionRootFindingGSL = precisionRootFindingGSLAux;
	}
	
	InterpolationMethod getMethod(){ return method; }
	double getPrecisionRootFindingGSL(){ return precisionRootFindingGSL; }
	RootFindingMethod getMethodRootFindingGSL(){ return methodRootFindingGSL; }
	InterpolationGSL1DimMethod convertMethod();
	bool usingGSLMethod();
	static std::string toStringInterpolationMethod(InterpolationMethod methodAux);
	std::string getInterpolationMethodName(){ return toStringInterpolationMethod(method); }
	static int getNumberOfInterpolationMethods(){ return InterpolationMethodCount; }
	static InterpolationMethod getInterpolationMethod(int index){ return static_cast<InterpolationMethod>(index); }
	static InterpolationMethod stringToInterpolationMethod(const std::string& );
};


class SU2QuarkMesonModelGrandPotential
{
private:
	std::vector<SU2QuarkMesonModelActionExtremum> grandPotential;

public:
	SU2QuarkMesonModelGrandPotential(){};
	SU2QuarkMesonModelGrandPotential(SU2QuarkMesonModelEffectiveAction , SU2QuarkMesonModelInterpolation );
	SU2QuarkMesonModelGrandPotential(std::vector<SU2QuarkMesonModelActionExtremum> grandPotentialAux){ grandPotential = grandPotentialAux; }
	SU2QuarkMesonModelGrandPotential(SU2QuarkMesonModelParameters , OneDimensionalGrid , QuarkMesonFlowParameters , SU2QuarkMesonModelInterpolation , int , double , std::vector<double> , std::vector<double> );
	SU2QuarkMesonModelGrandPotential(std::string );
	SU2QuarkMesonModelGrandPotential(SU2QuarkMesonModelParameters , OneDimensionalGrid , QuarkMesonFlowParameters , SU2QuarkMesonModelInterpolation , int , std::vector<double> , double , double );

	std::vector<SU2QuarkMesonModelActionExtremum> getExtrema(){ return grandPotential; }
	SU2QuarkMesonModelActionExtremum getExtremum(int i){ return grandPotential[i]; }
	int getLength(){ return int(grandPotential.size()); }
	
	double getTemperature(int i){ return grandPotential[i].getTemperature(); }
	double getChemicalPotentialUpQuark(int i){ return grandPotential[i].getChemicalPotentialUpQuark(); }
	double getChemicalPotentialDownQuark(int i){ return grandPotential[i].getChemicalPotentialDownQuark(); }
	int getNumberOfExtrema(int i){ return grandPotential[i].getNumberOfExtrema(); }
	branchIdentifier getBranchIdentifier(int i){ return grandPotential[i].getBranchIdentifier(); }
	double getSigmaExtremum(int i){ return grandPotential[i].getSigmaExtremum(); }
	double getQuarkMassExtremum(int i){ return grandPotential[i].getQuarkMassExtremum(); }
	double getPionCurvMass2Extremum(int i){ return grandPotential[i].getPionCurvMass2Extremum(); }
	double getSigmaCurvMass2Extremum(int i){ return grandPotential[i].getSigmaCurvMass2Extremum(); }
	double getGrandPotentialExtremum(int i){ return grandPotential[i].getGrandPotentialExtremum(); }
	double getDTGrandPotentialExtremum(int i){ return grandPotential[i].getDTGrandPotentialExtremum(); }
	double getDCpuGrandPotentialExtremum(int i){ return grandPotential[i].getDCpuGrandPotentialExtremum(); }
	double getDCpdGrandPotentialExtremum(int i){ return grandPotential[i].getDCpdGrandPotentialExtremum(); }

	void addExtrema(std::vector<SU2QuarkMesonModelActionExtremum> );
	
	void sortBySigma();
	void sortBySigmaAndUpQuarkChemicalPotentialInTheHighBranch();
	void sortBySigmaAndTemperatureInTheHighBranch();

	void logGrandPotentialToFile(std::string );
};

std::vector<SU2QuarkMesonModelActionExtremum> evaluateExtremaInterpolationHybrid1(SU2QuarkMesonModelEffectiveAction );

std::vector<SU2QuarkMesonModelActionExtremum> evaluateExtremaInterpolationGSL(SU2QuarkMesonModelEffectiveAction , InterpolationGSL1DimMethod , RootFindingMethod , double );


#endif