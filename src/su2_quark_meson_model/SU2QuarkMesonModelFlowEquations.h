#ifndef SU2QUARKMESONMODELFLOWEQUATIONS_H
#define SU2QUARKMESONMODELFLOWEQUATIONS_H


double fermiDistribution(double , double );

double boseDistribution(double , double );


double su2QuarkMesonModelUVPotential(double , double , double );

double su2QuarkMesonModelEnergySigma(double , double );

double su2QuarkMesonModelEnergyPion(double , double , double );

double su2QuarkMesonModelEnergyQuark(double , double , double );

double su2QuarkMesonModelOptimalStepYokotaFVacuum(double , double , double );

double su2QuarkMesonModelOptimalStepYokotaF(double , double , double , double );

double su2QuarkMesonModelOptimalStepYokotaGVacuum(double , double );

double su2QuarkMesonModelOptimalStepYokotaG(double , double , double );

double su2QuarkMesonModelRHSFlowEqPotentialVacuum(double , double , double , double );

double su2QuarkMesonModelRHSFlowEqPotential(double , double , double , double , double , double , double );

double su2QuarkMesonModelRHSFlowEqDTPotential(double , double , double , double , double , double , double , double , double , double );

double su2QuarkMesonModelRHSFlowEqDCpfPotential(double , double , double , double , double , double , double , double , double );


#endif