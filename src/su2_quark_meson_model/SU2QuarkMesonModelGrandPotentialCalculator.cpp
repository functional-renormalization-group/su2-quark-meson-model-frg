#include <cmath>
#include <algorithm>
#include "SU2QuarkMesonModelGrandPotential.h"
#include "SU2QuarkMesonModelGrandPotentialCalculator.h"
#include "LinearInterpolation.h"
#include "QuadraticInterpolationGivenExtremum.h"


using namespace std;


void SU2QuarkMesonModelGrandPotentialCalculator::evalGrandPotentialFixedTempListOfQuarkChemPot(SU2QuarkMesonModelParameters modelParams, OneDimensionalGrid sigmaGrid, QuarkMesonFlowParameters flowParams, SU2QuarkMesonModelInterpolation interpolation, int numberThreads, double temperature, vector<double> &cpQuark)
{	
	sort(cpQuark.begin(), cpQuark.end());
	SU2QuarkMesonModelGrandPotential grandPotFixedTemp(modelParams, sigmaGrid, flowParams, interpolation, numberThreads, temperature, cpQuark, cpQuark);

	//print results in the screen
	for (int i = 0; i < grandPotFixedTemp.getLength(); i++)
	{	
		printf("%.8f\t", grandPotFixedTemp.getTemperature(i));
		printf("%.8f\t", grandPotFixedTemp.getChemicalPotentialUpQuark(i));
		printf("%.8f\t", grandPotFixedTemp.getChemicalPotentialDownQuark(i));
		printf("%i\t", grandPotFixedTemp.getNumberOfExtrema(i));
		printf("%i\t", grandPotFixedTemp.getBranchIdentifier(i));
		printf("%.8f\t", grandPotFixedTemp.getSigmaExtremum(i));
		
		printf("\n");
	}

	//save results to file
	double cpMin = cpQuark[0];
	double cpMax = cpQuark[int(cpQuark.size())-1];
	string fileName = "T"+to_string(temperature)+"_CpMin"+to_string(cpMin)+"_CpMax"+to_string(cpMax)+"_"+interpolation.getInterpolationMethodName();
	grandPotFixedTemp.logGrandPotentialToFile(fileName);
}


void SU2QuarkMesonModelGrandPotentialCalculator::evalGrandPotentialFixedTempRangeOfQuarkChemPot(SU2QuarkMesonModelParameters modelParams, OneDimensionalGrid sigmaGrid, QuarkMesonFlowParameters flowParams, SU2QuarkMesonModelInterpolation interpolation, int numberThreads, double temperature, double cpQuarkMin, double cpQuarkMax, int N)
{	
	if ( fabs(cpQuarkMax-cpQuarkMin)>0 && N==1 )
	{
		cout << "Warning: The maximum and minimum chemical potentials are different, but N==1! Only the minimum value will be calculated!\n";
	}

	double deltaCpQuark = 0;
	if ( N>1 ){ deltaCpQuark = (cpQuarkMax-cpQuarkMin)/( N-1 ); }
	
	vector<double> cpQuark;
	for (int i = 0; i < N; i++)
	{
		cpQuark.push_back( cpQuarkMin + i*deltaCpQuark );
	}

	SU2QuarkMesonModelGrandPotentialCalculator::evalGrandPotentialFixedTempListOfQuarkChemPot(modelParams, sigmaGrid, flowParams, interpolation, numberThreads, temperature, cpQuark);
}


void SU2QuarkMesonModelGrandPotentialCalculator::evalGrandPotentialFixedQuarkChemPotListOfTemp(SU2QuarkMesonModelParameters modelParams, OneDimensionalGrid sigmaGrid, QuarkMesonFlowParameters flowParams, SU2QuarkMesonModelInterpolation interpolation, int numberThreads, vector<double> &temperature, double cpQuark)
{	
	sort(temperature.begin(), temperature.end());
	SU2QuarkMesonModelGrandPotential grandPotFixedTemp(modelParams, sigmaGrid, flowParams, interpolation, numberThreads, temperature, cpQuark, cpQuark);

	//print results in the screen
	for (int i = 0; i < grandPotFixedTemp.getLength(); i++)
	{	
		printf("%.8f\t", grandPotFixedTemp.getTemperature(i));
		printf("%.8f\t", grandPotFixedTemp.getChemicalPotentialUpQuark(i));
		printf("%.8f\t", grandPotFixedTemp.getChemicalPotentialDownQuark(i));
		printf("%i\t", grandPotFixedTemp.getNumberOfExtrema(i));
		printf("%i\t", grandPotFixedTemp.getBranchIdentifier(i));
		printf("%.8f\t", grandPotFixedTemp.getSigmaExtremum(i));
		
		printf("\n");
	}

	//save results to file
	double tempMin = temperature[0];
	double tempMax = temperature[int(temperature.size())-1];
	string fileName = "TempMin"+to_string(tempMin)+"_TempMax"+to_string(tempMax)+"_Cp"+to_string(cpQuark);
	grandPotFixedTemp.logGrandPotentialToFile(fileName);
}


void SU2QuarkMesonModelGrandPotentialCalculator::evalGrandPotentialFixedQuarkChemPotpRangeOfTemp(SU2QuarkMesonModelParameters modelParams, OneDimensionalGrid sigmaGrid, QuarkMesonFlowParameters flowParams, SU2QuarkMesonModelInterpolation interpolation, int numberThreads, double temperatureMin, double temperatureMax, double cpQuark, int N)
{	
	vector<double> temperature;

	double deltaTemperature = (temperatureMax-temperatureMin)/( N-1 );
	for (int i = 0; i < N; i++)
	{
		temperature.push_back( temperatureMin + i*deltaTemperature );
	}

	SU2QuarkMesonModelGrandPotentialCalculator::evalGrandPotentialFixedQuarkChemPotListOfTemp(modelParams, sigmaGrid, flowParams, interpolation, numberThreads, temperature, cpQuark);
}


void SU2QuarkMesonModelGrandPotentialCalculator::evalGrandPotentialFixedTempRangeOfQuarkChemPot(SU2QuarkMesonModelParameters modelParams, OneDimensionalGrid sigmaGrid, QuarkMesonFlowParameters flowParams, vector<SU2QuarkMesonModelInterpolation> interpolation, int numberThreads, double temperature, double cpQuarkMin, double cpQuarkMax, int N)
{	
	if ( fabs(cpQuarkMax-cpQuarkMin)>0 && N==1 )
	{
		cout << "Warning: The maximum and minimum chemical potentials are different, but N==1! Only the minimum value will be calculated!\n";
	}

	double deltaCpQuark = 0;
	if ( N>1 ){ deltaCpQuark = (cpQuarkMax-cpQuarkMin)/( N-1 ); }
	
	vector<double> cpQuark;
	for (int i = 0; i < N; i++)
	{
		cpQuark.push_back( cpQuarkMin + i*deltaCpQuark );
	}

	vector <SU2QuarkMesonModelEffectiveAction> actionList(int(cpQuark.size()));
	#pragma omp parallel num_threads(numberThreads)
	{
		#pragma omp for nowait
		for (int i = 0; i < int(cpQuark.size()); i++)
		{
			//quark-meson model parameters
			double Lambda = modelParams.getModelCutoff();
			double gS = modelParams.getYukawaCoupling();
			double m = modelParams.getC2UVPotential();
			double lambda = modelParams.getC4UVPotential();
			double c = modelParams.getC1SymmetryBreaking();
			SU2QuarkMesonModelParameters modelParamsAux(Lambda, gS, m, lambda, c, temperature, cpQuark[i], cpQuark[i]);	

			actionList[i] = SU2QuarkMesonModelEffectiveAction(modelParamsAux, sigmaGrid, flowParams);
			actionList[i].solveFlowEqsLPA();
		}
	}
	
	for (int j = 0; j < int(interpolation.size()); j++)
	{
		SU2QuarkMesonModelGrandPotential grandPotFixedTempPrivate;
		for (int i = 0; i < int(cpQuark.size()); i++)
		{
			SU2QuarkMesonModelGrandPotential grandPot(actionList[i], interpolation[j]);
			grandPotFixedTempPrivate.addExtrema( grandPot.getExtrema() );
		}

		//sort the calculations
		grandPotFixedTempPrivate.sortBySigmaAndUpQuarkChemicalPotentialInTheHighBranch();


		//save results to file
		double cpMin = cpQuark[0];
		double cpMax = cpQuark[int(cpQuark.size())-1];
		string fileName = "T"+to_string(temperature)+"_CpMin"+to_string(cpMin)+"_CpMax"+to_string(cpMax)+"_"+interpolation[j].getInterpolationMethodName();
		grandPotFixedTempPrivate.logGrandPotentialToFile(fileName);


		//print results in the screen
		for (int i = 0; i < grandPotFixedTempPrivate.getLength(); i++)
		{	
			printf("%.8f\t", grandPotFixedTempPrivate.getTemperature(i));
			printf("%.8f\t", grandPotFixedTempPrivate.getChemicalPotentialUpQuark(i));
			printf("%.8f\t", grandPotFixedTempPrivate.getChemicalPotentialDownQuark(i));
			printf("%i\t", grandPotFixedTempPrivate.getNumberOfExtrema(i));
			printf("%i\t", grandPotFixedTempPrivate.getBranchIdentifier(i));
			printf("%.8f\t", grandPotFixedTempPrivate.getSigmaExtremum(i));
			printf("\n");
		}
	}
}


void SU2QuarkMesonModelGrandPotentialCalculator::evalGrandPotentialFixedTempRangeOfQuarkChemPot(SU2QuarkMesonModelParameters modelParams, OneDimensionalGrid sigmaGrid, QuarkMesonFlowParameters flowParams, int numberThreads, double temperature, double cpQuarkMin, double cpQuarkMax, int N)
{	
	//create vector containing all the interpolation methods defined in the SU2QuarkMesonModelInterpolation class
	int numberOfInterpolations = SU2QuarkMesonModelInterpolation::getNumberOfInterpolationMethods();
	vector<SU2QuarkMesonModelInterpolation> interpolation(numberOfInterpolations);
	for (int i = 0; i < numberOfInterpolations; i++)
	{	
		interpolation[i] = SU2QuarkMesonModelInterpolation( SU2QuarkMesonModelInterpolation::getInterpolationMethod(i) );
		//cout << SU2QuarkMesonModelInterpolation::getInterpolationMethodName(SU2QuarkMesonModelInterpolation::getInterpolationMethod(i)) << "\n";
	}
	
	SU2QuarkMesonModelGrandPotentialCalculator::evalGrandPotentialFixedTempRangeOfQuarkChemPot(modelParams, sigmaGrid, flowParams, interpolation, numberThreads, temperature, cpQuarkMin, cpQuarkMax, N);
}


void SU2QuarkMesonModelGrandPotentialCalculator::evalGrandPotentialFixedTemp(const IniFileParser& config)
{
	// Model parameters
    double Lambda = config.getDouble("ModelParameters", "Lambda");
    double gS = config.getDouble("ModelParameters", "gS");
    double m = config.getDouble("ModelParameters", "m") * Lambda;
    double lambda = config.getDouble("ModelParameters", "lambda");
    double c = config.getDouble("ModelParameters", "c") * pow(Lambda, 3);
    SU2QuarkMesonModelParameters qmParams(Lambda, gS, m, lambda, c, 0.0, 0.0, 0.0);

    cout << "\nModelParameters:" << endl;
    cout << "Lambda = " << Lambda << endl;
    cout << "gS = " << gS << endl;
    cout << "m = " << m << endl;
    cout << "lambda = " << lambda << endl;
    cout << "c = " << c << endl;


    // Grid parameters
    double sigmaMin = config.getDouble("GridParameters", "sigmaMin");
    double sigmaMax = config.getDouble("GridParameters", "sigmaMax");
    int numberOfGridPoints = config.getInt("GridParameters", "numberOfGridPoints");
	string derivativeMethodOnGrid = config.getValue("GridParameters", "derivativeMethodOnGrid");
	OneDimensionalGrid gridParams(sigmaMin, sigmaMax, numberOfGridPoints, stringToDifferentiationMethod1D(derivativeMethodOnGrid));

    cout << "\nGrid Parameters:" << endl;
    cout << "sigmaMin = " << sigmaMin << endl;
    cout << "sigmaMax = " << sigmaMax << endl;
    cout << "numberOfGridPoints = " << numberOfGridPoints << endl;
	cout << "derivativeMethodOnGrid = " << derivativeMethodOnGrid << endl;


	// Flow parameters
    double kUV = config.getDouble("FlowParameters", "kUV");
    double kIR = config.getDouble("FlowParameters", "kIR");
	string quantitiesToFlow = config.getValue("FlowParameters", "quantitiesToFlow");
	QuarkMesonFlowParameters flowParams(kUV, kIR, stringToQuantitiesToFlow(quantitiesToFlow));

    cout << "\nFlow Parameters:" << endl;
    cout << "kUV = " << kUV << endl;
    cout << "kIR = " << kIR << endl;
	cout << "quantitiesToFlow = " << toStringQuantitiesToFlow( stringToQuantitiesToFlow(quantitiesToFlow) ) << endl;


	// Interpolation parameters
    string interpolationMethod = config.getValue("InterpolationParameters", "type");

	cout << "\nInterpolationParameters:" << endl;
    cout << "type = " << interpolationMethod << endl;
	SU2QuarkMesonModelInterpolation interpolation(SU2QuarkMesonModelInterpolation::stringToInterpolationMethod(interpolationMethod));


	// Parallel Computing parameters
	int numberOfCPUs = config.getInt("ParallelComputingParameters", "numberOfCPUs");

	cout << "\nParallelComputingParameters:" << endl;
	cout << "numberOfCPUs = " << numberOfCPUs << endl;


	// Temperature and chemical potentials
    vector<map<string, string>> thermodynamicData = config.getSectionsData("ThermodynamicParameters");

    cout << "\nThermodynamicParameters:" << endl;
    for (int i = 0; i < int(thermodynamicData.size()); ++i) 
	{
		const map<string, string>& section = thermodynamicData[i];

        double temperature = config.getDouble(section, "temperature");
        double cpMin = config.getDouble(section, "cpMin");
        double cpMax = config.getDouble(section, "cpMax");
        int numberCp = config.getInt(section, "numberCp");

        // Print temperature and chemical potential parameters
		cout << "\n################################" << endl;
        cout << "Set " << i + 1 << ":" << endl;
        cout << "Temperature = " << temperature << endl;
        cout << "cpMin = " << cpMin << endl;
        cout << "cpMax = " << cpMax << endl;
        cout << "numberCp = " << numberCp << endl;
		cout << endl;

        // Call the function that performs the calculations
		SU2QuarkMesonModelGrandPotentialCalculator::evalGrandPotentialFixedTempRangeOfQuarkChemPot(qmParams, gridParams, flowParams, interpolation, numberOfCPUs, temperature, cpMin, cpMax, numberCp);
    }
}