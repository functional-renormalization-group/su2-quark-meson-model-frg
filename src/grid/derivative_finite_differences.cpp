#include <cmath>
#include "derivative_finite_differences.h"

using namespace std;


//Several approximations of the first and second derivative of a function using finite differences.


//Calculate the first derivative of the function f with finite differences (3 point rule)
vector<double> calculate1stDerivative3PointRule(const double dx, const int N, vector<double> &f)
{   
    vector<double> dfdx(N,0);

    //Calculate the first derivative of the points f[2] to f[N-3] with the 3 point rule
    for (int i = 1; i <= N-2; ++i)
    {
        dfdx[i] = ( - f[i-1] + f[i+1] )/( 2.0*dx );
    }

    //Calculate the derivatives at the edges
    int j;

    //Calculate the first derivative at the points f[0] with the 3 point rule 
    j = 0;
    dfdx[j] = ( - 3.0*f[j] + 4.0*f[j+1] - f[j+2] )/( 2.0*dx );

    //Calculate the first derivative at the points f[N-1] with the 3 point rule 
    j = N-1;
    dfdx[j] = ( 3.0*f[j] - 4.0*f[j-1] + f[j-2] )/( 2.0*dx );

    return dfdx;
}


//Calculate the second derivative of the function f with finite differences (3 point rule)
vector<double> calculate2ndDerivative3PointRule(const double dx, const int N, vector<double> &f)
{	
	vector<double> d2fdx2(N,0);

    //Calculate the second derivative of the points f[2] to f[N-3] with the 3 point rule
    for (int i = 1; i <= N-2; ++i)
    {
        d2fdx2[i] = ( f[i-1] - 2.0*f[i] + f[i+1] )/( pow(dx,2) );
    }
    
    //Calculate the derivatives at the edges
    int j;

    //Calculate the second derivative at the points f[0] with the 3 point rule 
    j = 0;
    d2fdx2[j] = ( f[j] - 2.0*f[j+1] + f[j+2] )/( pow(dx,2) );
    
    //Calculate the second derivative at the points f[N-1] with the 3 point rule 
    j = N-1;
    d2fdx2[j] = ( f[j] - 2.0*f[j-1] + f[j-2] )/( pow(dx,2) );

    return d2fdx2;
}


//Calculate the first derivative of the function f with finite differences (5 point rule)
vector<double> calculate1stDerivative5PointRule(const double dx, const int N, vector<double> &f)
{	
	vector<double> dfdx(N,0);

    //Calculate the first derivative of the points f[2] to f[N-3] with the 5 point rule
    for (int i = 2; i <= N-3; ++i)
    {
        dfdx[i] = ( f[i-2] - 8.0*f[i-1] + 8.0*f[i+1] - f[i+2] )/( 12.0*dx );
    }

    //Calculate the derivatives at the edges
    int j;

    //Calculate the first derivative at the points f[0] to f[1] with the 5 point rule 
    j = 1;
    dfdx[j] = ( - 3.0*f[j-1] - 10.0*f[j] + 18.0*f[j+1] - 6.0*f[j+2] + f[j+3] )/( 12.0*dx );
    j = 0;
    dfdx[j] = ( - 25.0*f[j] + 48.0*f[j+1] - 36.0*f[j+2] + 16.0*f[j+3] - 3.0*f[j+4] )/( 12.0*dx );

    //Calculate the first derivative at the points f[N-2] to f[N-1] with the 5 point rule 
    j = N - 2;
    dfdx[j] = ( 3.0*f[j+1] + 10.0*f[j] - 18.0*f[j-1] + 6.0*f[j-2] - f[j-3] )/( 12.0*dx );
    j = N - 1;
    dfdx[j] = ( 25.0*f[j] - 48.0*f[j-1] + 36.0*f[j-2] - 16.0*f[j-3] + 3.0*f[j-4] )/( 12.0*dx );

    return dfdx;
}


//Calculate the second derivative of the function f with finite differences (5 point rule)
vector<double> calculate2ndDerivative5PointRule(const double dx, const int N, vector<double> &f)
{	
	vector<double> d2fdx2(N,0);

    //Calculate the second derivative of the points f[2] to f[N-3] with the 5 point rule
    for (int i = 2; i <= N-3; ++i)
    {
        d2fdx2[i] = ( - f[i-2] + 16.0*f[i-1] - 30.0*f[i] + 16.0*f[i+1] - f[i+2] )/( 12.0*pow(dx,2) );
    }

    //Calculate the derivatives at the edges
    int j;

    //Calculate the second derivative at the points f[0] to f[1] with the 5 point rule 
    j = 1;
    d2fdx2[j] = ( 11.0*f[j-1] - 20.0*f[j] + 6.0*f[j+1] + 4.0*f[j+2] - f[j+3] )/( 12.0*pow(dx,2) );
    j = 0;
    d2fdx2[j] = ( 35.0*f[j] - 104.0*f[j+1] + 114.0*f[j+2] - 56.0*f[j+3] + 11.0*f[j+4] )/( 12.0*pow(dx,2) );

    //Calculate the second derivative at the points f[N-2] to f[N-1] with the 5 point rule 
    j = N-2;
    d2fdx2[j] = ( 11.0*f[j+1] - 20.0*f[j] + 6.0*f[j-1] + 4.0*f[j-2] - f[j-3] )/( 12.0*pow(dx,2) );
    j = N-1;
    d2fdx2[j] = ( 35.0*f[j] - 104.0*f[j-1] + 114.0*f[j-2] - 56.0*f[j-3] + 11.0*f[j-4] )/( 12.0*pow(dx,2) );

	return d2fdx2;
}


//Calculate the first derivative of the function f with finite differences (7 point rule)
vector<double> calculate1stDerivative7PointRule(const double dx, const int N, vector<double> &f)
{   
    vector<double> dfdx(N,0);

    //Calculate the first derivative of the points f[3] to f[N_dx-4] with the 7 point rule
    for (int i = 3; i <= N-4; ++i)
    {
        dfdx[i] = (  - f[i-3] + 9.0*f[i-2] - 45.0*f[i-1] + 45.0*f[i+1] - 9.0*f[i+2] + f[i+3] )/( 60.0*dx );
    }

    //Calculate the derivatives at the edges
    int j;

    //Calculate the first derivative at the points f[0] to f[2] with the 7 point rule 
    j = 2;
    dfdx[j] = ( 2.0*f[j-2] - 24.0*f[j-1] - 35.0*f[j] + 80.0*f[j+1] - 30.0*f[j+2] + 8.0*f[j+3] - f[j+4] )/( 60.0*dx );
    j = 1;
    dfdx[j] = ( -10.0*f[j-1] - 77.0*f[j] + 150.0*f[j+1] - 100.0*f[j+2] + 50.0*f[j+3] - 15.0*f[j+4] + 2.0*f[j+5] )/( 60.0*dx );
    j = 0;
    dfdx[j] = ( - 147.0*f[j] + 360.0*f[j+1] - 450.0*f[j+2] + 400.0*f[j+3] - 225.0*f[j+4] + 72.0*f[j+5] - 10.0*f[j+6] )/( 60.0*dx );

    //Calculate the first derivative at the points f[N_dx-3] to f[N_dx-1] with the 7 point rule 
    j = N-3;
    dfdx[j] = ( -2.0*f[j+2] + 24.0*f[j+1] + 35.0*f[j] - 80.0*f[j-1] + 30.0*f[j-2] - 8.0*f[j-3] + f[j-4] )/( 60.0*dx );
    j = N-2;
    dfdx[j] =  ( 10.0*f[j+1] + 77.0*f[j] - 150.0*f[j-1] + 100.0*f[j-2] - 50.0*f[j-3] + 15.0*f[j-4] - 2.0*f[j-5] )/( 60.0*dx );
    j = N-1;
    dfdx[j] = ( 147.0*f[j] - 360.0*f[j-1] + 450.0*f[j-2] - 400.0*f[j-3] + 225.0*f[j-4] - 72.0*f[j-5] + 10.0*f[j-6] )/( 60.0*dx );

    return dfdx;
}


//Calculate the second derivative of the function f with finite differences (7 point rule)
vector<double> calculate2ndDerivative7PointRule(const double dx, const int N, vector<double> &f)
{   
    vector<double> d2fdx2(N,0);

    //Calculate the second derivative of the points f[3] to f[N_dx-4] with the 7 point rule
    for (int i = 3; i <= N-4; ++i)
    {
        d2fdx2[i] = ( 2.0*f[i-3] - 27.0*f[i-2] + 270.0*f[i-1] - 490.0*f[i] + 270.0*f[i+1] - 27.0*f[i+2] + 2.0*f[i+3] )/( 180.0*pow(dx,2) );
    }

    //DERIVATIVE ON THE EDGES   
    int j;

    //Calculate the second derivative of the points f[0] to f[2] with the 7 point rule 
    j = 2;
    d2fdx2[j] = ( - 11.0*f[j-2] + 214.0*f[j-1] - 378.0*f[j] + 130.0*f[j+1] + 85.0*f[j+2] - 54.0*f[j+3] + 16.0*f[j+4] - 2.0*f[j+5] )/( 180.0*pow(dx,2) );
    j = 1;
    d2fdx2[j] = ( 126.0*f[j-1] - 70.0*f[j] - 486.0*f[j+1] + 855.0*f[j+2] - 670.0*f[j+3] + 324.0*f[j+4] - 90.0*f[j+5] + 11.0*f[j+6] )/( 180.0*pow(dx,2) );
    j = 0;
    d2fdx2[j] = ( 938.0*f[j] - 4014.0*f[j+1] + 7911.0*f[j+2] - 9490.0*f[j+3] + 7380.0*f[j+4] - 3618.0*f[j+5] + 1019.0*f[j+6] - 126.0*f[j+7]  )/( 180.0*pow(dx,2) );

    //Calculate the second derivative of the points f[N_dx-3] to f[N_dx-1] with the 7 point rule 
    j = N-3;
    d2fdx2[j] = ( - 11.0*f[j+2] + 214.0*f[j+1] - 378.0*f[j] + 130.0*f[j-1] + 85.0*f[j-2] - 54.0*f[j-3] + 16.0*f[j-4] - 2.0*f[j-5] )/( 180.0*pow(dx,2) );
    j = N-2;
    d2fdx2[j] = ( 126.0*f[j+1] - 70.0*f[j] - 486.0*f[j-1] + 855.0*f[j-2] - 670.0*f[j-3] + 324.0*f[j-4] - 90.0*f[j-5] + 11.0*f[j-6] )/( 180.0*pow(dx,2) );
    j = N-1;
    d2fdx2[j] = ( 938.0*f[j] - 4014.0*f[j-1] + 7911.0*f[j-2] - 9490.0*f[j-3] + 7380.0*f[j-4] - 3618.0*f[j-5] + 1019.0*f[j-6] - 126.0*f[j-7]  )/( 180.0*pow(dx,2) );

    return d2fdx2;
}


//Calculate the first derivative of the function f with finite differences (9 point rule)
vector<double> calculate1stDerivative9PointRule(const double dx, const int N, vector<double> &f)
{   
    vector<double> dfdx(N,0);

    //Calculate the first derivative of the points f[4] to f[N_dx-5] with the 9 point rule
    for (int i = 4; i <= N-5; ++i)
    {
        dfdx[i] = ( 3.0*f[i-4] - 32.0*f[i-3] + 168.0*f[i-2] - 672.0*f[i-1] + 672.0*f[i+1] - 168.0*f[i+2] + 32.0*f[i+3] - 3.0*f[i+4])/( 840.0*dx );
    }

    //Calculate the derivatives at the edges
    int j;

    //Calculate the first derivative of the points f[0] to f[3] with the 9 point rule
    j = 3;
    dfdx[j] = ( - 5.0*f[j-3] + 60.0*f[j-2] - 420.0*f[j-1] - 378.0*f[j] + 1050.0*f[j+1] - 420.0*f[j+2] + 140.0*f[j+3] - 30.0*f[j+4] + 3.0*f[j+5] )/( 840.0*dx );
    j = 2;
    dfdx[j] = ( 15.0*f[j-2] - 240.0*f[j-1] - 798.0*f[j] + 1680.0*f[j+1] - 1050.0*f[j+2] + 560.0*f[j+3] - 210.0*f[j+4] + 48.0*f[j+5] - 5.0*f[j+6] )/( 840.0*dx );
    j = 1;
    dfdx[j] = ( - 105.0*f[j-1] - 1338.0*f[j] + 2940.0*f[j+1] - 2940.0*f[j+2] + 2450.0*f[j+3] - 1470.0*f[j+4] + 588.0*f[j+5] - 140.0*f[j+6] + 15.0*f[j+7] )/( 840.0*dx );
    j = 0;
    dfdx[j] = ( - 2283.0*f[j] + 6720.0*f[j+1] - 11760.0*f[j+2] + 15680.0*f[j+3] - 14700.0*f[j+4] + 9408.0*f[j+5] - 3920.0*f[j+6] + 960.0*f[j+7] - 105.0*f[j+8] )/( 840.0*dx );

    //Calculate the first derivative of the points f[N_dx-4] to f[N_dx-1] with the 9 point rule
    j = N-4;
    dfdx[j] = ( 5.0*f[j+3] - 60.0*f[j+2] + 420.0*f[j+1] + 378.0*f[j] - 1050.0*f[j-1] + 420.0*f[j-2] - 140.0*f[j-3] + 30.0*f[j-4] - 3.0*f[j-5] )/( 840.0*dx );
    j = N-3;
    dfdx[j] = ( - 15.0*f[j+2] + 240.0*f[j+1] + 798.0*f[j] - 1680.0*f[j-1] + 1050.0*f[j-2] - 560.0*f[j-3] + 210.0*f[j-4] - 48.0*f[j-5] + 5.0*f[j-6] )/( 840.0*dx );
    j = N-2;
    dfdx[j] = ( 105.0*f[j+1] + 1338.0*f[j] - 2940.0*f[j-1] + 2940.0*f[j-2] - 2450.0*f[j-3] + 1470.0*f[j-4] - 588.0*f[j-5] + 140.0*f[j-6] - 15.0*f[j-7] )/( 840.0*dx );
    j = N-1;
    dfdx[j] = ( 2283.0*f[j] - 6720.0*f[j-1] + 11760.0*f[j-2] - 15680.0*f[j-3] + 14700.0*f[j-4] - 9408.0*f[j-5] + 3920.0*f[j-6] - 960.0*f[j-7] + 105.0*f[j-8] )/( 840.0*dx );

    return dfdx;
}


//Calculate the second derivative of the function f with finite differences (9 point rule)
vector<double> calculate2ndDerivative9PointRule(const double dx, const int N, vector<double> &f)
{   
    vector<double> d2fdx2(N,0);

    //Calculate the second derivative of the points f[4] to f[N_dx-5] with the 9 point rule
    for (int i = 4; i <= N-5; ++i)
    {
        d2fdx2[i] = ( - 9.0*f[i-4] + 128.0*f[i-3] - 1008.0*f[i-2] + 8064.0*f[i-1] - 14350.0*f[i] - 9.0*f[i+4] + 128.0*f[i+3] - 1008.0*f[i+2] + 8064.0*f[i+1] )/( 5040.0*pow(dx,2) );
    }

    //Calculate the derivatives at the edges
    int j;

    //Calculate the second derivative of the points f[0] to f[3] with the 9 point rule
    j = 3;
    d2fdx2[j] = ( 47.0*f[j-3] - 684.0*f[j-2] + 7308.0*f[j-1] - 13216.0*f[j] + 6930.0*f[j+1] - 252.0*f[j+2] - 196.0*f[j+3] + 72.0*f[j+4] - 9.0*f[j+5] )/( 5040.0*pow(dx,2) );
    j = 2;
    d2fdx2[j] = ( - 261.0*f[j-2] + 5616.0*f[j-1] - 9268.0*f[j] + 1008.0*f[j+1] + 5670.0*f[j+2] - 4144.0*f[j+3] + 1764.0*f[j+4] - 432.0*f[j+5] + 47.0*f[j+6] )/( 5040.0*pow(dx,2) );
    j = 1;
    d2fdx2[j] = ( 3267.0*f[j-1] + 128.0*f[j] - 20916.0*f[j+1] + 38556.0*f[j+2] - 37030.0*f[j+3] + 23688.0*f[j+4] - 9828.0*f[j+5] + 2396.0*f[j+6] - 261.0*f[j+7] )/( 5040.0*pow(dx,2) );
    j = 0;
    d2fdx2[j] = ( 29531.0*f[j] - 138528.0*f[j+1] + 312984.0*f[j+2] - 448672.0*f[j+3] + 435330.0*f[j+4] - 284256.0*f[j+5] + 120008.0*f[j+6] - 29664.0*f[j+7] + 3267.0*f[j+8] )/( 5040.0*pow(dx,2) );

    //Calculate the second derivative of the points f[N_dx-4] to f[N_dx-1] with the 9 point rule
    j = N-4;
    d2fdx2[j] = ( 47.0*f[j+3] - 684.0*f[j+2] + 7308.0*f[j+1] - 13216.0*f[j] + 6930.0*f[j-1] - 252.0*f[j-2] - 196.0*f[j-3] + 72.0*f[j-4] - 9.0*f[j-5] )/( 5040.0*pow(dx,2) );
    j = N-3;
    d2fdx2[j] = ( - 261.0*f[j+2] + 5616.0*f[j+1] - 9268.0*f[j] + 1008.0*f[j-1] + 5670.0*f[j-2] - 4144.0*f[j-3] + 1764.0*f[j-4] - 432.0*f[j-5] + 47.0*f[j-6] )/( 5040.0*pow(dx,2) );
    j = N-2;
    d2fdx2[j] = ( 3267.0*f[j+1] + 128.0*f[j] - 20916.0*f[j-1] + 38556.0*f[j-2] - 37030.0*f[j-3] + 23688.0*f[j-4] - 9828.0*f[j-5] + 2396.0*f[j-6] - 261.0*f[j-7] )/( 5040.0*pow(dx,2) );
    j = N-1;
    d2fdx2[j] = ( 29531.0*f[j] - 138528.0*f[j-1] + 312984.0*f[j-2] - 448672.0*f[j-3] + 435330.0*f[j-4] - 284256.0*f[j-5] + 120008.0*f[j-6] - 29664.0*f[j-7] + 3267.0*f[j-8] )/( 5040.0*pow(dx,2) );

    return d2fdx2;
}




