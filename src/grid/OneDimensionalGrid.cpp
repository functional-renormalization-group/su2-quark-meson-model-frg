#include <cmath>
#include <iostream>
#include "derivative_finite_differences.h"
#include "OneDimensionalGrid.h"

using namespace std;


string toStringDifferentiationMethod1D(DifferentiationMethod1D method) 
{
    switch (method) 
    {
        case NONE:
            return "NONE";
        case FiniteDifferences3PointRule:
            return "FiniteDifferences3PointRule";
        case FiniteDifferences5PointRule:
            return "FiniteDifferences5PointRule";
        case FiniteDifferences7PointRule:
            return "FiniteDifferences7PointRule";
        case FiniteDifferences9PointRule:
            return "FiniteDifferences9PointRule";
        default:
            return "Unknown";
    }
}


DifferentiationMethod1D stringToDifferentiationMethod1D(const string& method)
{
    if (method == "NONE")
    { 
        return NONE; 
    }
    else if (method == "FiniteDifferences3PointRule")
    {
        return FiniteDifferences3PointRule;
    }
    else if (method == "FiniteDifferences5PointRule")
    {
        return FiniteDifferences5PointRule;
    }
    else if (method == "FiniteDifferences7PointRule")
    {
        return FiniteDifferences7PointRule;
    }
    else if (method == "FiniteDifferences9PointRule")
    {
        return FiniteDifferences9PointRule;
    }
	else
	{ 
		cout << "Invalid input string to convert to 'DifferentiationMethod1D'! Providing: " + method + ". Returning 'NONE'!\n"; 
		return NONE; 
	}
}


void OneDimensionalGrid::setDerivativeMethod1D(DifferentiationMethod1D derivativeMethod1DAux)
{   
    const int N = getGridLength();

    bool warning = false;
    if ( derivativeMethod1DAux==FiniteDifferences3PointRule )
    {
        if ( N<3 ){ warning = true; }
        else{ derivativeMethod1D = derivativeMethod1DAux; }
    }
    else if ( derivativeMethod1DAux==FiniteDifferences5PointRule )
    {
        if ( N<5 ){ warning = true; }
        else{ derivativeMethod1D = derivativeMethod1DAux; }
    }
    else if ( derivativeMethod1DAux==FiniteDifferences7PointRule )
    {
        if ( N<7 ){ warning = true; }
        else{ derivativeMethod1D = derivativeMethod1DAux; }
    }
    else if ( derivativeMethod1DAux==FiniteDifferences9PointRule )
    {
        if ( N<9 ){ warning = true; }
        else{ derivativeMethod1D = derivativeMethod1DAux; }
    }
    else{ derivativeMethod1D = NONE; }

    if ( warning )
    {
        cout << "The grid created is to small to approximate a functions's derivative at each grid point!\n";
        derivativeMethod1D = NONE;
    }
}


OneDimensionalGrid::OneDimensionalGrid(double min, double max, int N)
{	
	gridMin = min;
	gridMax = max;
	gridLength = N;
	gridDelta = (gridMax - gridMin)/( gridLength - 1 );
    gridPoints.assign(N, 0.0);
	for (int i = 0; i < N; ++i){ gridPoints[i] = gridMin + i*gridDelta; }
    
    //creating a grid using this constructor does not allow to approximate derivatives of functions
    derivativeMethod1D = NONE;
}


OneDimensionalGrid::OneDimensionalGrid(double min, double max, int N, DifferentiationMethod1D derivativeMethod1DAux)
{	
	gridMin = min;
	gridMax = max;
	gridLength = N;
	gridDelta = (gridMax - gridMin)/( gridLength - 1 );
    gridPoints.assign(N, 0.0);
	
    for (int i = 0; i < N; ++i){ gridPoints[i] = gridMin + i*gridDelta; }
    
    setDerivativeMethod1D(derivativeMethod1DAux);
}


vector<double> OneDimensionalGrid::calculate1stDerivative(vector<double> f)
{	
	const int N = getGridLength();
	const double dx = getGridDelta();
	const DifferentiationMethod1D method = getDerivativeMethod1D();

	const int Nfunc = f.size();
	if ( Nfunc!=N )
	{
		cout << "Trying to calculate 1st derivative in a grid with a different overall size when compared with the size of the given vector!\n";
		abort();
	}

	vector<double> dfdx;

	if      ( method==FiniteDifferences5PointRule ){ dfdx = calculate1stDerivative5PointRule(dx, N, f); }
    else if ( method==FiniteDifferences3PointRule ){ dfdx = calculate1stDerivative3PointRule(dx, N, f); }
    else if ( method==FiniteDifferences7PointRule ){ dfdx = calculate1stDerivative7PointRule(dx, N, f); }
    else if ( method==FiniteDifferences9PointRule ){ dfdx = calculate1stDerivative9PointRule(dx, N, f); }
    else{ cout << "No method was provided to calculate 1st Derivative!\n"; abort(); }

    return dfdx;
}


vector<double> OneDimensionalGrid::calculate2ndDerivative(vector<double> f)
{	
	const int N = getGridLength();
	const double dx = getGridDelta();
	const DifferentiationMethod1D method = getDerivativeMethod1D();

	const int Nf = f.size();
	if ( Nf!=N )
	{
		cout << "Trying to calculate 2nd derivative in a grid with a different overall size when compared with the size of the given vector!\n";
		abort();
	}

	vector<double> d2fdx2;

	if      ( method==FiniteDifferences5PointRule ){ d2fdx2 = calculate2ndDerivative5PointRule(dx, N, f); }
    else if ( method==FiniteDifferences3PointRule ){ d2fdx2 = calculate2ndDerivative3PointRule(dx, N, f); }
    else if ( method==FiniteDifferences7PointRule ){ d2fdx2 = calculate2ndDerivative7PointRule(dx, N, f); }
    else if ( method==FiniteDifferences9PointRule ){ d2fdx2 = calculate2ndDerivative9PointRule(dx, N, f); }
    else{ cout << "No method was provided to calculate 2nd Derivative!\n"; abort(); }

    return d2fdx2;
}


void OneDimensionalGrid::test1stDerivative()
{   
    const int N = getGridLength();
    vector<double> func(N, 0.0);

    for (int i = 0; i < N; i++)
    {   
        double x = getGridPoint(i);
        func[i] = sin(x);
    }
    
    vector<double> dfuncdx = calculate1stDerivative(func);

    vector<double> absoluteError(N, 0.0);
    
    double maxAbsoluteError = fabs(dfuncdx[0] - cos(getGridPoint(0)));
    double minAbsoluteError = maxAbsoluteError;
    double averageAbsoluteError = 0.0;
    for (int i = 0; i < N; i++)
    {   
        double x = getGridPoint(i);
        absoluteError[i] = fabs(dfuncdx[i] - cos(x));
        averageAbsoluteError = averageAbsoluteError + absoluteError[i]/N;
        //cout << x << "\t" << func[i] << "\t" << dfuncdx[i] << "\t" << absoluteError[i] << "\n";
        if (absoluteError[i]>maxAbsoluteError){ maxAbsoluteError = absoluteError[i]; }
        if (absoluteError[i]<minAbsoluteError){ minAbsoluteError = absoluteError[i]; }
    }

    cout << "Results from testing the 1st derivative method: \n";
    cout << "Maximum absolute error in the interval: " << "\t" << maxAbsoluteError << "\n";
    cout << "Minimum absolute error in the interval: " << "\t" << minAbsoluteError << "\n";
    cout << "Average absolute error in the interval: " << "\t" << averageAbsoluteError << "\n";
}


void OneDimensionalGrid::test2ndDerivative()
{   
    const int N = getGridLength();
    vector<double> func(N, 0.0);

    for (int i = 0; i < N; i++)
    {   
        double x = getGridPoint(i);
        func[i] = sin(x);
    }
    
    vector<double> dfuncdx = calculate2ndDerivative(func);

    vector<double> absoluteError(N, 0.0);
    
    double maxAbsoluteError = fabs( dfuncdx[0] - ( -sin(getGridPoint(0)) ) );
    double minAbsoluteError = maxAbsoluteError;
    double averageAbsoluteError = 0.0;
    for (int i = 0; i < N; i++)
    {   
        double x = getGridPoint(i);
        absoluteError[i] = fabs( dfuncdx[i] - ( -sin(x) ) );
        averageAbsoluteError = averageAbsoluteError + absoluteError[i]/N;
        //cout << x << "\t" << func[i] << "\t" << dfuncdx[i] << "\t" << absoluteError[i] << "\n";
        if (absoluteError[i]>maxAbsoluteError){ maxAbsoluteError = absoluteError[i]; }
        if (absoluteError[i]<minAbsoluteError){ minAbsoluteError = absoluteError[i]; }
    }
    
    cout << "Results from testing the 2nd derivative method: \n";
    cout << "Maximum absolute error in the interval: " << "\t" << maxAbsoluteError << "\n";
    cout << "Minimum absolute error in the interval: " << "\t" << minAbsoluteError << "\n";
    cout << "Average absolute error in the interval: " << "\t" << averageAbsoluteError << "\n";
}