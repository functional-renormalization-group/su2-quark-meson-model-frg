#ifndef ONEDIMENSIONALGRID_H
#define ONEDIMENSIONALGRID_H

#include <vector>
#include <string>


enum DifferentiationMethod1D
{ 
	NONE, 
	FiniteDifferences3PointRule, 
	FiniteDifferences5PointRule, 
	FiniteDifferences7PointRule, 
	FiniteDifferences9PointRule, 
	DifferentiationMethod1DCount // Used as the boundary, add new methods above this value! 
};

std::string toStringDifferentiationMethod1D(DifferentiationMethod1D );

DifferentiationMethod1D stringToDifferentiationMethod1D(const std::string& );


class OneDimensionalGrid
{	
private:
	double gridMin = 0;
	double gridMax = 0;
	int gridLength = 0;
	double gridDelta = 0;
	std::vector<double> gridPoints;
	DifferentiationMethod1D derivativeMethod1D;

private:
	void setDerivativeMethod1D(DifferentiationMethod1D );
	
public:
	DifferentiationMethod1D getDerivativeMethod1D(){ return derivativeMethod1D; }
	std::string getDerivativeMethod1DString(){ return ( toStringDifferentiationMethod1D(derivativeMethod1D) ); }

	OneDimensionalGrid(){};
	OneDimensionalGrid(double , double , int );
	OneDimensionalGrid(double , double , int , DifferentiationMethod1D );

	double getGridMin(){ return gridMin; }
	double getGridMax(){ return gridMax; }
	int getGridLength(){ return gridLength; }
	double getGridDelta(){ return gridDelta; }
	double getGridPoint(int i){ return gridPoints[i]; }
	std::vector<double> getGridPoints(){ return gridPoints; }

	std::vector<double> calculate1stDerivative(std::vector<double> );
	std::vector<double> calculate2ndDerivative(std::vector<double> );
	void test1stDerivative();
	void test2ndDerivative();
};



#endif