#ifndef DERIVATIVE_FINITE_DIFFERENCES_H
#define DERIVATIVE_FINITE_DIFFERENCES_H

#include <vector>

std::vector<double> calculate1stDerivative3PointRule(const double , const int , std::vector<double> &);

std::vector<double> calculate2ndDerivative3PointRule(const double , const int , std::vector<double> &);

std::vector<double> calculate1stDerivative5PointRule(const double , const int , std::vector<double> &);

std::vector<double> calculate2ndDerivative5PointRule(const double , const int , std::vector<double> &);

std::vector<double> calculate1stDerivative7PointRule(const double , const int , std::vector<double> &);

std::vector<double> calculate2ndDerivative7PointRule(const double , const int , std::vector<double> &);

std::vector<double> calculate1stDerivative9PointRule(const double , const int , std::vector<double> &);

std::vector<double> calculate2ndDerivative9PointRule(const double , const int , std::vector<double> &);

#endif