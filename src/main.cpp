//Renan Câmara Pereira 2022 - On Going

//Everything is in GeV and c=hbar=1
//The UV potential is defined as U(sigma) = (1/2) m^2 sigma^2 + (1/4) lambda sigma^4

#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <omp.h>
#include "OneDimensionalGrid.h"
#include "SU2QuarkMesonModelEffectiveAction.h"
#include "SU2QuarkMesonModelGrandPotential.h"
#include "command_line_processor.h"

using namespace std;


int main(int argc, char* argv[])
{	
	// Start counting time
    double start_s = omp_get_wtime();


    // Call the function and check its return value
	int processorResult = commandLineArgsProcessor(argc, argv);
    if ( processorResult==1 ) 
	{
        // If it returns 1, exit with failure code
        return 1;
    }
	else
	{
    	// Continue the main program execution if the function returns 0
    	std::cout << "\nCommands processed successfully, continuing execution..." << std::endl;
	}

/*
	//quark-meson model parameters
	double Lambda = 1.0;
	double gS = 4.2;
	double m = 0.969*Lambda;
	double lambda = 0.001;
	double c = 0.00175*pow(Lambda,3);
	double temp = 0.080;
	double cpu = 0.0;
	double cpd = 0.0;
	temp = 0.020;
	cpu = 0.3458;
	cpd = 0.3458;
	SU2QuarkMesonModelParameters qmParams(Lambda, gS, m, lambda, c, temp, cpu, cpd);

	//grid parameters
	double sMin = 0.002;
	double sMax = 0.122;
	int Ns = 80;
	OneDimensionalGrid gridParams(sMin, sMax, Ns, FiniteDifferences5PointRule);

	//flow parameters
	double kUV = Lambda;
	double kIR = 0.080;
	QuarkMesonFlowParameters flowParams(kUV, kIR);

	//create action
	SU2QuarkMesonModelEffectiveAction action(qmParams, gridParams, flowParams);
	action.printPotentialVsSigmaOnConsole();
	action.solveFlowEqsLPA();
	
	//action.logAllToFiles("configOG");
	
	action.printPotentialVsSigmaOnConsole();

	
	SU2QuarkMesonModelGrandPotential grandPotential(action);
	vector<SU2QuarkMesonModelActionExtremum> extrema = grandPotential.getExtrema();
	cout << extrema.size() << "\n";
	for (int i = 0; i < int(extrema.size()); i++)
	{
		cout << extrema[i].temperature << "\t";
		cout << extrema[i].chemicalPotentialUpQuark << "\t";
		cout << extrema[i].chemicalPotentialDownQuark << "\t";
		cout << extrema[i].numberOfExtrema << "\t";
		cout << extrema[i].sigmaExtrema << "\t";
		cout << "\n";
	}
*/


/*	
	SU2QuarkMesonModelPhaseDiagramAtFixedTemperature grandPot("grandPotential_T0.020000_CpMin0.342000_CpMax0.348000.dat", akima);
	cout << grandPot.calculateUpQuarkChemicalPotentialOfPhaseTransition() << "\n";
	vector<double> aux = grandPot.calculateUpQuarkChemicalPotentialOfSpinodals();
	for (int i = 0; i < int(aux.size()); i++)
	{
		cout << aux[i] << "\n";
	}
*/

/*
	double temperatureMin = 0.010;
	double temperatureMax = 0.100;
	double cp = 0.100;
	int numberTemperature = 10;
	runPhaseDiagramAtFixedDegenerateChemicalPotential(qmParams, gridParams, flowParams, cubic, 7, temperatureMin, temperatureMax, cp, numberTemperature);
*/



	// Stop clock and print total run time
    double stop_s = omp_get_wtime();
    double run_time = (stop_s-start_s);
    std::cout << "Run Time: " << run_time << std::endl;

	return 0;
}



