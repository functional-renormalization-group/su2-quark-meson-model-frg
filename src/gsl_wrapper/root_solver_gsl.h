#ifndef ROOT_SOLVER_GSL_H
#define ROOT_SOLVER_GSL_H

#include <gsl/gsl_vector.h>

enum MultiRootFindingMethod { hybrids, hybrid, dnewton, broyden };

void multiDimensionalRootFind(int , double , double* , void* , int (const gsl_vector*, void*, gsl_vector*), MultiRootFindingMethod );

enum RootFindingMethod { brent, bisection, falsepos };

double OneDimensionalRootFind(double , double , double , void* , double placeholder_f (double, void*), RootFindingMethod );

#endif