# SU2 Quark-Meson model within the Functional Renormalization Group appoach

## File Naming Convention

In the C++ part of this project, we use a mixed file naming convention to distinguish between different types of files and enhance project organization. The convention is as follows:

1. PascalCase: Files that contain class definitions or core modules are named in PascalCase, where each word is capitalized and no underscores are used. This helps identify key classes and modules in the project.    
Examples:
    - InterpolationGSL1Dim.cpp / InterpolationGSL1Dim.h
    - OneDimensionalGrid.cpp / OneDimensionalGrid.h
    - SU2QuarkMesonModelEffectiveAction.cpp / SU2QuarkMesonModelEffectiveAction.h

2. snake_case: Utility files, test files, and other non-class-specific modules are named in snake_case, where words are lowercase and separated by underscores. This makes it easy to recognize files that support or test core modules but aren’t classes themselves.
Examples:
    - main.cpp – The main entry point of the application.
    - main_tests.cpp – A test file for running unit tests.
    - derivative_finite_differences.cpp – A utility for finite difference calculations.

3. The main output executable for this project follows the camelCase naming convention.

The naming convention explained above is designed to improve readability:
1. PascalCase: Reserved for files containing primary classes or major modules, providing a clear separation of core components.
2. snake_case: Used for utilities, entry points, and test files, making it clear that these files support or complement the main classes without directly representing them.


## Evaluating extrema points of the effective potential in the infrared

After solving the flow equations from the UV down to the IR, the extrema points of the effective potential must be calculated. This is calculated using the following algorithm:
1. The derivative of the effective potential (including the symmetry breaking term) with respect to the sigma field is calculated using the same finite differences rule used in the partial differential equation solver;
2. Guesses are saved for the extrema points: the grid point immediately before the change of sign of the derivative calculated above;
3. The extrema points are calculated using the provided interpolation method.

